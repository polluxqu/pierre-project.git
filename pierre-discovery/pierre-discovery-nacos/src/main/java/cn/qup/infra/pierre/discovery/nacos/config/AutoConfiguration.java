package cn.qup.infra.pierre.discovery.nacos.config;

import cn.qup.infra.pierre.discovery.nacos.properties.MyNacosProperties;
import cn.qup.infra.pierre.discovery.nacos.util.AutoRegisterService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;

@Import({NacosConfiguration.class,
        AutoRegisterService.class})
public class AutoConfiguration {

    @Bean
    MyNacosProperties myNacosProperties(){
        return new MyNacosProperties();
    }

}
