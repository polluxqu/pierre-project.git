package cn.qup.infra.pierre.discovery.nacos.util;

import cn.qup.infra.pierre.core.properties.AppRuntime;
import cn.qup.infra.pierre.discovery.nacos.properties.MyNacosProperties;
import com.alibaba.nacos.api.PropertyKeyConst;
import com.alibaba.nacos.api.annotation.NacosInjected;
import com.alibaba.nacos.api.naming.NamingService;
import com.alibaba.nacos.api.naming.pojo.Instance;
import com.alibaba.nacos.client.naming.NacosNamingService;
import com.alibaba.nacos.common.http.client.NacosRestTemplate;
import com.alibaba.nacos.common.spi.NacosServiceLoader;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.net.InetAddress;
import java.util.Properties;

@Slf4j
@Component
public class AutoRegisterService implements InitializingBean {

    @Autowired(required = false)
    AppRuntime appRuntime;

    @Autowired
    MyNacosProperties myNacosProperties;

    @Value("${server.port:8080}")
    private Integer port;

//    @Autowired
//    @NacosInjected
//    NacosNamingService nacosNamingService;

    @Autowired
//    @NacosInjected
    NamingService namingService;

    @Override
    public void afterPropertiesSet() {
        // 使用配置类的方式，需在应用启动类中加注解 @EnableNacosDiscovery
        try {
//            Properties properties = new Properties();
//            properties.setProperty(PropertyKeyConst.SERVER_ADDR,myNacosProperties.getServerAddr());
//            properties.setProperty(PropertyKeyConst.NAMESPACE,myNacosProperties.getNamespace());
//            properties.setProperty("autoRegister",String.valueOf(myNacosProperties.isAutoRegister()));
//            nacosNamingService = new NacosNamingService(properties);
            String ip = IpUtil.getServerIp();
            namingService.registerInstance(appRuntime.getAppName(),appRuntime.getAppGroup(),ip,port);
            log.info(String.format("【Pierre-Discovery】: Service %s register on Nacos success.", appRuntime.getAppName()));
        }catch (Exception e){
            log.error("【Pierre-Discovery】: fail by ",e);
        }
    }
}
