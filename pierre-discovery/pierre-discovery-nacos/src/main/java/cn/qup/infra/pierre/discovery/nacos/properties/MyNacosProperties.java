package cn.qup.infra.pierre.discovery.nacos.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "pierre.discovery")
@EnableConfigurationProperties(MyNacosProperties.class)
public class MyNacosProperties {

    private String serverAddr;
    private String namespace;
    private boolean autoRegister;

}
