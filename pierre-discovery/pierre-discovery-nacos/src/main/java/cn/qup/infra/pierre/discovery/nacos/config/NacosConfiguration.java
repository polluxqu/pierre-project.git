package cn.qup.infra.pierre.discovery.nacos.config;

import cn.qup.infra.pierre.core.properties.AppRuntime;
import cn.qup.infra.pierre.discovery.nacos.properties.MyNacosProperties;
import com.alibaba.fastjson.JSON;
import com.alibaba.nacos.api.NacosFactory;
import com.alibaba.nacos.api.PropertyKeyConst;
import com.alibaba.nacos.api.annotation.NacosInjected;
import com.alibaba.nacos.api.exception.NacosException;
import com.alibaba.nacos.api.naming.NamingFactory;
import com.alibaba.nacos.api.naming.NamingService;
import com.alibaba.nacos.client.naming.NacosNamingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Properties;

@Slf4j
@Configuration
public class NacosConfiguration {

    @Autowired(required = false)
    AppRuntime appRuntime;

    @Autowired(required = false)
    MyNacosProperties myNacosProperties;

//    @Bean
//    NacosNamingService nacosNamingService(){
//        try{
//            Properties properties = new Properties();
//            properties.setProperty(PropertyKeyConst.SERVER_ADDR,myNacosProperties.getServerAddr());
//            if(!ObjectUtils.isEmpty(myNacosProperties.getNamespace())){
//                // 配置中未指定 Namespace ，则不配置本项，使用默认的 public
//                properties.setProperty(PropertyKeyConst.NAMESPACE,myNacosProperties.getNamespace());
//            }
//            properties.setProperty("autoRegister",String.valueOf(myNacosProperties.isAutoRegister()));
//            return new NacosNamingService(properties);
//        }catch (NacosException e){
//            log.error("",e);
//            return null;
//        }
//    }

    @Bean
    NamingService namingService(){
        try{
            Properties properties = new Properties();
            properties.setProperty(PropertyKeyConst.SERVER_ADDR,myNacosProperties.getServerAddr());
            properties.setProperty(PropertyKeyConst.NAMESPACE,appRuntime.getEnv().name());
            properties.setProperty("autoRegister",String.valueOf(myNacosProperties.isAutoRegister()));
            return NamingFactory.createNamingService(properties);
        }catch (NacosException e){
            log.error("",e);
            return null;
        }
    }
}
