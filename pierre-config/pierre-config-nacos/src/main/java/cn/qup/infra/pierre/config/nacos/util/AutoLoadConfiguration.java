package cn.qup.infra.pierre.config.nacos.util;

import cn.qup.infra.pierre.config.nacos.properties.MyNacosConfigProperties;
import cn.qup.infra.pierre.config.nacos.service.MyNacosConfigListener;
import cn.qup.infra.pierre.core.properties.AppRuntime;
import com.alibaba.nacos.api.annotation.NacosInjected;
import com.alibaba.nacos.api.config.ConfigService;
import com.alibaba.nacos.api.exception.NacosException;
import com.alibaba.nacos.client.config.NacosConfigService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class AutoLoadConfiguration implements InitializingBean {

    @Autowired
    AppRuntime appRuntime;

    @Autowired
    @NacosInjected
    ConfigService configService;

    @Autowired(required = false)
    MyNacosConfigProperties myNacosConfigProperties;

    @Override
    public void afterPropertiesSet() {
        try{
            String dataId = myNacosConfigProperties.getDataId();
            String conf = configService.getConfig(dataId,appRuntime.getAppGroup(),5000);
            MyNacosConfigListener listener = new MyNacosConfigListener();
            configService.addListener(dataId, appRuntime.getAppGroup(),listener);
            log.info("【Pierre-Config】: Nacos Configuration Listener running on dataId {}.",dataId);
        }catch (NacosException e){
            log.error("",e);
        }

    }
}
