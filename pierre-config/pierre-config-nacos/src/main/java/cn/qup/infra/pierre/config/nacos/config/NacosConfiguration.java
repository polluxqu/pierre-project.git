package cn.qup.infra.pierre.config.nacos.config;

import cn.qup.infra.pierre.config.nacos.properties.MyNacosConfigProperties;
import cn.qup.infra.pierre.core.properties.AppRuntime;
import com.alibaba.fastjson.JSON;
import com.alibaba.nacos.api.NacosFactory;
import com.alibaba.nacos.api.PropertyKeyConst;
import com.alibaba.nacos.api.config.ConfigService;
import com.alibaba.nacos.client.config.NacosConfigService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.ObjectUtils;

import java.util.Properties;

@Slf4j
@Configuration
public class NacosConfiguration {

    @Autowired(required = false)
    AppRuntime appRuntime;

    @Autowired(required = false)
    MyNacosConfigProperties myNacosConfigProperties;

    @Bean
    @ConditionalOnProperty("pierre.config")
    ConfigService configService(){
        try{
            final Properties properties = new Properties();
            properties.setProperty(PropertyKeyConst.SERVER_ADDR,myNacosConfigProperties.getServerAddr());
            properties.setProperty(PropertyKeyConst.NAMESPACE,myNacosConfigProperties.getNamespace());
            properties.setProperty("bootstrap.enable",String.valueOf(myNacosConfigProperties.getBootStrap().isEnable()));
            if(!ObjectUtils.isEmpty(myNacosConfigProperties.getGroup())){
                properties.setProperty("group",myNacosConfigProperties.getGroup());
            }else{
                properties.setProperty("group",appRuntime.getAppGroup());
            }
            if(!ObjectUtils.isEmpty(myNacosConfigProperties.getDataId())){
                properties.setProperty("data-id",myNacosConfigProperties.getDataId());
            }else{
                properties.setProperty("data-id",appRuntime.getAppRootPackageName() + "-application");
            }
            if(!ObjectUtils.isEmpty(myNacosConfigProperties.getType())){
                properties.setProperty("type",myNacosConfigProperties.getType());
            }else {
                // Nacos 默认的配置文件格式为 properties，这里重写为 yaml
                properties.setProperty("type","yaml");
            }

            if(!ObjectUtils.isEmpty(myNacosConfigProperties.getAutoRefresh())){
                properties.setProperty("auto-refresh",String.valueOf(myNacosConfigProperties.getAutoRefresh()));
            }else {
                properties.setProperty("auto-refresh","true");
            }

            if(!ObjectUtils.isEmpty(myNacosConfigProperties.getRemoteFirst())){
                properties.setProperty("remote-first",String.valueOf(myNacosConfigProperties.getRemoteFirst()));
            }else {
                properties.setProperty("remote-first","true");
            }
            log.info("【Pierre-Config】: Pierre-Configuration-Center connected {}.", JSON.toJSONString(properties));

//            return new NacosConfigService(properties);
            return NacosFactory.createConfigService(properties);
        }catch (Exception e){
            log.error("",e);
            return null;
        }
    }
}
