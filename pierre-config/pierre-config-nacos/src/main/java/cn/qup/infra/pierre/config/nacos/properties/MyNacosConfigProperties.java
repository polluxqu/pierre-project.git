package cn.qup.infra.pierre.config.nacos.properties;

import cn.qup.infra.pierre.config.nacos.pojo.BootStrap;
import lombok.Data;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "pierre.config")
@ConditionalOnProperty("pierre.config")
@EnableConfigurationProperties(MyNacosConfigProperties.class)
public class MyNacosConfigProperties {

    private BootStrap bootStrap;
    private String serverAddr;
    private String namespace;
    private String group;
    private String dataId;
    private String type;
    private Boolean autoRefresh;
    private Boolean remoteFirst;

}
