package cn.qup.infra.pierre.config.nacos.service;

import com.alibaba.nacos.api.config.listener.Listener;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.Executor;

@Slf4j
public class MyNacosConfigListener implements Listener {

    @Override
    public Executor getExecutor() {
        return null;
    }

    @Override
    public void receiveConfigInfo(String s) {
        log.info("【Pierre-Config】: configuration has been changed {}",s);
    }
}
