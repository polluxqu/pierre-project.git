package cn.qup.infra.pierre.config.nacos.config;

import cn.qup.infra.pierre.config.nacos.properties.MyNacosConfigProperties;
import cn.qup.infra.pierre.config.nacos.util.AutoLoadConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;

@Import({NacosConfiguration.class,
        AutoLoadConfiguration.class})
public class AutoConfiguration {

    @Bean
    @ConditionalOnProperty("pierre.config")
    MyNacosConfigProperties myNacosConfigProperties(){
        return new MyNacosConfigProperties();
    }
}
