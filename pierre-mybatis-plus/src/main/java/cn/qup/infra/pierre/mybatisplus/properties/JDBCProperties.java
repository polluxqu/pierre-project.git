package cn.qup.infra.pierre.mybatisplus.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "spring.datasource")
public class JDBCProperties {

    private String url;
    private String userName;
    private String password;

    @Bean
    JDBCProperties jdbcProperties(){
        return new JDBCProperties();
    }

}
