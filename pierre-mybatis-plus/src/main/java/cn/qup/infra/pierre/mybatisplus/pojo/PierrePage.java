package cn.qup.infra.pierre.mybatisplus.pojo;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

public class PierrePage<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    @Getter
    @Setter
    private long size=10;
    @Getter
    @Setter
    private long current = 1;
    @Getter
    @Setter
    private long total;
    @Getter
    @Setter
    private long pages;
    @Getter
    @Setter
    private boolean hasNext;
    @Getter
    @Setter
    private boolean hasPrevious;

    private List<T> records;

    private Page<T> page;
    public Page<T> get(){
        Page<T> page = new Page<>();
        page.setSize(this.size);
        page.setCurrent(this.current);
        return page;
    }

    public void set(Page<T> page){
        this.size = page.getSize();
        this.current = page.getCurrent();
        this.pages = page.getPages();
        this.total = page.getTotal();
        this.hasNext = page.hasNext();
        this.hasPrevious = page.hasPrevious();
        this.records = page.getRecords();
    }

}
