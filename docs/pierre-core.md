# Pierre Core

## 说明

Pierre-Core 是 Pierre 开发脚手架中的核心依赖，提供了框架中的一些基础类型、Util及 Bean。
以下为一些基础的能力介绍及使用介绍

## Util 能力

### CommonRequest 及 CommonResponse 

CommonRequest 及 CommonResponse 提供了 Http 接口快速封装的能力，均需 extends 使用。使用说明如下：

#### 定义接口的 Request
其中各属性代表请求的参数，@Param 注解支持的 Type 有 QUERY_STRING, POST_ITEM, HEADER 等

    @Data
    @Accessors(chain = true)
    @EqualsAndHashCode(callSuper = true)
    public class GetIPInfoRequest extends CommonRequest<GetIPInfoResponse> {
        // 该类中的所有属性为自定义接口参数，通过 @Param 注解区别参数类型，ParamType 支持 QUERY_STRING, POST_ITEM, HEADER 等
        @Param(type = ParamType.QUERY_STRING)
        private String ip;

        public GetIPInfoRequest(){
            // 接口地址
            this.url = "https://searchplugin.csdn.net/api/v1/ip/get";
            // 接口的 Http Method 类型
            this.methodType = MethodType.GET;
            // 返回值的自定义类型
            this.clz = GetIPInfoResponse.class;
        }
#### 定义接口的 Response
由 CommonResponse 扩展而来，CommonResponse 内置了 String code/ T data / String message，三个常见接口返回值，如与自定义接口返回值一致，则可直接定义一个空的自定义 Response 即可。
*注意： 如在自定义 Response 中已定义了其它返回属性，且接口返回结果字段中含 data 字段，则需在自定义 Response 中重新定义该字段，否则该字段将不会展示。如本示例所示，接口返回中不是 message 而是 msg，则需重新定义。接口返回的 data 我们明确结构并定义为 IpInfo 而不是 Object，而也需要重新定义。*

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    public class GetIPInfoResponse extends CommonResponse {

        private String msg;
        private IpInfo data;
    }
#### 使用定义的接口

        GetIPInfoRequest request = new GetIPInfoRequest();
        // 打开接口的网络请求性能跟踪能力，不设此参数默认为关闭
        request.setTraceNetwork(true);
        GetIPInfoResponse response = request.setIp("114.114.114.114")
                .exec();
        // 上述代码实际是拼装了一个对 https://searchplugin.csdn.net/api/v1/ip/get?ip=114.114.114.114 的 GET 请求，并返回结果
        log.info(JSON.toJSONString(response));
#### 查看结果
其中 trace 块中包含了接口整体请求从开始到各个节点中间的毫秒数，可自行计算每个节点的耗时，如下例所示，DNS 解释用时为 dnsEnd - dnsStart = 23.901 - 7.294 约为 16.6 毫秒

    {
        "code":"200",
        "data":{
            "address":"114DNS.COM 114DNS.COM  ",
            "ip":"114.114.114.114"
        },
        "msg":"success",
        "trace":{
            "url":"https://searchplugin.csdn.net/api/v1/ip/get?ip=114.114.114.114",
            "requestId":"7938af6a-5d69-4aaf-aa49-924d5f934c31",
            "callStart":"1.321",
            "dnsStart":"7.294",
            "dnsEnd":"23.901",
            "connectStart":"26.496",
            "secureConnectStart":"71.511",
            "secureConnectEnd":"321.042",
            "connectEnd":"339.321",
            "connectionAcquired":"355.972",
            "requestHeadersStart":"364.194",
            "requestHeadersEnd":"374.801",
            "responseHeadersStart":"547.163",
            "responseHeadersEnd":"549.811",
            "responseBodyStart":"564.517",
            "responseBodyEnd":"564.708",
            "connectionReleased":"565.328",
            "callEnd":"565.391"
        }
    }
### HexUtil

该 Util 为静态类，提供了基本的十六进制转换，包括十六进制字符串与 Byte 数组之间的相互转换、Byte 数组的 Base64加解密、Byte 数组
大小端转 int 等能力。

### DateTimeUtil

该 Util 为静态类，提供了多种与日期时间相关的转换能力，举例如下：
* Unix 时间戳与 Windows 时间戳的互换（操作 Windows AD时与时间相关时使用）
* 本地时间与 UTC 时间互换（支持多种源时间字符串格式）
* 获取两个指定时间之差（支持多种时间差单位）
* 获取昨天、今天、明天、上周、本周、上月、本月的开始及结束时间
* 获取指定日期所在季度的首月开始时间
* 获取指定时间指定前。后指定分钟后的时间
* 获取指定日期指定前、后指定天的开始时间
* 判断指定日期是否在 [startDate, endDate] 区间
* 判断指定时间所在年份是否闰年
* 判断指定的两个时间是否在同一周
* 获取指定时间为当年度的第几周
* 获取指定时间所在周的周几为几号
* 各种日期格式字符串到 Date 的互转

### ZlibUtil

该 Util 为静态类，提供了基本的字符串及 Byte 数组的 Zlib 压缩和解压能力。

## Bean 定义

### AppRuntime
包含了如 AppName, Description, Id, RootPackageName, InstanceId, Env 等信息。

### idWorker
雪花 Id 算法生成器，可通过 Autowired 后使用 idWorker.nextId() 直接生成新的 Id。

## 其它定义

ApiResult<T>, 可被 implements 的 ErrorCode 用于自定义友好的错误抛出，结合 BizException 使用。



