# Pierre-Config

## 说明

pierre-config 是 Pierre 开发脚手架的配置中心模块，用于管理配置信息。目前已经完成 Nacos 配置中心的对接。

## 使用方法

在项目pom.xml 中添加依赖：

    <dependencies>
        <dependency>
            <groupId>cn.qup.infra</groupId>
            <artifactId>pierre-config-nacos</artifactId>
            <version>${pierre.version}</version>
        </dependency>
    </dependencies>

pierre-config-nacos 模块中，提供了对接 Nacos 配置中心的实现。通过配置文件，配置 Nacos 配置中心的地址、命名空间、数据 ID、组 ID 等信息。
配置文件格式如下：

    # Pierre服务配置
    # 本配置用于定义Pierre服务与Nacos配置中心的连接及交互参数
    pierre:
      config:
        # Nacos服务器地址，用于建立与Nacos服务器的连接
        server-addr: ${nacos.config.serverAddr}
        # Nacos命名空间，用于隔离不同的配置环境
        namespace: ${nacos.config.namespace}
        # Nacos配置分组，用于进一步组织和管理配置
        group: ${nacos.config.group}
        # 配置ID，用于唯一标识所需的配置
        data-id: ${nacos.config.dataId}
        # 配置类型，用于指定配置的格式，如properties、yaml等
        type: ${nacos.config.type}
        # 是否自动刷新配置，当配置变更时，自动触发刷新动作
        auto-refresh: ${nacos.config.autoRefresh}
        # 是否优先从远程获取配置，用于在启动时或配置更新时决定从哪里获取配置
        remote-first: ${nacos.config.remoteFirst}
        # BootStrap配置，用于控制服务的启动和运行行为
        boot-strap:
          # 是否启用BootStrap，用于决定是否在服务启动时执行特定的初始化逻辑
          enabled: ${nacos.config.bootstrap.enabled}

注：
1. 本配置必选项为 server-addr、namespace 及 boot-strap.enable
2. group 如不配置，则会默认使用 spring.application.group 配置值
3. data-id 如不配置，则会默认使用 main 入口类的 package name + "-application", 如 "cn.qup.biz.ucenter-application"
4. type 如不配置，则会默认配置文件类型为 yaml 
5. auto-refresh 默认配置为 true
6. remote-first 默认配置为 true
