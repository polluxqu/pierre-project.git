# Pierre Trace

## 简介

pierre-trace 是一个基于 AOP 技术对代码指定块获取执行效率的工具，只需在 pom 中完成引用，并在 application.yml 中进行简单配置即可。监控数据输出默认为 console + log，也可通过简单的配置支持将结果输出至 kafka 中。 

## 使用示例

### 配置监控代码块

    pierre:
        trace:
            expression:
                - '@annotation(cn.qup.infra.pierre.trace.annotation.CalculateExecuteTime)'
                - '@annotation(org.springframework.web.bind.annotation.RequestMapping)'
                #- 下面是一些符合 Java AOP pointcut 表达式的示例
                #- 'execution(* cn.qup.infra..*(..))'
                #- 'within(cn.qup.infra.pierre.example.service..*)'

expression 为一个列表数组，表明将对符合哪些指定的 AOP 表达式的代码块进行执行效率监测。pierre-trace 内置了一个注解 @CalculateExecuteTime，可在需要获取执行效率监测的方法上直接添加该注解。
该表达式完全兼容 AOP 的所有表达式，具体可参考 [简书 - spring aop切面表达式详解及例子](https://www.jianshu.com/p/018c8cf70a59)。

### 配置输出结果至 Kafka

    pierre:
        trace:
            kafka:
                bootStrapServers:
                topics:
                #以下配置有默认值，如使用默认值可不需配置
                acks: "all"
                retries: 0
                batchSize: 16384L
                linger: 1L
                buffer: 33554432L
