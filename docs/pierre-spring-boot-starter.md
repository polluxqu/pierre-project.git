# Pierre Springboot Starter

## 简介

pierre-spring-boot-starter 是一个标准的 Spring Boot 自定义启动器，提供了 Pierre 开发框架下应用的基本能力。

## 能力介绍

### 标准化日志能力

pierre-spring-boot-starter 框架项目默认除 local 环境外，其它环境日志输出目录至 /data/logs/{app_name}/{instance_id}，日志默认预设了 APP_LOG、ACCESS_LOG、AUDIT_LOG 三种类型，可通过如下示例的代码进行选择，将不同类型的日志存入不同的文件，以便实现不同类型日志的分类搜集及在日志归档中的不同保存期限策略的目标实现。
    
    private Logger logger = LoggerFactory.getLogger("ACCESS_LOG");

### 标准化 Restful API 接口及全局异常输出能力

pierre-spring-boot-starter 框架项目对标准化的 Restful API(所有带 @RestController 注解的类中接口) 及所有的异常提供了统一的输出封包。

封包格式为 {code，msg，data}，为方便前端使用，code 字段兼容 http status codes，即接口成功时 code 为 200，也支持在接口中抛出自定义异常，示例代码如下：

    @ApiOperation(value = "测试Exception", notes = "")
    @RequestMapping(name = "demoException", value = "/demoException", method = RequestMethod.GET)
    public String demoException(HttpServletRequest request, @RequestParam String name){
        String str ="";
        try{
            if(name.equalsIgnoreCase("abc")){
                throw new BizException(CommonErrorCode.APP_PASSWORD_ERROR);
            }else{
                str = "correct";
            }
        }catch (BizException e){
            BizException.throwException(e.getCode(),e.getMsg());
        }
        return str;
    }

### Swagger 能力

pierre-spring-boot-starter 框架项目默认打开了 Swagger，访问地址为 http://127.0.0.1:{$port}/doc.html ，**Swagger 仅在 local 及 dev 环境中打开，且仅支持内网来源 IP 进行访问**。

注：swagger 仅内网访问可通过 application.yml 配置进行开关控制，具体配置为

    pierre:
        swagger:
            internal-access-only: false

本配置项不存在时默认值为 true，即 swagger 默认仅允许内网 IP 来源进行访问。

### 链路跟踪能力

pierre-spring-boot-starter 框架项目默认使用 Zipkin 实现链路跟踪能力，默认仅开放了 Controller 的接口请求跟踪，如需service 或方法级跟踪，可按 Zipkin 标准方式自定义新的 span 收集器。

若要使用本能力，请在 application.yml 中配置 Zipkin 服务相关信息

    pierre:
        trace:
            # Zipkin 服务器 url
            url: http://ip:port
            #下面的参数有如下默认值，按需修改添加使用，如无需修改，则不需要加入配置中
            connectTimeout: 6000
            readTimeout: 6000
            flushInterval: 1
            compressionEnabled: true
            samplerRate: 1

### 接口验证能力

pierre-spring-boot-starter 框架项目默认支持 AuthToken 及 Signature 两种方式对接口进行自定义鉴权，使用方式如下：

1. 在项目启动类上添加 @EnableAutoRegisterPMS (如使用 pierre-initializr 生成的项目会自动生成此注解)
2. 对要进行监控的接口上添加 @KeyAuthNeeds 或 @SignatureNeeds 注解 
3. 创建一个类 implements IPMSValidateService，实现其中的 pmsKeyAuthValidate 或 pmsSignatureValidate 方法（取决于接口是使用 @KeyAuthNeeds 或 @SignatureNeeds 进行注解的） 
4. 上述两个方法均会将请求的 HttpServletRequest 对象进行透传，实现方法时可从该对象中自由获取 Header/Cookies 等信息。 
5. 上述两个方法返回值 ValidateResponse 对象中的 insertHeaders 属性为 Map<String, String>，在自定义鉴权完成后可通过为该属性赋值，以达到将鉴权中产生的一些数据（如 token 对应的 userRole 等）传递回接口层，示例如下：


        @Slf4j
        @Service
        public class PMSValidateServiceImpl implements IPMSValidateService {

            @Override
            public ValidateResponse pmsKeyAuthValidate(HttpServletRequest request) {
                // 自定义的 KeyAuth 注解接口验证
                // 直接验证通过，并对 Request 中注入 header pms-x-user-role
                // 也可注入其它信息，如用户Id等，方便 Controller 层的 RequestMapping 接口方法中进行使用
                ValidateResponse response = new ValidateResponse();
                // response code 为 “0” 时 starter 自动校验接口鉴权通过
                response.setCode("0");
                // 鉴权不通过时可对 response 自定义返回的 code 及 message 如下
                // response.setCode("403");
                //  response.setMessage("访问被拒绝");

                Map<String,String> map = new HashMap<>();
                map.put("pms-x-user-role","admin");
                response.setInsertHeaders(map);
                return response;
            }

            @Override
            public ValidateResponse pmsSignatureValidate(HttpServletRequest request) { 
                return null;
            }
        }

### 事件注册、监听器能力

pierre-spring-boot-starter 框架项目内置了事件注册、监听器能力，具体使用方式如下：

1. 定义自定义事件类，扩展PierreEvent，示例如下：
注：事件类必须扩展自 PierreEvent, 该类已内置了 eventName, eventId 及 eventTime 属性，无需新增.

        @Data
        public class MyEvent extends PierreEvent {
            private String source;
            private String target;
            private String content;
        }

2. 定义自定义事件的监控器，实现对该事件的监听处理，示例如下：

        @Slf4j
        public class MyEventListener implements PierreListener<MyEvent> {
        
            @Override
            public void onApplicationEvent(MyEvent event) {
                log.info("MyEventListener onApplicationEvent: {}", event);
            }
        }

3. 注册自定义事件监听器并发布事件，示例如下：

        @Slf4j
        public class MyEventTest {
            public static void main(String[] args) {
                // 注册事件监听
                EventManage.addListener(new MyEventListener());
                // 发布事件
                MyEvent event = new MyEvent();
                event.setEventName("MyEvent");
                event.setSource("source");
                event.setTarget("target");
                event.setContent("content");
                EventManage.publish(event);
                log.info("MyEventTest Published: {}", event);
            }
        }

### 接口限流能力

pierre-spring-boot-starter 框架项目默认实现了接口限流能力，限流统计模式支持单ip + url（即对只针对同一ip来的请求进行聚合统计，并判断其是否需要被限流）和全局，具体使用方式如下：
在需要配置限流的接口上添加如下注释

      @AccessLimit(region = AccessLimitRegion.GLOBAL, type = AccessLimitType.QUOTA, duration = 60, limit = 100)
region 参数说明：

GLOBAL: 表示访问限流全局有效，不区分来源IP，满足限流阈值要求的任何 ip 来源的请求均会执行该限流动作

IP：表示访问限流条件为 url + ip，达到标准后仅针对该 ip 及该 url 的请求执行限流动作

type 参数说明：

QUOTA：表示一段时间内访问次数限制

QPS：表示并发访问次数限制

duration 参数表示时间段（秒）该参数在 type 为 QUOTA 时有效，limit 参数表示限流阈值（访问次数或QPS）
