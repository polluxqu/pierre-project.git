# Pierre Security

## 说明

pierre-security 为 pierre 框架提供了基础的安全相关能力接口，主要为各种加密及随机密码生成等。

## 能力简介

### GoogleAuthenticator

GoogleAuthenticator 为一个静态类，可以直接使用，主要使用介绍如下所示：

* 生成一个 GoogleAuthenticator Secret 实体


    GoogleAuthenticator.genSecret("myApp");
* 校验指定 MFA 动态验证码是否匹配


    GoogleAuthenticator.authCode("821431","myApp");
### PasswordGenerator

PasswordGenerator 为一个静态类，可以直接使用。用于生成指定位数的随机密码，可指定密码位数，小写字母位数，符号位数，数字位数等。

### DESSlatUtil

DESSlatUtil 为一个静态类，可以直接使用。用于生成加盐的 DES 加密串，加密效果为同样的源串及 key 情况下，每次加密的结果不一致。可参考如下示例

    @Slf4j
    public class DESSlatUtilTest {

        @Test
        public void run(){
            for(int i =1;i<=5;i++){
                String s = DESSlatUtil.Encrypt("abc","123",true);
                log.info(s);
                }
            }
        }
可以看到，5次循环生成的密文不一致

    14:26:01.208 [main] INFO cn.qup.infra.pierre.security.encrypt.DESSlatUtilTest - 0FC2A94037B7D7EC71737968
    14:26:01.211 [main] INFO cn.qup.infra.pierre.security.encrypt.DESSlatUtilTest - 86D812054A02365D58303066
    14:26:01.211 [main] INFO cn.qup.infra.pierre.security.encrypt.DESSlatUtilTest - CB93B5AA777D8CBF46615142
    14:26:01.211 [main] INFO cn.qup.infra.pierre.security.encrypt.DESSlatUtilTest - CDF11AC08B43356179746D55
    14:26:01.212 [main] INFO cn.qup.infra.pierre.security.encrypt.DESSlatUtilTest - 71694B62CF02B8EA73615338

### EncryptUtil

提供了 MD/SHA/AES/DES/DES3/RSA 等各类加解密能力。

### SM2KeyPair

提供了国密 II 密钥对生成，加解密能力。

### SM3Util 

提供了国密 III 哈希加密能力。

### SM4Util

提供了国密 IV 加解密能力
