package cn.qup.infra.pierre.core.service.serialize;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

/**
 * 解决Long类型前端精度丢失问题
 * eg:
 *     @ JsonSerialize(using = LongToStringJsonSerializer.class)
 *     private Long id;
 */
public class LongToStringJsonSerializer extends JsonSerializer<Long> {

    @Override
    public void serialize(Long value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        if (value != null){
            gen.writeString(String.valueOf(value));
        }
    }
}
