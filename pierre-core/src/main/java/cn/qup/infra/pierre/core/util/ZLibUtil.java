package cn.qup.infra.pierre.core.util;

import lombok.extern.slf4j.Slf4j;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;

@Slf4j
public class ZLibUtil {
    /**
     * 压缩直接数组
     *
     * @param data 需要压缩的字节数组
     * @return 压缩后的字节数组
     */
    // 压缩直接数组
    public static byte[] compress(byte[] data) {
        byte[] output = new byte[0];
        Deflater compressor = new Deflater();
        compressor.reset();
        compressor.setInput(data);
        compressor.finish();
        ByteArrayOutputStream bos = new ByteArrayOutputStream(data.length);
        try {
            byte[] buf = new byte[1024];
            while (!compressor.finished()) {
                int i = compressor.deflate(buf);
                bos.write(buf, 0, i);
            }
            output = bos.toByteArray();
        } catch (Exception e) {
            output = data;
            log.warn("",e);
//            e.printStackTrace();
        } finally {
            try {
                bos.close();
            } catch (IOException e) {
                log.warn("",e);
//                e.printStackTrace();
            }
        }
        compressor.end();
        return output;
    }

    /**
     * 将字节数组压缩后写入输出流。
     *
     * @param data 待压缩的字节数组
     * @param os 输出流，用于保存压缩后的数据
     */
    // 压缩 字节数组到输出流
    public static void compress(byte[] data, OutputStream os) {
        DeflaterOutputStream dos = new DeflaterOutputStream(os);
        try {
            dos.write(data, 0, data.length);
            dos.finish();
            dos.flush();
        } catch (IOException e) {
            log.warn("",e);
//            e.printStackTrace();
        }
    }

    /**
     * 解压缩字节数组
     *
     * @param data 需要解压缩的字节数组
     * @return 解压缩后的字节数组
     */
    // 解压缩 字节数组
    public static byte[] decompress(byte[] data) {
        byte[] output = new byte[0];
        Inflater inflater = new Inflater();
        inflater.reset();
        inflater.setInput(data);
        ByteArrayOutputStream outputStream  = new ByteArrayOutputStream(data.length);
        try {
            byte[] result  = new byte[1024];
            while (!inflater.finished()) {
                int count  = inflater.inflate(result );
                outputStream .write(result , 0, count );
            }
            output = outputStream .toByteArray();
        } catch (Exception e) {
            output = data;
            log.warn("",e);
//            e.printStackTrace();
        } finally {
            try {
                outputStream .close();
                inflater.end();
            } catch (IOException e) {
                log.warn("",e);
//                e.printStackTrace();
            }
        }

        return output;
    }
    // 解压缩 字节数组
    public static String decompress_str(byte[] data) {
        byte[] output;
        Inflater inflater = new Inflater();
        inflater.reset();
        inflater.setInput(data);
        ByteArrayOutputStream outputStream  = new ByteArrayOutputStream(data.length);
        try {
            byte[] buf = new byte[1024];
            int count = -1;
            while (!inflater.finished() && count==-1 ) {
                count  = inflater.inflate(buf);
                if(count==0){
                    count =-1;
                }
                outputStream.write(buf, 0, count );
            }
            output = outputStream.toByteArray();
        } catch (Exception e) {
            output = data;
            log.warn("",e);
//            e.printStackTrace();
        } finally {
            try {
                outputStream .close();
                inflater.end();
            } catch (IOException e) {
                log.warn("",e);
//                e.printStackTrace();
            }
        }
        return new String(output);
    }
    // 解压缩 输入流 到字节数组
    public static byte[] decompress(InputStream is) {
        InflaterInputStream iis = new InflaterInputStream(is);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(1024);
        try {
            int i = 1024;
            byte[] buf = new byte[i];
            while ((i = iis.read(buf, 0, i)) > 0) {
                outputStream.write(buf, 0, i);
            }
        } catch (IOException e) {
            log.warn("",e);
//            e.printStackTrace();
        }
        return outputStream.toByteArray();
    }
}
