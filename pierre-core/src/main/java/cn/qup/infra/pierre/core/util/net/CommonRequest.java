package cn.qup.infra.pierre.core.util.net;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import cn.qup.infra.pierre.core.annotation.Param;
import cn.qup.infra.pierre.core.enums.MethodType;
import cn.qup.infra.pierre.core.util.MapUtil;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.annotation.JSONField;
import org.springframework.stereotype.Component;
import com.alibaba.fastjson.JSONObject;

@Slf4j
@Component
/**
 * 公共请求类，为所有的请求提供基础功能和属性
 * 该类被标注为一个组件，可以被Spring容器管理
 */
public abstract class CommonRequest<T extends CommonResponse> {

    @Getter
    @JSONField(serialize = false)
    protected boolean debug = false;
    /**
     * 是否处于调试模式
     * @param debug 调试模式开关
     */
    public void setDebug(boolean debug) {
        this.debug = debug;
    }

    @Getter
    @JSONField(serialize = false)
    protected String url;

    @Getter
    @JSONField(serialize = false)
    protected boolean isTraceNetwork = false;
    /**
     * 是否追踪网络请求
     * @param traceNetwork 网络追踪开关
     */
    public void setTraceNetwork(boolean traceNetwork) {
        isTraceNetwork = traceNetwork;
    }

    @Getter
    @JSONField(serialize = false)
    protected String requestId = UUID.randomUUID().toString();
    /**
     * 设置请求的唯一标识
     * @param requestId 请求的唯一ID
     */
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    @Getter
    @JSONField(serialize = false)
    protected MethodType methodType = MethodType.GET;

    @JSONField(serialize = false)
    protected Class<?> clz = JSONObject.class;

    @Getter
    @JSONField(serialize = false)
    protected Map<String,Object> params = new HashMap<>();

    @Getter
    @JSONField(serialize = false)
    protected Map<String,String> headers = new HashMap<>();

    @Getter
    @JSONField(serialize = false)
    protected String pathVariable;
    /**
     * 设置路径变量
     * @param pathVariable 路径变量值
     */
    public void setPathVariable(String pathVariable) {
        this.pathVariable = pathVariable;
    }

    @Getter
    @JSONField(serialize = false)
    protected Object body = new Object();

    private final Map<String,Object> mapBody = new HashMap<>();

    /**
     * 默认构造函数
     */
    public CommonRequest() {
    }

    /**
     * 设置请求参数
     * @param key 参数名
     * @param value 参数值
     */
    protected void setParam(String key ,Object value){
        this.params.put(key,value);
    }

    /**
     * 批量设置请求参数
     * @param map 参数集合
     */
    protected void setParam(Map<String,Object> map){
        this.params = map;
    }

    /**
     * 设置请求头
     * @param key 头部字段名
     * @param value 头部字段值
     */
    public void setHeader(String key, String value){
        this.headers.put(key,value);
    }

    /**
     * 为请求体设置一个键值对
     * @param key 键
     * @param value 值
     */
    protected void setBody(String key, Object value){
        this.mapBody.put(key, value);
        this.body = this.mapBody;
    }

    /**
     * 将对象直接设置为请求体
     * @param map 要设置为请求体的对象
     */
    protected void setBody(Map<String,Object> map) {
        this.mapBody.clear();
        this.mapBody.putAll(map);
        this.body = this.mapBody;
    }

    /**
     * 将字符串设置为请求体
     * @param s 请求体字符串
     */
    protected void setBody(String s){
        this.body =s;
    }

    /**
     * 执行请求
     * @return 响应结果
     */
    @JSONField(serialize = false)
    public <T> T exec()  {

        CommonACSRequest request = new CommonACSRequest();
        this.reflectEntity(this, Param.class,true);
        CommonResponse acsResponse = request.commonHttpRequest(this);

        T res;
        if (!acsResponse.code.equals("0")) {
            res = handleResponse(acsResponse, true);
        } else {
            res = handleResponse(acsResponse, false);
        }
        return res;
    }

    /**
     * 处理响应
     * @param acsResponse 响应对象
     * @param isError 是否为错误
     * @return 处理后的结果
     */
    private <T> T handleResponse(CommonResponse acsResponse, boolean isError) {
        JSONObject jsonObject;
        if (isTraceNetwork) {
            jsonObject = JSON.parseObject(JSON.toJSONString(isError ? acsResponse : acsResponse.data));
            jsonObject.put("trace", MapUtil.cache.get(this.requestId));
            return JSON.parseObject(jsonObject.toJSONString(), (Class<T>) this.clz);
        } else {
            return JSON.parseObject(JSON.toJSONString(isError ? acsResponse : acsResponse.data), (Class<T>) this.clz);
        }
    }

    /**
     * 反射处理实体类
     * @param object 实体对象
     * @param annotation 注解类型
     */
    protected void reflectEntity(Object object, Class<? extends Annotation> annotation){
        reflectEntity(object,annotation,false);
    }

    /**
     * 反射处理实体类，根据注解提取信息
     * @param object 实体对象
     * @param annotation 注解类型
     * @param getNameIfJSONField 是否获取JSONField定义的名称
     */
    protected void reflectEntity(Object object, Class<? extends Annotation> annotation, boolean getNameIfJSONField){
        Class<?> objClass = object.getClass();

        Field[] fields = objClass.getDeclaredFields();
        for(Field field: fields){
            if(field.isAnnotationPresent(annotation)){
                String fieldName ="";
                Object fieldValue = null;
                if(getNameIfJSONField){
                    boolean hasJsonProperty = field.isAnnotationPresent(JSONField.class);
                    if(hasJsonProperty){
                        fieldName = field.getAnnotation(JSONField.class).name();
                    }else{
                        fieldName = field.getName();
                    }
                }else{
                    fieldName = field.getName();
                }
                field.setAccessible(true);
                try{
                   fieldValue = field.get(object);
                }catch (Exception e){
                    log.warn("Failed to access field: " + fieldName, e);
                }
                switch (field.getAnnotation(Param.class).type()){
                    case QUERY_STRING:{
                        if(fieldValue!=null){
                            this.setParam(fieldName,fieldValue);
                        }
                        break;
                    }
                    case POST_ITEM:{
                        if(fieldValue!=null){
                            this.setBody(fieldName,fieldValue);
                        }
                        break;
                    }
                    case HEADER:{
                        if(fieldValue!=null){
                            this.setHeader(fieldName,fieldValue.toString());
                        }
                        break;
                    }
                }
            }
        }
    }

}

