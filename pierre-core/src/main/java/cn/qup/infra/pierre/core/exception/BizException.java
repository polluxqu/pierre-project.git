package cn.qup.infra.pierre.core.exception;

import cn.qup.infra.pierre.core.enums.CommonErrorCode;
import cn.qup.infra.pierre.core.enums.ErrorCode;
import lombok.Getter;
import org.springframework.util.StringUtils;

@Getter
public class BizException extends RuntimeException {
    private final String code;
    private final String msg;
    private final Object data;

    public BizException(String code, String msg) {
        super(msg);
        this.code = code;
        this.msg = msg;
        this.data = null;
    }

    public BizException(String code, String msg, Object data) {
        super(msg);
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public BizException(String code, String msg, Throwable cause) {
        super(msg, cause);
        this.code = code;
        this.msg = msg;
        this.data = null;
    }

    public BizException(CommonErrorCode errorCode) {
        super(errorCode.getMsg());
        this.code = errorCode.getCode();
        this.msg = errorCode.getMsg();
        this.data = null;
    }

    public BizException(CommonErrorCode errorCode, Throwable cause) {
        super(errorCode.getMsg(), cause);
        this.code = errorCode.getCode();
        this.msg = errorCode.getMsg();
        this.data = null;
    }

    public BizException(CommonErrorCode errorCode, String placeHolder) {

        super(String.format(errorCode.getMsg(),placeHolder) );
        this.code = errorCode.getCode();
        this.msg = String.format(errorCode.getMsg(),placeHolder);
        this.data = null;
    }

    public static void throwException(CommonErrorCode baseCodeMsg) {
        throw new BizException(baseCodeMsg.getCode(), baseCodeMsg.getMsg());
    }



    public static void throwException(CommonErrorCode baseCodeMsg, String msg) {
        throw new BizException(baseCodeMsg.getCode(), StringUtils.hasLength(baseCodeMsg.getMsg()) ? msg : baseCodeMsg.getMsg());
    }



    public static void throwException(String code, String msg) {
        throw new BizException(code, msg);
    }


    public static void throwException(String msg) {
        throw new BizException(CommonErrorCode.UNKNOWN_EXCEPTION.getCode(), msg);
    }

}

