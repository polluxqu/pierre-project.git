package cn.qup.infra.pierre.core.annotation;

import cn.qup.infra.pierre.core.enums.ParamType;

import java.lang.annotation.*;

@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Param {
    ParamType type();
}
