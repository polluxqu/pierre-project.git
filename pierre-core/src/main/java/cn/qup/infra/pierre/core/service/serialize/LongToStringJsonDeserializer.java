package cn.qup.infra.pierre.core.service.serialize;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import org.springframework.util.StringUtils;

import java.io.IOException;

/**
 * 解决Long类型前端精度丢失问题
 * eg:
 *     @ JsonDeserialize(using = LongToStringJsonDeserializer.class)
 *     private Long id;
 */
public class LongToStringJsonDeserializer extends JsonDeserializer<Long> {
    @Override
    public Long deserialize(JsonParser p, DeserializationContext ctx) throws IOException {
        String text = p.getText();

        if (StringUtils.hasText(text)) {
            return Long.parseLong(text);
        }

        return null;
    }
}
