package cn.qup.infra.pierre.core.enums;

public enum ParamType {
    QUERY_STRING,
    POST_ITEM,
    HEADER,
}
