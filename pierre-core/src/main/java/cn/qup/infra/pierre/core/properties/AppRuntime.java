package cn.qup.infra.pierre.core.properties;

import cn.qup.infra.pierre.core.enums.AppEnv;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
public class AppRuntime {

    private int instanceId;
    private AppEnv Env;
    private String appId;
    private String appName;
    private String appGroup;
    private String appDescription;
    private String appRootPackageName;
    private boolean encryptContent;
    private boolean swaggerInternalAccessOnly;
}

