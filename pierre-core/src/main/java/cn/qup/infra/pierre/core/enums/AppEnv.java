package cn.qup.infra.pierre.core.enums;

public enum AppEnv {
    local,
    dev,
    test,
    prod
}
