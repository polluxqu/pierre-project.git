package cn.qup.infra.pierre.core.util.net;

import cn.qup.infra.pierre.core.enums.MethodType;
import cn.qup.infra.pierre.core.exception.BizException;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import okhttp3.Response;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;
import java.util.UUID;

/**
 * 通用的ACS请求处理类，封装了HTTP请求的发送和响应处理
 */
@Slf4j
public class CommonACSRequest {

    /**
     * 发送一个通用的HTTP请求
     *
     * @param request 请求对象，包含请求的所有必要信息
     * @return CommonResponse 响应对象，包含响应的所有必要信息
     * @throws BizException 业务异常，当响应状态码不为200时抛出
     */
    public CommonResponse commonHttpRequest(CommonRequest request) {
        CommonResponse response = new CommonResponse();
        try {
            // 获取请求方法类型
            MethodType methodType = request.getMethodType();
            // 获取请求URL
            String url = request.getUrl();
            // 获取请求参数
            Map<String, Object> params = request.getParams();
            // 获取请求体
            Object body = request.getBody();
            // 获取请求头部
            Map<String, String> headers = request.getHeaders();
            // 获取或生成请求ID
            String requestId = request.getRequestId() != null ? request.getRequestId() : UUID.randomUUID().toString();

            // 构建OkHttpUtils实例
            OkHttpUtils httpUtils = OkHttpUtils.builder(request.isTraceNetwork)
                    .url(buildUrl(url, methodType, params, request.pathVariable))
                    .addHeader("pierre-x-request-id", requestId);

            // 添加所有头部信息
            headers.forEach(httpUtils::addHeader);

            // 根据请求方法类型，执行相应的HTTP请求
            switch (methodType) {
                case GET:
                    httpUtils.get();
                    break;
                case POST:
                case PUT:
                    // 处理请求体
                    if (body instanceof String) {
                        httpUtils.addBody((String) body);
                    } else {
                        httpUtils.addBody(JSON.toJSONString(body));
                    }
                    // 执行POST或PUT请求
                    if (methodType.equals(MethodType.POST)) {
                        httpUtils.post(true);
                    } else {
                        httpUtils.put(true);
                    }
                    break;
                case DELETE:
                    httpUtils.delete();
                    break;
            }

            // 同步执行HTTP请求
            httpUtils.sync();

            // 请求调试日志
            if (request.isDebug()) {
                log.info(httpUtils.ResponseBody);
            }
            // 检查HTTP响应
            if (httpUtils.HttpResponse == null) {
                throw new BizException("502", String.format("there is no response from %s", url));
            }
            // 处理不同的HTTP状态码
            int statusCode = httpUtils.HttpResponse.code();
            switch (statusCode) {
                case 400:
                    throw new BizException("400", String.format("bad request: %s", JSON.toJSONString(httpUtils)));
                case 401:
                    throw new BizException("401", httpUtils.ResponseBody);
                case 403:
                    throw new BizException("403", "Access Forbidden: " + url + " message: " + httpUtils.ResponseBody);
                case 404:
                    throw new BizException("404", String.format("url %s not found", url));
                case 200:
                    response.data = httpUtils.ResponseBody;
                    break;
                default:
                    response.code = String.valueOf(statusCode);
                    throw new BizException("500", String.format("response: %s", httpUtils.ResponseBody));
            }

        } catch (BizException e) {
            // 业务异常处理
            response.code = e.getCode();
            response.message = e.getMessage();
        } catch (Exception e) {
            // 其他异常处理
            response.code = "500";
            response.message = e.getMessage();
            log.warn("Unexpected error occurred during request handling.", e);
        }
        return response;
    }

    /**
     * 构建完整的请求URL
     *
     * @param base 基础URL
     * @param methodType 请求方法类型
     * @param params 请求参数
     * @param pathVariable 路径变量
     * @return String 完整的请求URL
     */
    private String buildUrl(String base, MethodType methodType, Map<String, Object> params, String pathVariable) {
        StringBuilder urlBuilder = new StringBuilder(base);
        // 拼接路径变量
        if (pathVariable != null && !pathVariable.isEmpty()) {
            urlBuilder.append(pathVariable);
        }
        // 拼接GET或DELETE请求的参数
        if (!params.isEmpty() && (methodType.equals(MethodType.GET) || methodType.equals(MethodType.DELETE))) {
            urlBuilder.append("?");
            urlBuilder.append(MapQuery.urlEncodeUTF8(params));
        }
        return urlBuilder.toString();
    }
}
