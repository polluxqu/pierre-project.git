package cn.qup.infra.pierre.core.enums;

import lombok.Getter;

@Getter
public enum CommonErrorCode implements ErrorCode {
    SUCCESS("200",""),
    UNKNOWN_EXCEPTION("500","未知异常"),
    TRANSFORM_RESPONSE_EXCEPTION("500","统一返回结构转换异常: %s"),
    NO_AUTHENTICATION_HEADER("401","未找到验证头"),
    NO_SIGNATURE_HEADER("401","未找到签名头"),
    NO_SIGNATURE_ID_HEADER("401","未找到签名Id头"),
    NO_RSA_KEYPAIR("403","对应密钥对"),
    INVALID_SIGNATURE("403","签名无效"),
    SIGNATURE_EXPIRED("403","签名已超时失效"),
    SIGNATURE_INCORRECT("403","签名不匹配"),
    APP_PARAM_ERROR("10001", "请求参数错误"),
    APP_PARAM_NULL("10002", "请求参数为空"),
    APP_RETURN_NULL("10003", "返回数据为空"),
    APP_DATA_NULL("10004", "未查询到相关数据"),
    APP_PASSWORD_ERROR("10005", "密码错误"),
    APP_INVALIDATE_USER("10006", "非法用户"),
    INVOKE_IS_REJECT("10007", "请求被拒绝"),
    API_REQUEST_LIMIT("10008", "指定接口请求限流被触发");

    CommonErrorCode(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }
    /**
     * 异常编码
     */
    private final String code;
    /**
     * 异常消息
     */
    private final String msg;

    public CommonErrorCode resolve(String code) {
        for (CommonErrorCode errorCode : values()) {
            if (errorCode.getCode().equals(code)) {
                return errorCode;
            }
        }
        return null;
    }

    /**
     * @param code error code
     * @return enum
     * @deprecated use {@link CommonErrorCode#resolve(String)}
     */
    @Deprecated
    public CommonErrorCode getMsg(String code) {
        for (CommonErrorCode errorCode : values()) {
            if (errorCode.getCode().equals(code)) {
                return errorCode;
            }
        }
        return null;
    }
}

