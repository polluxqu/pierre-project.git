package cn.qup.infra.pierre.core.util.net;

import cn.qup.infra.pierre.core.pojo.HttpRequestTrace;
import cn.qup.infra.pierre.core.util.MapUtil;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.jetbrains.annotations.NotNull;
import org.springframework.lang.Nullable;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicLong;

@Slf4j
public class NetworkListener extends EventListener {
    /**
     * 自定义EventListener工厂
     */

    private static final String REQUEST_ID = "pierre-x-request-id";

    public static final Factory FACTORY = new Factory() {
        final AtomicLong nextCallId = new AtomicLong(1L);

        @NotNull
        @Override
        public EventListener create(@NotNull Call call) {
//            log.info("Http Network Tracing Start.");
            long callId = nextCallId.getAndIncrement();
            return new NetworkListener(callId, call, System.nanoTime());
        }

    };

    /**
     * 每次请求的标识
     */
    private final long callId;

    /**
     * 每次请求的开始时间，单位纳秒
     */
    private final long callStartNanos;
    private long dnsStartNanos;
    private long connectStartNanos;
    private long secureConnectStartNanos;
    private long requestHeaderStartNanos;
    private long requestBodyStartNanos;
    private long responseHeaderStartNanos;
    private long responseBodyStartNanos;

    private final HttpRequestTrace trace;

    public NetworkListener(long callId,@NotNull Call call, long callStartNanos) {
        this.callId = callId;
        this.callStartNanos = callStartNanos;
        trace = new HttpRequestTrace();
        this.trace.setUrl(call.request().url().toString());
        this.trace.setRequestId(call.request().header(REQUEST_ID));
    }

    private String calcTime(){
        return calcTime(callStartNanos);
//        long elapseNanos = System.nanoTime() - callStartNanos;
//        return String.format(Locale.CHINA, "%.3f", elapseNanos / 1000000d);
    }

    private String calcTime(long start){
        long elapseNanos = System.nanoTime() - start;
        return String.format(Locale.CHINA, "%.3f", elapseNanos / 1000000d);
    }

    @Override
    public void callStart(@NotNull Call call) {
        super.callStart(call);
        this.trace.setCallStart(calcTime());
    }

    @Override
    public void dnsStart(@NotNull Call call, @NotNull String domainName) {
        super.dnsStart(call, domainName);
        this.dnsStartNanos =System.nanoTime();
//        this.trace.setDnsStart(calcTime());
    }

    @Override
    public void dnsEnd(@NotNull Call call, @NotNull String domainName, @NotNull List<InetAddress> inetAddressList) {
        super.dnsEnd(call, domainName, inetAddressList);
//        this.trace.setDnsEnd(calcTime());
        this.trace.setDnsNanos(calcTime(this.dnsStartNanos));
    }

    @Override
    public void connectStart(@NotNull Call call, @NotNull InetSocketAddress inetSocketAddress, @NotNull Proxy proxy) {
        super.connectStart(call, inetSocketAddress, proxy);
        this.connectStartNanos =System.nanoTime();
        this.trace.setRemoteIP(inetSocketAddress.getAddress().getHostAddress());
//        this.trace.setConnectStart(calcTime());;
    }

    @Override
    public void secureConnectStart(@NotNull Call call) {
        super.secureConnectStart(call);
        this.secureConnectStartNanos =System.nanoTime();
//        this.trace.setSecureConnectStart(calcTime());
    }

    @Override
    public void secureConnectEnd(@NotNull Call call, @Nullable Handshake handshake) {
        super.secureConnectEnd(call, handshake);
//        this.trace.setSecureConnectEnd(calcTime());
        this.trace.setSecureConnectionNanos(calcTime(this.secureConnectStartNanos));
    }

    @Override
    public void connectEnd(@NotNull Call call, @NotNull InetSocketAddress inetSocketAddress, @NotNull Proxy proxy, @Nullable Protocol protocol) {
        super.connectEnd(call, inetSocketAddress, proxy, protocol);
//        this.trace.setConnectEnd(calcTime());
        this.trace.setConnectionNanos(calcTime(connectStartNanos));
    }

    @Override
    public void connectFailed(@NotNull Call call, @NotNull InetSocketAddress inetSocketAddress, @NotNull Proxy proxy, @Nullable Protocol protocol, @NotNull IOException ioe) {
        super.connectFailed(call, inetSocketAddress, proxy, protocol, ioe);
//        this.trace.setConnectFailed(calcTime());
        this.trace.setConnectionNanos(calcTime(connectStartNanos));
    }

    @Override
    public void connectionAcquired(@NotNull Call call, @NotNull Connection connection) {
        super.connectionAcquired(call, connection);
//        this.trace.setConnectionAcquired(calcTime());
        this.trace.setConnectionNanos(calcTime(connectStartNanos));
    }

    @Override
    public void connectionReleased(@NotNull Call call, @NotNull Connection connection) {
        super.connectionReleased(call, connection);
//        this.trace.setConnectionReleased(calcTime());
    }

    @Override
    public void requestHeadersStart(@NotNull Call call) {
        super.requestHeadersStart(call);
        this.requestHeaderStartNanos=System.nanoTime();
//        this.trace.setRequestHeadersStart(calcTime());
    }

    @Override
    public void requestHeadersEnd(@NotNull Call call, @NotNull Request request) {
        super.requestHeadersEnd(call, request);
//        this.trace.setRequestHeadersEnd(calcTime());
        this.trace.setRequestHeaders(JSON.toJSONString(request.headers()));
        this.trace.setRequestHeaderNanos(calcTime(this.requestHeaderStartNanos));
    }

    @Override
    public void requestBodyStart(@NotNull Call call) {
        super.requestBodyStart(call);
        this.requestBodyStartNanos =System.nanoTime();
//        this.trace.setRequestBodyStart(calcTime());
    }

    @Override
    public void requestBodyEnd(@NotNull Call call, long byteCount) {
        super.requestBodyEnd(call, byteCount);
//        this.trace.setRequestBodyEnd(calcTime());
        this.trace.setRequestLength(byteCount);
        this.trace.setRequestBody(JSON.toJSONString(call.request().body()));
        this.trace.setRequestBodyNanos(calcTime(this.requestBodyStartNanos));
    }

    @Override
    public void responseHeadersStart(@NotNull Call call) {
        super.responseHeadersStart(call);
        this.responseHeaderStartNanos =System.nanoTime();
//        this.trace.setResponseHeadersStart(calcTime());
    }

    @Override
    public void responseHeadersEnd(@NotNull Call call, @NotNull Response response) {
        super.responseHeadersEnd(call, response);
//        this.trace.setResponseHeadersEnd(calcTime());
        this.trace.setResponseHeaderNanos(calcTime(this.responseHeaderStartNanos));
    }

    @Override
    public void responseBodyStart(@NotNull Call call) {
        super.responseBodyStart(call);
        this.responseBodyStartNanos =System.nanoTime();
//        this.trace.setResponseBodyStart(calcTime());
    }

    @Override
    public void responseBodyEnd(@NotNull Call call, long byteCount) {
        super.responseBodyEnd(call, byteCount);
//        this.trace.setResponseBodyEnd(calcTime());
        this.trace.setResponseLength(byteCount);
        this.trace.setResponseBodyNanos(calcTime(this.responseBodyStartNanos));
    }

    @Override
    public void callEnd(@NotNull Call call) {
        super.callEnd(call);
//        this.trace.setCallEnd(calcTime());
        this.trace.setCallNanos(calcTime());
        MapUtil.cache.put(this.trace.getRequestId(),this.trace);
//        log.info("【Pierre-Trace】: "+ JSON.toJSONString(this.trace));
    }

    @Override
    public void callFailed(@NotNull Call call, @NotNull IOException ioe) {
        super.callFailed(call, ioe);
//        this.trace.setCallFailed(calcTime());
        this.trace.setCallNanos(calcTime());
        MapUtil.cache.put(this.trace.getRequestId(),this.trace);
//        log.info("【Pierre-Trace】: "+ JSON.toJSONString(this.trace));
    }
}
