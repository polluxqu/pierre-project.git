package cn.qup.infra.pierre.core.util.net;

import cn.qup.infra.pierre.core.pojo.HttpRequestTrace;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CommonResponse<T> {
    protected String code ="0";
    protected T data;
    protected String message;
    protected HttpRequestTrace trace;
}

