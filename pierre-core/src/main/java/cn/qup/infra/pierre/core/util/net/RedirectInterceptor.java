package cn.qup.infra.pierre.core.util.net;

import lombok.extern.slf4j.Slf4j;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;

//处理重定向的拦截器
@Slf4j
public class RedirectInterceptor implements Interceptor {
    private static final int RETRY_MAX_COUNT = 3;
    private final int retryCount = 0;

    @NotNull
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Response response = chain.proceed(request);
        int code = response.code();
        String url;
        String location = response.headers().get("Location");
        if (location != null) {
            if (location.contains("http://") || location.contains("https://")) {
                url = location;
            } else {
                url = String.format("%s://%s%s", response.request().url().url().getProtocol(),
                        response.request().url().url().getHost(), location);
            }
            Request newRequest = request.newBuilder().url(url).build();
            switch (code) {
                case 301:
                case 302:
                case 307:
                case 308:
                    //获取重定向的地址
                    //System.out.println(String.format("current url: %s status: %s", url,String.valueOf(response.isSuccessful())));
                    response.close();
                    response = chain.proceed(newRequest);
                    break;
                default:

            }
        }
        return response;
    }
}
