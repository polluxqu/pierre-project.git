package cn.qup.infra.pierre.core.enums;

public interface ErrorCode {
    String getCode();

    String getMsg();
}
