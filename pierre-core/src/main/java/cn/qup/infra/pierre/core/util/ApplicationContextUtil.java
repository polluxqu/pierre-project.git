package cn.qup.infra.pierre.core.util;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.lang.NonNullApi;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class ApplicationContextUtil implements ApplicationContextAware {
    /**
     * 上下文对象实例
     * -- GETTER --
     *  获取applicationContext
     *

     */
    @Getter
    public static ApplicationContext applicationContext;

    /**
     * 设置应用上下文。
     * 此方法将传入的ApplicationContext赋值给静态变量ApplicationContextUtil.applicationContext。
     * 如果在赋值过程中发生BeansException异常，则记录警告日志。
     *
     * @param applicationContext 应用上下文对象，不允许为null。
     */
    @Override
    public void setApplicationContext(@NotNull ApplicationContext applicationContext) {
        try{
            // 尝试更新应用上下文
            ApplicationContextUtil.applicationContext = applicationContext;
        }catch (BeansException e){
            //捕获并记录异常
            log.warn("获取applicationContext失败",e);
        }

    }

    /**
     * 通过name获取 Bean.
     * @param name bean名称
     * @return Bean 对象
     */
    public static Object getBean(String name){
        return getApplicationContext().getBean(name);
    }

    /**
     * 通过class获取Bean.
     * @param clazz bean类型
     * @param <T> 类型
     * @return Bean 对象
     */
    public static <T> T getBean(Class<T> clazz){
        return getApplicationContext().getBean(clazz);
    }

    /**
     * 通过name,以及Clazz返回指定的Bean
     * @param name Bean 名称
     * @param clazz Bean 类型
     * @param <T> 类型
     * @return Bean 对象
     */
    public static  <T> T getBean(String name,Class<T> clazz){
        return getApplicationContext().getBean(name, clazz);
    }

    /**
     * 获取当前应用的包名。
     * 该方法通过分析栈轨迹元素来定位当前主应用的包名，并将其返回。
     *
     * @return 返回当前应用的包名字符串。
     */
    public static String getApplicationPackageName(){
        //设置扫描路径
        String packageName = "";
        // 获取当前主应用包名
        StackTraceElement[] stackTraceElements = new RuntimeException().getStackTrace();
        for (StackTraceElement stackTraceElement : stackTraceElements) {
            if ("main".equals(stackTraceElement.getMethodName())) {
                packageName = stackTraceElement.getClassName();
                break;
            }
        }
        // 移除类名部分，仅保留包名
        packageName = packageName.substring(0, packageName.lastIndexOf("."));
        return packageName;
    }
}
