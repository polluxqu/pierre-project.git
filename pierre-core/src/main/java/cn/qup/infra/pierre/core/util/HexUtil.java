package cn.qup.infra.pierre.core.util;

import java.util.Arrays;
import java.util.Base64;

public class HexUtil {
    private static final char[] HEX_CHARS = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

    /**
     * 十六进制转化为二进制
     */
    public static byte[] hexStrToByteArray(String hexString) {
        if (hexString == null) {
            return null;
        }
        if (hexString.length() == 0) {
            return new byte[0];
        }
        byte[] byteArray = new byte[hexString.length() / 2];
        for (int i = 0; i < byteArray.length; i++) {
            String subStr = hexString.substring(2 * i, 2 * i + 2);
            byteArray[i] = ((byte) Integer.parseInt(subStr, 16));
        }
        return byteArray;
    }

    /**
     * 二进制转化为十六进制
     */
    public static String byteArrayToHexStr(byte[] byteArray) {
        if (byteArray == null) {
            return null;
        }
        char[] hexChars = new char[byteArray.length * 2];
        for (int j = 0; j < byteArray.length; j++) {
            int v = byteArray[j] & 0xFF;
            hexChars[j * 2] = HEX_CHARS[v >>> 4];
            hexChars[j * 2 + 1] = HEX_CHARS[v & 0x0F];
        }
        return new String(hexChars);
    }

    /**
     * byte 数组转化为 Base64 字符串
     * @param data
     * @return
     */
    public static String encode(byte[] data) {
        return Base64.getEncoder().encodeToString(data);
    }

    /**
     * Base64字符串 转换为 byte数组
     */
    public static byte[] decode(String base64) {
        try {
            return Base64.getDecoder().decode(base64);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new byte[0];
    }

    /**
     * 字节数组转 int, 小端，低字节在后
     */
    public static Integer bytesToIntLittleEndian(byte[] bytes){
        if(bytes.length<4){
            bytes = Arrays.copyOf(bytes,4);
        }
        return bytes[0] & 0xFF |
                (bytes[1] & 0xFF) << 8 |
                (bytes[2] & 0xFF << 16) |
                (bytes[3] & 0xFF << 24);
    }

    /**
     * 字节数组转 int, 大端，高字节在后
     */
    public static Integer bytesToIntBigEndian(byte[] bytes){
        if(bytes.length<4){
            bytes = Arrays.copyOf(bytes,4);
        }
        return bytes[3] & 0xFF |
                (bytes[2] & 0xFF) << 8 |
                (bytes[1] & 0xFF << 16) |
                (bytes[0] & 0xFF << 24);
    }
}

