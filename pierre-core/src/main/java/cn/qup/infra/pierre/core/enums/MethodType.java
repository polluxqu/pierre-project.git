package cn.qup.infra.pierre.core.enums;

public enum MethodType {
    GET(false),
    PUT(true),
    POST(true),
    POST_BIN(true),
    DELETE(false),
    HEAD(false),
    OPTIONS(false);

    private final boolean hasContent;
    private MethodType(boolean hasContent) {
        this.hasContent = hasContent;
    }
    public boolean hasContent() {
        return this.hasContent;
    }
}
