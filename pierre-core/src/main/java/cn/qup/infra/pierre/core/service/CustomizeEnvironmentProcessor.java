package cn.qup.infra.pierre.core.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.env.EnvironmentPostProcessor;
import org.springframework.core.env.ConfigurableEnvironment;

import java.util.UUID;

/**
 * 自定义环境处理器，用于在Spring Boot应用启动时，自动设置应用实例ID
 * Author: Pollux.Qu
 * 2024-08-13
 */
public class CustomizeEnvironmentProcessor implements EnvironmentPostProcessor {

    /**
     * 在环境被处理之后，设置应用实例ID
     * 如果系统属性中不存在"spring.application.instanceId"，则生成一个唯一的实例ID并设置为系统属性
     *
     * @param environment 可配置的环境对象，未使用
     * @param application SpringApplication对象，用于获取应用信息或进行应用操作
     */
    @Override
    public void postProcessEnvironment(ConfigurableEnvironment environment, SpringApplication application) {
        // 检查系统属性中是否已设置实例ID
        if(!System.getProperties().containsKey("spring.application.instanceId")){
            // 如果未设置，则生成一个唯一的实例ID，并设置为系统属性
            System.setProperty("spring.application.instanceId", String.valueOf(Math.abs(UUID.randomUUID().hashCode())));
        }
    }
}
