package cn.qup.infra.pierre.core.autoconfiguration;

import cn.qup.infra.pierre.core.enums.AppEnv;
import cn.qup.infra.pierre.core.properties.AppRuntime;
import cn.qup.infra.pierre.core.util.ApplicationContextRunner;
import cn.qup.infra.pierre.core.util.ApplicationContextUtil;
import cn.qup.infra.pierre.core.util.IdWorker;
import com.alibaba.fastjson.JSON;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.cache.CacheProperties;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.cache.CacheManager;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Objects;
import java.util.Random;

@Slf4j
@Configuration
public class PierreCoreAutoConfiguration {

    @Value("${spring.profiles.active:local}")
    private String env;

    @Value("${spring.application.name:pierre-project}")
    private String appName;

    @Value("${spring.application.group:default-group}")
    private String appGroup;

    @Value("${spring.application.id:1}")
    private String appId;

    @Value("${spring.application.description:no description}")
    private String appDescription;

    @Value("${pierre.log.encrypt-content:true}")
    private boolean encryptContent;

    @Value("${pierre.swagger.internal-access-only:true}")
    private boolean swaggerInternalAccessOnly;

    // 由于 pierre-logging 模板依赖 appRuntime 的 instanceId，所以不能在此处才生成 instanceId
    // 需在 implements EnvironmentPostProcessor 中生成，并注入在环境变量的 spring.application.instanceId 中 然后在此处取回
    // 否则 pierre-logging 模板无法正常启动
    private final String instanceId = System.getProperty("spring.application.instanceId");

    @Bean
    @ConditionalOnMissingBean
    AppRuntime appRuntime() {
        AppRuntime appRuntime = new AppRuntime();
        appRuntime.setAppName(appName);
        appRuntime.setAppGroup(appGroup);
        appRuntime.setEnv(AppEnv.valueOf(env));
        appRuntime.setAppId(appId);
        appRuntime.setAppDescription(appDescription);
        appRuntime.setInstanceId(Integer.parseInt(instanceId));
        appRuntime.setEncryptContent(encryptContent);
        appRuntime.setSwaggerInternalAccessOnly(swaggerInternalAccessOnly);
        appRuntime.setAppRootPackageName(ApplicationContextUtil.getApplicationPackageName());
        log.info("【Pierre-Core】: AppRuntime is '{}'", JSON.toJSONString(appRuntime));
        return  appRuntime;
    }

    @Bean
    IdWorker idWorker(){
        Random random = new Random();
        return new IdWorker(random.nextInt(1023));
    }


}
