package cn.qup.infra.pierre.core.util;

import lombok.extern.slf4j.Slf4j;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

@Slf4j
public class ReflectionUtils {

    /**
     * 获取指定对象的所有注解
     * @param obj 目标对象
     * @return 注解列表
     */
    public static Annotation[] getAnnotations(Object obj) {
        return obj.getClass().getAnnotations();
    }

    /**
     * 获取指定对象的所有字段
     * @param obj 目标对象
     * @return 字段列表
     */
    public static Field[] getFields(Object obj) {
        return obj.getClass().getDeclaredFields();
    }

    /**
     * 获取指定对象的所有方法名
     * @param obj 目标对象
     * @return 方法名列表
     */
    public static String[] getMethodNames(Object obj) {
        Method[] methods = obj.getClass().getDeclaredMethods();
        return Arrays.stream(methods).map(Method::getName).toArray(String[]::new);
    }

    /**
     * 获取指定对象的指定字段的值
     * @param obj 目标对象
     * @param fieldName 字段名
     * @return 字段值，如果字段不存在则返回null
     */
    public static Object getValue(Object obj, String fieldName) {
        try {
            Field field = obj.getClass().getDeclaredField(fieldName);
            field.setAccessible(true);
            return field.get(obj);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            log.warn("",e);
            return null;
        }
    }

    /**
     * 反射调用指定类中的方法，并返回方法执行的结果。如果方法不存在或参数不匹配，则返回null。
     * @param obj 目标对象，如果为null则表示调用静态方法。
     * @param methodName 方法名。
     * @param parameterTypes 参数类型列表。如果为null，则表示无参方法。如果参数为[]，则表示可变参数方法。
     * @return 方法执行结果，如果方法不存在或参数不匹配则返回null。
     */
    public static Object invokeMethod(Object obj, String methodName, Class<?>... parameterTypes) {
        try {
            Method method = obj == null ? Class.forName(methodName).getDeclaredMethod(methodName) : obj.getClass().getDeclaredMethod(methodName, parameterTypes);
            method.setAccessible(true);
            return method.invoke(obj, (Object[]) null); // 如果参数为[]，则表示可变参数方法，使用null代替参数列表。
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            log.warn("",e);
            return null;
        } catch (ClassNotFoundException e) { // 如果方法名为类名，需要额外处理ClassNotFoundException异常。
            log.warn("",e);
            return null;
        }
    }
}
