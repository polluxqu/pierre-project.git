package cn.qup.infra.pierre.core.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringApplicationRunListener;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;

import java.time.Duration;

@Slf4j
@Component
public class ApplicationContextRunner implements SpringApplicationRunListener {

    public ApplicationContextRunner(SpringApplication sa, String[] args){
    }

    @Override
    public void ready(ConfigurableApplicationContext context, Duration timeTaken) {
        ApplicationContextUtil.applicationContext = context;
    }

    @Override
    public void started(ConfigurableApplicationContext context, Duration timeTaken) {
//        SpringApplicationRunListener.super.started(context, timeTaken);
        log.info(String.format("【Pierre-Core】ApplicationContext %s started, during %s seconds.",context.getId(),timeTaken.getSeconds()));
    }
}

