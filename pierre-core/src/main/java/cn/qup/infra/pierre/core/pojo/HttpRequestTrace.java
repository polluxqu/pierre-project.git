package cn.qup.infra.pierre.core.pojo;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class HttpRequestTrace {
    private String url;

    @JSONField(ordinal = 1)
    private String requestId;

    @JSONField(ordinal = 2)
    private String callStart;

    @JSONField(ordinal = 3)
    private String callNanos;

//    @JSONField(ordinal = 3)
//    private String dnsStart;
//
//    @JSONField(ordinal = 4)
//    private String dnsEnd;
//
//    @JSONField(ordinal = 5)
//    private String connectStart;
//
//    @JSONField(ordinal = 6)
//    private String secureConnectStart;
//
//    @JSONField(ordinal = 7)
//    private String secureConnectEnd;
//
//    @JSONField(ordinal = 8)
//    private String connectEnd;
//
//    @JSONField(ordinal = 9)
//    private String connectFailed;
//
//    @JSONField(ordinal = 10)
//    private String connectionAcquired;
//
//    @JSONField(ordinal = 19)
//    private String connectionReleased;
//
//    @JSONField(ordinal = 11)
//    private String requestHeadersStart;
//
//    @JSONField(ordinal = 12)
//    private String requestHeadersEnd;
//
//    @JSONField(ordinal = 13)
//    private String requestBodyStart;
//
//    @JSONField(ordinal = 14)
//    private String requestBodyEnd;
//
//    @JSONField(ordinal = 15)
//    private String responseHeadersStart;
//
//    @JSONField(ordinal = 16)
//    private String responseHeadersEnd;
//
//    @JSONField(ordinal = 17)
//    private String responseBodyStart;
//
//    @JSONField(ordinal = 18)
//    private String responseBodyEnd;
//
//    @JSONField(ordinal = 20)
//    private String callEnd;
//
//    @JSONField(ordinal = 20)
//    private String callFailed;

    @JSONField(ordinal = 21)
    private String requestHeaders;

    @JSONField(ordinal = 22)
    private String requestBody;

    @JSONField(ordinal = 23)
    private String remoteIP;

    @JSONField(ordinal = 24)
    private long requestLength;

    @JSONField(ordinal = 25)
    private long responseLength;

    @JSONField(ordinal = 4)
    private String dnsNanos;

    @JSONField(ordinal = 5)
    private String connectionNanos;

    @JSONField(ordinal = 6)
    private String secureConnectionNanos;

    @JSONField(ordinal = 7)
    private String requestHeaderNanos;

    @JSONField(ordinal = 8)
    private String requestBodyNanos;

    @JSONField(ordinal = 9)
    private String responseHeaderNanos;

    @JSONField(ordinal = 10)
    private String responseBodyNanos;
}
