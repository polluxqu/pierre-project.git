package cn.qup.infra.pierre.core.pojo;

import lombok.Data;

import java.io.Serializable;

@Data
public class ApiResult<T> implements Serializable {

    private static final long serialVersionUID = -1L;

    private Integer code = 0;
    private String message="";
    private T data ;

    public ApiResult(Integer code, String message, T data){
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public ApiResult() {

    }
}
