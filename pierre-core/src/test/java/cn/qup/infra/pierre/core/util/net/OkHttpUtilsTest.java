package cn.qup.infra.pierre.core.util.net;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
class OkHttpUtilsTest {

    @Test
    public void run(){
        OkHttpUtils httpUtils = OkHttpUtils.builder(true)
                .url("https://www.baidu.com");
        httpUtils.get().sync();
        log.info(httpUtils.ResponseBody);
    }

}
