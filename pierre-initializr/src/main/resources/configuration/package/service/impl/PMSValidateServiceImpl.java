package {{packageName}};

import cn.qup.infra.pierre.springbootstarter.pojo.ValidateResponse;
import cn.qup.infra.pierre.springbootstarter.service.IPMSValidateService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
public class PMSValidateServiceImpl implements IPMSValidateService {

    /**
     * 对于使用 @KeyAuthNeeds 注解的接口进行签名验证方法实现
     * @param request 透传过来的请求
     * @return
     */
    @Override
    public ValidateResponse pmsKeyAuthValidate(HttpServletRequest request) {

        // TODO 需完成自定义的 PMSValidateService 实现
        // 自定义的 KeyAuth 注解接口验证
        // 直接验证通过，并对 Request 中注入 header pms-x-token
        ValidateResponse response = new ValidateResponse();
        response.setCode("0"); // 或者 setCode("200")，此处为兼容前端，支持 200 为成功代码

        // 鉴权成功后可注入其它信息，如用户角色等，方便 Controller 层的 RequestMapping 接口方法中进行使用
        Map<String,String> map = new HashMap<>();
        map.put("pms-x-token","abcd");
        response.setInsertHeaders(map);

//        鉴权结果如果为不通过，可指定输出结果为指定异常
        response.setCode("403");
        response.setMessage("访问被拒绝");

        return response;
    }

    /**
     * 对于使用 @SignatureNeeds 注解的接口进行签名验证方法实现
     * @param request 透传过来的请求
     * @return
     */
    @Override
    public ValidateResponse pmsSignatureValidate(HttpServletRequest request) {
        return null;
    }
}
