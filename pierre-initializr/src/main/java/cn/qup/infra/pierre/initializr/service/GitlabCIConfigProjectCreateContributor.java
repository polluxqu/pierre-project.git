package cn.qup.infra.pierre.initializr.service;

import io.spring.initializr.generator.project.contributor.SingleResourceProjectContributor;

public class GitlabCIConfigProjectCreateContributor extends SingleResourceProjectContributor {

    public GitlabCIConfigProjectCreateContributor() {
        this("classpath:configuration/ci/.gitlab-ci.yml");
    }

    public GitlabCIConfigProjectCreateContributor(String resourcePattern) {
        super(".gitlab-ci.yml", resourcePattern);
    }
}

