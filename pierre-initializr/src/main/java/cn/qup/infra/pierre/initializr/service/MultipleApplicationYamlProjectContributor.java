package cn.qup.infra.pierre.initializr.service;

import cn.qup.infra.pierre.initializr.config.ApplicationConfigurationFile;
import io.spring.initializr.generator.project.contributor.ProjectContributor;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Set;

@Slf4j
public class MultipleApplicationYamlProjectContributor implements ProjectContributor {
    private final Set<ApplicationConfigurationFile> applicationConfigurationFiles;

    public MultipleApplicationYamlProjectContributor(Set<ApplicationConfigurationFile> applicationConfigurationFiles) {
        this.applicationConfigurationFiles = applicationConfigurationFiles;
    }

    @Override
    public void contribute(Path projectRoot) throws IOException {
        for (ApplicationConfigurationFile configurationFile : applicationConfigurationFiles) {
            Path file = Files.createFile(projectRoot.resolve(configurationFile.getTargetFile()));
            try (PrintWriter writer = new PrintWriter(Files.newBufferedWriter(file))) {
                configurationFile.write(writer);
            }
        }
    }
}

