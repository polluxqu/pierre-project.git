package cn.qup.infra.pierre.initializr.service;

import io.spring.initializr.generator.project.contributor.ProjectContributor;
import io.spring.initializr.metadata.InitializrMetadata;
import io.spring.initializr.metadata.InitializrMetadataProvider;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEventPublisher;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@Slf4j
public class ApplicationPropertiesDeleteContributor implements ProjectContributor {

    @Override
    public void contribute(Path projectRoot) throws IOException {
        try {
            Files.deleteIfExists(projectRoot.resolve("src/main/resources/application.properties"));
        } catch (IOException e) {
            // ignore
            log.warn("", e);
        }
    }


    @Override
    public int getOrder() {
        return LOWEST_PRECEDENCE;
    }
}

