package cn.qup.infra.pierre.initializr.config;

import cn.qup.infra.pierre.initializr.properties.PierreInitializrProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties({PierreInitializrProperties.class})
public class PierreInitializrAutoConfiguration {
}
