package cn.qup.infra.pierre.initializr.config;

import cn.qup.infra.pierre.initializr.service.ApplicationPropertiesDeleteContributor;
import cn.qup.infra.pierre.initializr.service.GitlabCIConfigProjectCreateContributor;
import cn.qup.infra.pierre.initializr.service.MultipleApplicationYamlProjectContributor;
import cn.qup.infra.pierre.initializr.service.PierrePackageContributor;
import io.spring.initializr.generator.io.template.MustacheTemplateRenderer;
import io.spring.initializr.generator.language.Annotation;
import io.spring.initializr.generator.language.TypeDeclaration;
import io.spring.initializr.generator.project.ProjectDescription;
import io.spring.initializr.generator.project.ProjectGenerationConfiguration;
import io.spring.initializr.generator.spring.code.MainApplicationTypeCustomizer;
import io.spring.initializr.generator.spring.scm.git.GitIgnoreCustomizer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;

import java.util.Set;

@Slf4j
@ProjectGenerationConfiguration
public class InitializrConfiguration {

    private final ProjectDescription description;

    public InitializrConfiguration(ProjectDescription description) {
        this.description = description;
    }

    @Bean
    public GitlabCIConfigProjectCreateContributor gitlabCIConfigProjectCreateContributor() {
        return new GitlabCIConfigProjectCreateContributor();
    }

    @Bean
    public ApplicationPropertiesDeleteContributor applicationYamlContributor() {
        return new ApplicationPropertiesDeleteContributor();
    }

    @Bean
    public MultipleApplicationYamlProjectContributor multipleApplicationYamlProjectContributor(Set<ApplicationConfigurationFile> configurationFiles) {
        return new MultipleApplicationYamlProjectContributor(configurationFiles);
    }

    @Bean
    ApplicationConfigurationFile applicationYaml() {
        return new ApplicationConfigurationFile(
                new MustacheTemplateRenderer("classpath:/configuration/yaml"),
                description,
                "application",
                "src/main/resources/application.yml");
    }

    @Bean
    ApplicationConfigurationFile applicationDefaultYaml() {
        return new ApplicationConfigurationFile(
                new MustacheTemplateRenderer("classpath:/configuration/yaml"),
                description,
                "application-default",
                "src/main/resources/application-local.yml");
    }

    @Bean
    ApplicationConfigurationFile applicationDevYaml() {
        return new ApplicationConfigurationFile(
                new MustacheTemplateRenderer("classpath:/configuration/yaml"),
                description,
                "application-dev",
                "src/main/resources/application-dev.yml");
    }

    @Bean
    ApplicationConfigurationFile applicationTestYaml() {
        return new ApplicationConfigurationFile(
                new MustacheTemplateRenderer("classpath:/configuration/yaml"),
                description,
                "application-test",
                "src/main/resources/application-test.yml");
    }

    @Bean
    ApplicationConfigurationFile applicationProdYaml() {
        return new ApplicationConfigurationFile(
                new MustacheTemplateRenderer("classpath:/configuration/yaml"),
                description,
                "application-prod",
                "src/main/resources/application-prod.yml");
    }

    @Bean
    public GitIgnoreCustomizer loggingGitIgnoreCustomizer() {
        return gitIgnore -> gitIgnore.getGeneral().add("*.log", "*.gz");
    }

    @Bean
    public MainApplicationTypeCustomizer<TypeDeclaration> swaggerEnable(){
        return typeDeclaration -> {
            typeDeclaration.annotate(Annotation.name("springfox.documentation.oas.annotations.EnableOpenApi"));
        };
    }


//    @Bean
//    @ConditionalOnRequestedDependency("pierre-mybatis-plus")
//    public MainApplicationTypeCustomizer<TypeDeclaration> excludeMPJInjector(){
//        return typeDeclaration -> {
//            typeDeclaration.annotate(Annotation.name("org.springframework.boot.autoconfigure.SpringBootApplication", attrib -> {
//                attrib.attribute("exclude", ArrayList.class,"MPJSqlInjector.class");
//            }));
//        };
//    }

    @Bean
    public MainApplicationTypeCustomizer<TypeDeclaration> PMSAutoRegisterEnable(){
        return typeDeclaration -> {
            typeDeclaration.annotate(Annotation.name("cn.qup.infra.pierre.springbootstarter.annotation.EnableAutoRegisterPMS"));
        };
    }

    @Bean
    public PierrePackageContributor pierrePackageContributor() {
        return new PierrePackageContributor(description);
    }
}
