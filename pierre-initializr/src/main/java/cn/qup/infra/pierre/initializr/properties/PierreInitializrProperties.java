package cn.qup.infra.pierre.initializr.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "pierre")
public class PierreInitializrProperties {
    private String version = "2.0-SNAPSHOT";
}

