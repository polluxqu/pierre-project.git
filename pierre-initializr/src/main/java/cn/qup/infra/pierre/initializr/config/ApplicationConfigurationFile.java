package cn.qup.infra.pierre.initializr.config;

import io.spring.initializr.generator.io.template.MustacheTemplateRenderer;
import io.spring.initializr.generator.io.text.MustacheSection;
import io.spring.initializr.generator.project.ProjectDescription;
import lombok.Getter;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class ApplicationConfigurationFile {
    private final MustacheSection application;

    @Getter
    private final String targetFile;

    public ApplicationConfigurationFile(MustacheTemplateRenderer templateRenderer,
                                        ProjectDescription description,
                                        String templateName,
                                        String targetFile) {
        Map<String, Object> params = new HashMap<>();
        params.put("project", description);
        params.putIfAbsent("appId", Math.abs(Objects.hashCode(description.getName())));
        params.putIfAbsent("appDescription",description.getApplicationName());
        this.application = new MustacheSection(templateRenderer, templateName, params);
        this.targetFile = targetFile;
    }

    public void write(PrintWriter writer) {
        application.write(writer);
    }

}
