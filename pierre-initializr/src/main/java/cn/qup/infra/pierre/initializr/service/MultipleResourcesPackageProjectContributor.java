package cn.qup.infra.pierre.initializr.service;

import io.spring.initializr.generator.language.SourceStructure;
import io.spring.initializr.generator.project.ProjectDescription;
import io.spring.initializr.generator.project.contributor.ProjectContributor;
import lombok.AllArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * 多种资源复制到包根目录下的项目创建器
 */
@AllArgsConstructor
public class MultipleResourcesPackageProjectContributor implements ProjectContributor  {
    private final PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();

    private final ProjectDescription description;

    /**
     * 待复制的资源位置
     */
    private final String rootResource;

    @Override
    public void contribute(Path projectRoot) throws IOException {
        Path packageRoot = getPackageRoot(projectRoot);

        Resource root = this.resolver.getResource(this.rootResource);
        Resource[] resources = this.resolver.getResources(this.rootResource + "/**");
        for (Resource resource : resources) {
            if (resource.isReadable()) {
                String filename = extractFileName(root.getURI(), resource.getURI());
                Path output = packageRoot.resolve(filename);
                Files.createDirectories(output.getParent());
                Files.createFile(output);
                // TODO 此处可增加对默认 package 中的 .java 文件进行类似 {{packageName}} 的占位符属性替换的工作
                FileCopyUtils.copy(resource.getInputStream(), Files.newOutputStream(output));
            }
        }
    }

    private Path getPackageRoot(Path projectRoot) {
        SourceStructure structure = this.description.getBuildSystem().getMainSource(projectRoot,
                this.description.getLanguage());
        return structure.getSourcesDirectory().resolve(this.description.getPackageName().replace('.', '/'));
    }

    private String extractFileName(URI root, URI resource) {
        String candidate = resource.toString().substring(root.toString().length());
        return StringUtils.trimLeadingCharacter(candidate, '/');
    }
}
