package cn.qup.infra.pierre.initializr.config;

import cn.qup.infra.pierre.initializr.properties.PierreInitializrProperties;
import io.spring.initializr.generator.buildsystem.Build;
import io.spring.initializr.generator.buildsystem.Dependency;
import io.spring.initializr.generator.buildsystem.maven.MavenBuild;
import io.spring.initializr.generator.buildsystem.maven.MavenBuildSystem;
import io.spring.initializr.generator.condition.ConditionalOnBuildSystem;
import io.spring.initializr.generator.project.ProjectGenerationConfiguration;
import io.spring.initializr.generator.spring.build.BuildCustomizer;
import io.spring.initializr.generator.version.VersionProperty;
import io.spring.initializr.generator.version.VersionReference;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

@ProjectGenerationConfiguration
@EnableConfigurationProperties({PierreInitializrProperties.class})
public class DependenciesConfiguration {
//    @Bean
//    public BuildCustomizer<Build> pierreDependencies() {
//        return build -> build.boms().add("pierre-dependencies",
//                BillOfMaterials.withCoordinates("com.qinyibang.infra", "pierre-dependencies")
//                        .version(VersionReference.ofProperty(VersionProperty.of("pierre.version")))
//                        .build());
//    }

    @Bean
    public BuildCustomizer<Build> pierreStarter() {
        return build -> build.dependencies().add("pierre-starter",
                Dependency.withCoordinates("cn.qup.infra", "pierre-spring-boot-starter")
                        .version(VersionReference.ofProperty(VersionProperty.of("pierre.version"))));
    }

//    @Bean
//    @ConditionalOnRequestedDependency("web")
//    public BuildCustomizer<Build> pierreWeb() {
//        return build -> build.dependencies().add("pierre-web",
//                Dependency.withCoordinates("com.wangxiaobao.infra", "pierre-web"));
//    }

//    @Bean
//    public BuildCustomizer<Build> pierreOpenfeign() {
//        return build -> build.settings().
//    }

    @Bean
    public BuildCustomizer<Build> pierreVersion(PierreInitializrProperties pierreInitializrProperties) {
        return build -> build.properties().version("pierre.version", pierreInitializrProperties.getVersion())
                .version("java.version","11");
    }

    @Bean
    @ConditionalOnBuildSystem(MavenBuildSystem.ID)
    public BuildCustomizer<MavenBuild> buildInfo() {

        return build -> {
            if (!build.plugins().has("org.springframework.boot", "spring-boot-maven-plugin")) {
                return;
            }

            build.plugins().add("org.springframework.boot", "spring-boot-maven-plugin",
                    plugin -> plugin.execution("build-info", execution -> execution.goal("build-info")
                            )
                            .configuration(config -> config.add("excludes",
                                    excludes -> excludes.add("exclude",exclude ->
                                            exclude.add("groupId","org.projectlombok")
                                                    .add("artifactId","lombok"))))
            );
            build.plugins().add("pl.project13.maven", "git-commit-id-plugin", plugin -> plugin
                    .configuration(config -> config
                            .add("failOnNoGitDirectory", "false")
                            .add("offline", "true")
                    )
            );
        };
    }
}
