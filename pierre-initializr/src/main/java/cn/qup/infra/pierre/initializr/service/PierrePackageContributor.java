package cn.qup.infra.pierre.initializr.service;

import io.spring.initializr.generator.project.ProjectDescription;
public class PierrePackageContributor extends MultipleResourcesPackageProjectContributor {

    public PierrePackageContributor(ProjectDescription description){
        super(description, "classpath:configuration/package");
    }
}
