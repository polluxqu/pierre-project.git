# Pierre-Project

#### 介绍
自用JAVA开发脚手架


#### 安装教程

1. git clone https://gitee.com/polluxqu/pierre-project.git
2. maven 工具对主项目 pierre-project 进行 install
3. 修改主模块中 pom.xml 中 distributionManagement 模块的 repository 配置，指向自己的maven私仓
4. 使用 maven 工具对主项目 pierre-project 进行 deploy
5. 在新项目中的 pom.xml 中添加依赖

#### 使用说明

1.  [Pierre-Core](docs/pierre-core.md)
2.  [Pierre-Config](docs/pierre-config.md)
2.  [Pierre-Spring-Boot-Starter](docs/pierre-spring-boot-starter.md)
3.  [Pierre-Security](docs/pierre-security.md)
4.  [Pierre-Trace](docs/pierre-trace.md)

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

