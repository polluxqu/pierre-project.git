package cn.qup.infra.pierre.logging.util;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.Appender;
import ch.qos.logback.core.rolling.RollingFileAppender;
import ch.qos.logback.core.rolling.RollingPolicy;
import ch.qos.logback.core.rolling.RollingPolicyBase;
import ch.qos.logback.core.rolling.TriggeringPolicy;
import ch.qos.logback.core.spi.AppenderAttachableImpl;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;

@Slf4j
public class LogbackUtil {
    private static final Logger logger = LoggerFactory.getLogger(LogbackUtil.class);

    // 获取从logger到RootLogger的所有appenders
    public static ArrayList<Appender> getAppendersTraversal(Logger logger){
        Logger parent = logger;
        ArrayList<Appender> result = new ArrayList<>();
        while(parent != null){
            ArrayList<Appender> tempResult = getAppenders(parent);
            if (tempResult != null){
                result.addAll(tempResult);
            }

            parent = getParent(parent);
        }
        return result;
    }

    // 获取logger中的appenders
    public static ArrayList<Appender> getAppenders(Logger logger){
        AppenderAttachableImpl<ILoggingEvent> aai;
        ArrayList<Appender> fileAppenders = new ArrayList<>();
        try {
            Class<?> clazz = ch.qos.logback.classic.Logger.class;
            Field aaiField = clazz.getDeclaredField("aai");
            aaiField.setAccessible(true);
            AppenderAttachableImpl<ILoggingEvent> markedAai;
            aai =  (AppenderAttachableImpl<ILoggingEvent>)aaiField.get(logger);
            if (aai == null){
                return null;
            }
            Iterator<Appender<ILoggingEvent>> aaiIterator = aai.iteratorForAppenders();
            while (aaiIterator.hasNext()){
                Appender<ILoggingEvent> appender = aaiIterator.next();
                fileAppenders.add(appender);
            }
            return fileAppenders;
        } catch (NoSuchFieldException | IllegalAccessException e){
            log.warn("getAppenders error", e);
//            e.printStackTrace();
            return fileAppenders;
        }
    }

    // 获取logger的parent logger
    public static Logger getParent(Logger logger) {
        Class<?> clazz = ch.qos.logback.classic.Logger.class;
        try {
            Field parentField = clazz.getDeclaredField("parent");
            parentField.setAccessible(true);
            Logger parent = (Logger)parentField.get(logger);
            return parent;
        } catch (NoSuchFieldException | IllegalAccessException e){
            log.warn("getParent error", e);
            return null;
        }
    }

    public static RollingFileAppender getRollingFileAppender(Appender appender){
        if (appender instanceof RollingFileAppender){
            return (RollingFileAppender)appender;
        }
        return null;
    }

    public static RollingFileAppender updateRollingPolicyToMark(RollingFileAppender rollingFileAppender, String newFile, String newPattern){
        RollingPolicy rollingPolicy = rollingFileAppender.getRollingPolicy();
        TriggeringPolicy triggeringPolicy = rollingFileAppender.getTriggeringPolicy();
        RollingPolicyBase rollingPolicyBase = (RollingPolicyBase)rollingPolicy;
        rollingPolicyBase.setFileNamePattern(newPattern);

        // stop all:appender + policy
        stopRollingFileAppender(rollingFileAppender);
        // set fileName
        setFileName(rollingFileAppender, newFile);
        // start
        startRollingFileAppender(rollingFileAppender);

        return rollingFileAppender;
    }

    private static void stopRollingFileAppender(RollingFileAppender rollingFileAppender){
        rollingFileAppender.stop();
    }

    private static void startRollingFileAppender(RollingFileAppender rollingFileAppender){
        rollingFileAppender.getTriggeringPolicy().start();
        rollingFileAppender.getRollingPolicy().start();
        rollingFileAppender.start();
    }

    private static void setFileName(RollingFileAppender rollingFileAppender, String fileName){
        RollingPolicy rollingPolicy = rollingFileAppender.getRollingPolicy();
        TriggeringPolicy triggeringPolicy = rollingFileAppender.getTriggeringPolicy();
        // remove policy
        rollingFileAppender.setRollingPolicy(null);
        rollingFileAppender.setTriggeringPolicy(null);

        // set fileName
        rollingFileAppender.setFile(fileName);

        // set policy
        rollingFileAppender.setRollingPolicy(rollingPolicy);
        rollingFileAppender.setTriggeringPolicy(triggeringPolicy);
    }
}
