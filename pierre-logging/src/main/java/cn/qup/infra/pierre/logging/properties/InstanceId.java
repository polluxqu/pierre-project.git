package cn.qup.infra.pierre.logging.properties;

import ch.qos.logback.core.PropertyDefinerBase;
import org.springframework.stereotype.Component;

@Component
public class InstanceId extends PropertyDefinerBase {

    @Override
    public String getPropertyValue() {
        return System.getProperty("spring.application.instanceId");
    }
}
