Pierre-Trace

Pierre-Trace 是利用AOP切面实现的一个应用监控增强，用于对应用内指定方法进行运行效率监测的组件，POM 引用方式如下

        <dependency>
            <groupId>cn.qup.infra</groupId>
            <artifactId>pierre-trace</artifactId>
            <version>${pierre-version}</version>
        </dependency>
        
引用后在被引用应用的 application.yml 新增配置块

pierre:
  trace:
    trace-annotation: "@annotation(cn.qup.infra.pierre.trace.annotation.CalculateExecuteTime)"
    
其中，trace-annotation 配置项为标准的 AOP 切面表达式，Pierre-Trace 中提供了默认的被监控方法注解 CalculateExecuteTime，
您也可以自定义自己的注解或者使用其它现有的注解对其进行应用性能监控。
