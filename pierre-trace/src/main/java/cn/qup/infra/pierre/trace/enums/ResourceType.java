package cn.qup.infra.pierre.trace.enums;

public enum ResourceType {
    Private, Public
}
