package cn.qup.infra.pierre.trace.config;

import cn.qup.infra.pierre.trace.properties.MyKafkaProperties;
import cn.qup.infra.pierre.trace.properties.TraceProperties;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.context.annotation.*;
import org.springframework.util.ObjectUtils;

import java.util.Properties;

@Slf4j
@Configuration
@Import(PierreDynamicAOPConfig.class)
@Role(RootBeanDefinition.ROLE_INFRASTRUCTURE)
public class AspectAutoConfiguration {

    @Bean
    @Role(RootBeanDefinition.ROLE_INFRASTRUCTURE)
    MyKafkaProperties myKafkaProperties(){
        return new MyKafkaProperties();
    }

    /**
     * 创建并配置一个KafkaProducer实例，用于发送String类型的键值对消息。
     * 这个方法依赖于MyKafkaProperties来获取Kafka的配置信息。
     * 如果MyKafkaProperties中的bootStrapServers和topics不为空，将根据这些配置创建一个KafkaProducer实例。
     * 否则，将返回null。
     *
     * @return KafkaProducer<String, String> 如果配置完整，则返回一个配置好的KafkaProducer实例；否则返回null。
     */
    @Bean
    @Role(RootBeanDefinition.ROLE_INFRASTRUCTURE)
    KafkaProducer<String, String> kafkaProducer(){
        MyKafkaProperties myKafkaProperties = new MyKafkaProperties();
        // 根据提供的配置信息，判断是否可以创建KafkaProducer实例
        if (!(ObjectUtils.isEmpty(myKafkaProperties.getBootStrapServers()) && ObjectUtils.isEmpty(myKafkaProperties.getTopics()))) {
            Properties properties = new Properties();
            // 设置KafkaProducer的配置属性
            properties.setProperty("bootstrap.servers",myKafkaProperties.getBootStrapServers());
            properties.setProperty("topics",myKafkaProperties.getTopics());
            properties.setProperty("acks",myKafkaProperties.getAcks());
            properties.setProperty("retries",String.valueOf(myKafkaProperties.getRetries()));
            properties.setProperty("linger.ms",String.valueOf(myKafkaProperties.getLinger()));
            properties.setProperty("batch.size",String.valueOf(myKafkaProperties.getBatchSize()));
            properties.setProperty("buffer.memory",String.valueOf(myKafkaProperties.getBuffer()));
            // 创建并返回配置好的KafkaProducer实例
            return new KafkaProducer<>(properties);
        }else{
            // 如果必要的配置信息缺失，则返回null
            return null;
        }
    }


    @Bean
    @Role(RootBeanDefinition.ROLE_INFRASTRUCTURE)
    TraceProperties traceProperties(){
        //        log.info("【Pierre-Trace】: trace expression {}", JSON.toJSONString(traceProperties.getExpression()));
        return new TraceProperties();
    }


}
