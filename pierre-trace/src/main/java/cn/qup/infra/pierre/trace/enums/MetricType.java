package cn.qup.infra.pierre.trace.enums;

public enum MetricType {
    Count, Gauge, Amount
}
