package cn.qup.infra.pierre.trace.enums;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.TimeUnit;

public enum AggregateUnit {
    MINUTE {
        public long truncateTime(long timestamp) {
            long minutes = TimeUnit.MILLISECONDS.toMinutes(timestamp);
            return TimeUnit.MINUTES.toMillis(minutes);
        }
    },
    HOUR {
        public long truncateTime(long timestamp) {
            long hours = TimeUnit.MILLISECONDS.toHours(timestamp);
            return TimeUnit.HOURS.toMillis(hours);
        }
    },
    DAY {
        public long truncateTime(long timestamp) {
            ZonedDateTime t = ZonedDateTime.ofInstant(Instant.ofEpochMilli(timestamp), DEFAULT_TIME_ZONE);
            long seconds = t.truncatedTo(ChronoUnit.DAYS).getLong(ChronoField.INSTANT_SECONDS);
            return TimeUnit.SECONDS.toMillis(seconds);
        }
    };

    public long truncateTime(long timestamp) {
        throw new AbstractMethodError();
    }

    private static ZoneId DEFAULT_TIME_ZONE = ZoneId.of("Asia/Shanghai");
}
