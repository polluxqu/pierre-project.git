package cn.qup.infra.pierre.trace.pojo;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class HttpRequestTrace {
    private String url;

    @JSONField(ordinal = 1)
    private String callStart;

    @JSONField(ordinal = 2)
    private String dnsStart;

    @JSONField(ordinal = 3)
    private String dnsEnd;

    @JSONField(ordinal = 4)
    private String connectStart;

    @JSONField(ordinal = 5)
    private String secureConnectStart;

    @JSONField(ordinal = 6)
    private String secureConnectEnd;

    @JSONField(ordinal = 7)
    private String connectEnd;

    @JSONField(ordinal = 8)
    private String connectFailed;

    @JSONField(ordinal = 9)
    private String connectionAcquired;

    @JSONField(ordinal = 18)
    private String connectionReleased;

    @JSONField(ordinal = 10)
    private String requestHeadersStart;

    @JSONField(ordinal = 11)
    private String requestHeadersEnd;

    @JSONField(ordinal = 12)
    private String requestBodyStart;

    @JSONField(ordinal = 13)
    private String requestBodyEnd;

    @JSONField(ordinal = 14)
    private String responseHeadersStart;

    @JSONField(ordinal = 15)
    private String responseHeadersEnd;

    @JSONField(ordinal = 16)
    private String responseBodyStart;

    @JSONField(ordinal = 17)
    private String responseBodyEnd;

    @JSONField(ordinal = 19)
    private String callEnd;

    @JSONField(ordinal = 20)
    private String callFailed;
}
