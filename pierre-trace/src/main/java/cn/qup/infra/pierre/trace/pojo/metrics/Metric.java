package cn.qup.infra.pierre.trace.pojo.metrics;

import cn.qup.infra.pierre.trace.enums.AggregateUnit;
import cn.qup.infra.pierre.trace.enums.MetricType;
import cn.qup.infra.pierre.trace.enums.ResourceType;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;
import java.util.TreeMap;

@Data
@Accessors(chain = true)
public abstract class Metric {

    @JSONField(name = "metric")
    private String metricName;

    private ResourceType resourceType;

    private String appName;

    private String appId;

    private String instanceId;

    private long timestamp;

}
