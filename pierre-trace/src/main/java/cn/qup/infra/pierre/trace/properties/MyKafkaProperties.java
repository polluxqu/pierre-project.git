package cn.qup.infra.pierre.trace.properties;

import lombok.Data;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
//@ConfigurationProperties(prefix = "spring.kafka")
@ConfigurationProperties(prefix = "pierre.trace.kafka")
@ConditionalOnProperty("pierre.trace.kafka")
@EnableConfigurationProperties({MyKafkaProperties.class})
public class MyKafkaProperties {

    private String bootStrapServers;
    private String topics;
    private String acks = "all";
    private Integer retries =0;
    private Long batchSize = 16384L;
    private Long linger = 1L;
    private Long buffer=33554432L;

}
