package cn.qup.infra.pierre.trace.util;

import cn.qup.infra.pierre.trace.pojo.ExecuteTime;
import cn.qup.infra.pierre.trace.pojo.metrics.CounterMetric;
import cn.qup.infra.pierre.trace.pojo.metrics.Metric;
import cn.qup.infra.pierre.trace.properties.MyKafkaProperties;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.aopalliance.aop.Advice;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.jetbrains.annotations.NotNull;
import org.springframework.aop.Advisor;
import org.springframework.aop.AfterAdvice;
import org.springframework.aop.AfterReturningAdvice;
import org.springframework.aop.MethodBeforeAdvice;
import org.springframework.aop.framework.adapter.AdvisorAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.sql.Timestamp;

@Slf4j
@Component
public class PierreAOPAdvice extends PierreAOPInterceptor implements MethodBeforeAdvice,AfterAdvice, AfterReturningAdvice, AdvisorAdapter {

    @Lazy
    @Autowired
    MyKafkaProperties myKafkaProperties;

    @Lazy
    @Autowired
    KafkaProducer<String, String> kafkaProducer;

    private final ExecuteTime executeTime = new ExecuteTime();

    @Override
    public void before(@NotNull Method method, @NotNull Object[] args, Object target) throws Throwable {

    }

    @Override
    public void afterReturning(Object returnValue, @NotNull Method method, @NotNull Object[] args, Object target) {
        executeTime.setResult(returnValue);
        String result = JSON.toJSONString(executeTime, SerializerFeature.IgnoreNonFieldGetter, SerializerFeature.IgnoreErrorGetter);
        try{
            if(kafkaProducer != null){
                // 如果 pierre project 配置了 trace 消息队列，则增加往队列中吐数据的动作
                ProducerRecord<String, String> record =new ProducerRecord<>(myKafkaProperties.getTopics(),result);
                kafkaProducer.send(record);
            }
        }catch (Exception e){
            log.warn("",e);
        }

        log.info("【Pierre-Trace】: " + result);
    }

    @Override
    public Object invoke(MethodInvocation invocation) {
        Object object =null;
        try{
            long beginTime = System.currentTimeMillis();
            executeTime.setClassName(invocation.getMethod().getDeclaringClass().getName());
            executeTime.setMethod(invocation.getMethod().getName());
            executeTime.setBeginTime(new Timestamp(beginTime));
            executeTime.setArgs(invocation.getArguments());
            object = super.invoke(invocation);
            executeTime.setExecuteTime(System.currentTimeMillis() -beginTime);
        }catch (Throwable throwable){
            log.warn("",throwable);
//            throwable.printStackTrace();
        }
        return object;
    }

    @Override
    public boolean supportsAdvice(@NonNull Advice advice) {
        return true;
    }

    @NotNull
    @Override
    public MethodInterceptor getInterceptor(@NonNull Advisor advisor) {
        return null;
    }
}
