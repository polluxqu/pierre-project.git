package cn.qup.infra.pierre.trace.pojo;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExecuteTime {

    @JSONField(name = "类",ordinal = 0)
    private String className;

    @JSONField(name = "方法",ordinal = 1)
    private String method;

    @JSONField(name = "入参",ordinal = 2)
    private Object[] args;

    @JSONField(name = "开始时间",ordinal = 3)
    private Timestamp beginTime;

    @JSONField(name = "执行耗时",ordinal = 4)
    private long ExecuteTime;

    @JSONField(name = "执行结果",ordinal = 5)
    private Object result;
}
