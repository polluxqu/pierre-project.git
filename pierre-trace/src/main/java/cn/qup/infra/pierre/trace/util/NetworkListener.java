package cn.qup.infra.pierre.trace.util;

import cn.qup.infra.pierre.trace.pojo.HttpRequestTrace;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.jetbrains.annotations.NotNull;
import org.springframework.lang.Nullable;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicLong;

@Slf4j
public class NetworkListener extends EventListener {
    /**
     * 自定义EventListener工厂
     */
    public static final Factory FACTORY = new Factory() {
        final AtomicLong nextCallId = new AtomicLong(1L);

        @NotNull
        @Override
        public EventListener create(@NotNull Call call) {
            log.info("Http Network Tracing Start.");
            long callId = nextCallId.getAndIncrement();
            return new NetworkListener(callId, call.request().url(), System.nanoTime());
        }

    };

    /**
     * 每次请求的标识
     */
    private final long callId;

    /**
     * 每次请求的开始时间，单位纳秒
     */
    private final long callStartNanos;

    private final HttpRequestTrace trace;

    public NetworkListener(long callId, HttpUrl url, long callStartNanos) {
        this.callId = callId;
        this.callStartNanos = callStartNanos;
        trace = new HttpRequestTrace();
        this.trace.setUrl(url.toString());
    }

    private String calcTime(){
        long elapseNanos = System.nanoTime() - callStartNanos;
        return String.format(Locale.CHINA, "%.3f", elapseNanos / 1000000d);
    }

    @Override
    public void callStart(@NotNull Call call) {
        super.callStart(call);
        this.trace.setCallStart(calcTime());
    }

    @Override
    public void dnsStart(@NotNull Call call, @NotNull String domainName) {
        super.dnsStart(call, domainName);
        this.trace.setDnsStart(calcTime());
    }

    @Override
    public void dnsEnd(@NotNull Call call, @NotNull String domainName, @NotNull List<InetAddress> inetAddressList) {
        super.dnsEnd(call, domainName, inetAddressList);
        this.trace.setDnsEnd(calcTime());
    }

    @Override
    public void connectStart(@NotNull Call call, @NotNull InetSocketAddress inetSocketAddress, @NotNull Proxy proxy) {
        super.connectStart(call, inetSocketAddress, proxy);
        this.trace.setConnectStart(calcTime());;
    }

    @Override
    public void secureConnectStart(@NotNull Call call) {
        super.secureConnectStart(call);
        this.trace.setSecureConnectStart(calcTime());
    }

    @Override
    public void secureConnectEnd(@NotNull Call call, @Nullable Handshake handshake) {
        super.secureConnectEnd(call, handshake);
        this.trace.setSecureConnectEnd(calcTime());
    }

    @Override
    public void connectEnd(@NotNull Call call, @NotNull InetSocketAddress inetSocketAddress, @NotNull Proxy proxy, @Nullable Protocol protocol) {
        super.connectEnd(call, inetSocketAddress, proxy, protocol);
        this.trace.setConnectEnd(calcTime());
    }

    @Override
    public void connectFailed(@NotNull Call call, @NotNull InetSocketAddress inetSocketAddress, @NotNull Proxy proxy, @Nullable Protocol protocol, @NotNull IOException ioe) {
        super.connectFailed(call, inetSocketAddress, proxy, protocol, ioe);
        this.trace.setConnectFailed(calcTime());
    }

    @Override
    public void connectionAcquired(@NotNull Call call, @NotNull Connection connection) {
        super.connectionAcquired(call, connection);
        this.trace.setConnectionAcquired(calcTime());
    }

    @Override
    public void connectionReleased(@NotNull Call call, @NotNull Connection connection) {
        super.connectionReleased(call, connection);
        this.trace.setConnectionReleased(calcTime());
    }

    @Override
    public void requestHeadersStart(@NotNull Call call) {
        super.requestHeadersStart(call);
        this.trace.setRequestHeadersStart(calcTime());
    }

    @Override
    public void requestHeadersEnd(@NotNull Call call, @NotNull Request request) {
        super.requestHeadersEnd(call, request);
        this.trace.setRequestHeadersEnd(calcTime());
    }

    @Override
    public void requestBodyStart(@NotNull Call call) {
        super.requestBodyStart(call);
        this.trace.setRequestBodyStart(calcTime());
    }

    @Override
    public void requestBodyEnd(@NotNull Call call, long byteCount) {
        super.requestBodyEnd(call, byteCount);
        this.trace.setRequestBodyEnd(calcTime());
    }

    @Override
    public void responseHeadersStart(@NotNull Call call) {
        super.responseHeadersStart(call);
        this.trace.setResponseHeadersStart(calcTime());
    }

    @Override
    public void responseHeadersEnd(@NotNull Call call, @NotNull Response response) {
        super.responseHeadersEnd(call, response);
        this.trace.setResponseHeadersEnd(calcTime());
    }

    @Override
    public void responseBodyStart(@NotNull Call call) {
        super.responseBodyStart(call);
        this.trace.setResponseBodyStart(calcTime());
    }

    @Override
    public void responseBodyEnd(@NotNull Call call, long byteCount) {
        super.responseBodyEnd(call, byteCount);
        this.trace.setResponseBodyEnd(calcTime());
    }

    @Override
    public void callEnd(@NotNull Call call) {
        super.callEnd(call);
        this.trace.setCallEnd(calcTime());
        log.info("【Pierre-Trace】: "+ JSON.toJSONString(this.trace));
    }

    @Override
    public void callFailed(@NotNull Call call, @NotNull IOException ioe) {
        super.callFailed(call, ioe);
        this.trace.setCallFailed(calcTime());
        log.info("【Pierre-Trace】: "+ JSON.toJSONString(this.trace));
    }
}
