package cn.qup.infra.pierre.trace.pojo.metrics;

import cn.qup.infra.pierre.trace.enums.MetricType;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class GaugeMetric extends Metric {

    private static final MetricType metricType = MetricType.Gauge;
    public MetricType getMetricType() {
        return this.metricType;
    }

    private Number value;
}
