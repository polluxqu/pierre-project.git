package cn.qup.infra.pierre.trace.properties;

import lombok.Data;
import lombok.Getter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.List;

@Getter
@Configuration
@ConfigurationProperties(prefix = "pierre.trace")
@ConditionalOnProperty("pierre.trace")
@EnableConfigurationProperties(TraceProperties.class)
public class TraceProperties {

    private String[] expression ={"@annotation(cn.qup.infra.pierre.trace.annotation.CalculateExecuteTime)"};

    public void setExpression(String[] expression){
        this.expression = expression;
    }

}
