package cn.qup.infra.pierre.trace.config;

import cn.qup.infra.pierre.trace.properties.MyKafkaProperties;
import cn.qup.infra.pierre.trace.properties.TraceProperties;
import cn.qup.infra.pierre.trace.service.PierreDynamicPointcut;
import cn.qup.infra.pierre.trace.util.PierreAOPAdvice;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.aop.Pointcut;
import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.context.annotation.*;

import java.util.List;

@Slf4j
@Configuration
@Role(RootBeanDefinition.ROLE_INFRASTRUCTURE)
public class PierreDynamicAOPConfig {

    @Lazy
    @Autowired(required = false)
    TraceProperties traceProperties;

    @Bean
    @Role(RootBeanDefinition.ROLE_INFRASTRUCTURE)
    @DependsOn("traceProperties")
    public Pointcut customPointCut() {
        PierreDynamicPointcut dynamicPointcut = new PierreDynamicPointcut();
        String expression = String.join(" || ",traceProperties.getExpression());
        try{
            dynamicPointcut.setExpression(expression);

            log.info("【Pierre-Trace】: Set trace expression {} finish.",expression);
        }catch (Exception e){
            log.warn("【Pierre-Trace】: Set trace expression {} fail by {}",expression,e.getMessage());
        }
        return dynamicPointcut;
    }

    @Bean
    @Role(RootBeanDefinition.ROLE_INFRASTRUCTURE)
    PierreAOPAdvice getAdvice(){
        return new PierreAOPAdvice();
    }

    @Bean
    @Role(RootBeanDefinition.ROLE_INFRASTRUCTURE)
    DefaultPointcutAdvisor defaultPointcutAdvisor(){
        DefaultPointcutAdvisor defaultPointcutAdvisor = new DefaultPointcutAdvisor();
        defaultPointcutAdvisor.setPointcut(customPointCut());
        defaultPointcutAdvisor.setAdvice(getAdvice());
        return defaultPointcutAdvisor;
    }
}
