package cn.qup.infra.pierre.trace.pojo.metrics;

import cn.qup.infra.pierre.trace.enums.MetricType;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class AmountMetric extends Metric {

    private static final MetricType metricType = MetricType.Amount;
    public MetricType getMetricType() {
        return this.metricType;
    }

    private Number value;
}
