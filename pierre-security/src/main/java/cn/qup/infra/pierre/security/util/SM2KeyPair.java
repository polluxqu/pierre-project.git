package cn.qup.infra.pierre.security.util;

import cn.qup.infra.pierre.core.pojo.ApiResult;
import cn.qup.infra.pierre.core.util.HexUtil;
import cn.qup.infra.pierre.security.pojo.SM2Key;
import lombok.extern.slf4j.Slf4j;
import org.bouncycastle.asn1.gm.GMNamedCurves;
import org.bouncycastle.asn1.x9.X9ECParameters;
import org.bouncycastle.crypto.engines.SM2Engine;
import org.bouncycastle.crypto.params.ECDomainParameters;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.crypto.params.ParametersWithRandom;
import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPrivateKey;
import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPublicKey;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.jce.spec.ECParameterSpec;
import org.bouncycastle.jce.spec.ECPrivateKeySpec;
import org.bouncycastle.jce.spec.ECPublicKeySpec;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.ECGenParameterSpec;
import java.util.Base64;

@Slf4j
public class SM2KeyPair {

    //椭圆曲线ECParameters ASN.1 结构
    private static final X9ECParameters x9ECParameters = GMNamedCurves.getByName("sm2p256v1");
    //椭圆曲线公钥或私钥的基本域参数。
    private static final ECParameterSpec ecDomainParameters = new ECParameterSpec(x9ECParameters.getCurve(), x9ECParameters.getG(), x9ECParameters.getN());


    /**
     * 生成SM2密钥对
     *
     * @return 包含SM2密钥对的ApiResult对象
     *         - 数据字段包含SM2公钥和私钥
     *         - 错误情况下，包含错误代码和消息
     */
    public static ApiResult<SM2Key> genSM2KeyPair(){
        // 初始化结果对象
        ApiResult<SM2Key> result = new ApiResult<>();
        try{
            // 设置SM2曲线参数规范
            ECGenParameterSpec sm2Spec = new ECGenParameterSpec("sm2p256v1");
            // 获取密钥对生成器实例
            final KeyPairGenerator kpg = KeyPairGenerator.getInstance("EC", new BouncyCastleProvider());
            // 初始化密钥对生成器
            kpg.initialize(sm2Spec,new SecureRandom());
            // 创建SM2密钥对象
            SM2Key sm2Key = new SM2Key();
            // 生成密钥对
            KeyPair keyPair = kpg.generateKeyPair();
            // 获取并处理公钥
            PublicKey publicKey = keyPair.getPublic();
            if(publicKey instanceof BCECPublicKey){
                // 将公钥编码为Base64格式字符串
                sm2Key.setPublicKey(Base64.getEncoder().encodeToString(((BCECPublicKey) publicKey).getQ().getEncoded(false)));
            }
            // 获取并处理私钥
            PrivateKey privateKey = keyPair.getPrivate();
            if(privateKey instanceof BCECPrivateKey){
                // 将私钥转换为16进制字符串
                sm2Key.setPrivateKey(((BCECPrivateKey) privateKey).getD().toString(16));
            }
            // 设置结果数据
            result.setData(sm2Key);
        }catch (Exception e){
            // 处理异常情况
            result.setCode(500);
            result.setMessage(e.getMessage());
            // 记录警告日志
            log.warn("",e);
        }
        // 返回结果
        return result;
    }

    /**
     * @Description 私钥字符串转换为 BCECPrivateKey 私钥对象
     * @Author Pollux.Qu
     * @param privateKeyHex 32字节十六进制私钥字符串
     * @return BCECPrivateKey SM2私钥对象
     */

    private static BCECPrivateKey getBCECPrivateKeyByPrivateKeyHex(String privateKeyHex) {
        //将十六进制私钥字符串转换为BigInteger对象
        BigInteger d = new BigInteger(privateKeyHex, 16);
        //通过私钥和私钥域参数集创建椭圆曲线私钥规范
        ECPrivateKeySpec ecPrivateKeySpec = new ECPrivateKeySpec(d, ecDomainParameters);
        //通过椭圆曲线私钥规范，创建出椭圆曲线私钥对象（可用于SM2解密和签名）
        return new BCECPrivateKey("EC", ecPrivateKeySpec, BouncyCastleProvider.CONFIGURATION);

    }

    /**
     * @Description 公钥字符串转换为 BCECPublicKey 公钥对象
     * @Author Pollux.Qu
     * @param pubKeyHex 64字节十六进制公钥字符串(如果公钥字符串为65个字节首个字节为0x04：表示该公钥为非压缩格式，操作时需要删除)
     * @return BCECPublicKey SM2公钥对象
     */
    private static BCECPublicKey getECPublicKeyByPublicKeyHex(String pubKeyHex) {
        String hex = HexUtil.byteArrayToHexStr(Base64.getDecoder().decode(pubKeyHex));
        //截取64个字节有效的SM2公钥（如果公钥首个字节为0x04）
        if (hex.length() > 128) {
            hex = hex.substring(hex.length() - 128);
        }

        //将公钥拆分为x,y分量（各32个字节）
        String stringX = hex.substring(0, 64);
        String stringY = hex.substring(stringX.length());
        //将公钥x、y分量转换为BigInteger类型
        BigInteger x = new BigInteger(stringX, 16);
        BigInteger y = new BigInteger(stringY, 16);
        //通过公钥x、y分量创建椭圆曲线公钥规范
        ECPublicKeySpec ecPublicKeySpec = new ECPublicKeySpec(x9ECParameters.getCurve().createPoint(x, y), ecDomainParameters);
        //通过椭圆曲线公钥规范，创建出椭圆曲线公钥对象（可用于SM2加密及验签）
        return new BCECPublicKey("EC", ecPublicKeySpec, BouncyCastleProvider.CONFIGURATION);
    }

    /**
     * @Description 公钥加密
     * @Author Pollux.Qu
     * @param publicKey SM2公钥
     * @param data      明文数据
     * @param modeType  加密模式
     * @return String
     */
    public static String encrypt(BCECPublicKey publicKey, String data, int modeType) {

        //加密模式
        SM2Engine.Mode mode = SM2Engine.Mode.C1C3C2;
        if (modeType != 1) {
            mode = SM2Engine.Mode.C1C2C3;
        }

        //通过公钥对象获取公钥的基本域参数。
        ECParameterSpec ecParameterSpec = publicKey.getParameters();
        ECDomainParameters ecDomainParameters = new ECDomainParameters(ecParameterSpec.getCurve(),
                ecParameterSpec.getG(), ecParameterSpec.getN());

        //通过公钥值和公钥基本参数创建公钥参数对象
        ECPublicKeyParameters ecPublicKeyParameters = new ECPublicKeyParameters(publicKey.getQ(), ecDomainParameters);

        //根据加密模式实例化SM2公钥加密引擎
        SM2Engine sm2Engine = new SM2Engine(mode);

        //初始化加密引擎
        sm2Engine.init(true, new ParametersWithRandom(ecPublicKeyParameters, new SecureRandom()));
        byte[] arrayOfBytes = null;
        try {
            //将明文字符串转换为指定编码的字节串
            byte[] in = data.getBytes(StandardCharsets.UTF_8);

            //通过加密引擎对字节数串行加密
            arrayOfBytes = sm2Engine.processBlock(in, 0, in.length);
        } catch (Exception e) {
            log.warn("SM2加密时出现异常:" + e.getMessage());
//            e.printStackTrace();
        }

        //将加密后的字节串转换为十六进制字符串
        return HexUtil.byteArrayToHexStr(arrayOfBytes);

    }

    public static String encrypt(String publicKey, String data){
        BCECPublicKey pubKey = getECPublicKeyByPublicKeyHex(publicKey);
        return encrypt(pubKey, data, 1);
    }

    /**
     * @Description 私钥解密
     * @Author Pollux.Qu
     * @param privateKey SM私钥
     * @param cipherData 密文数据
     * @param modeType   解密模式
     * @return String
     */

    public static String decrypt(BCECPrivateKey privateKey, String cipherData, int modeType) {

        //解密模式
        SM2Engine.Mode mode = SM2Engine.Mode.C1C3C2;
        if (modeType != 1) {
            mode = SM2Engine.Mode.C1C2C3;
        }

        //将十六进制字符串密文转换为字节数组（需要与加密一致，加密是：加密后的字节数组转换为了十六进制字符串）
        byte[] cipherDataByte = HexUtil.hexStrToByteArray(cipherData);

        // 通过私钥对象获取私钥的基本域参数
        ECParameterSpec ecParameterSpec = privateKey.getParameters();
        ECDomainParameters ecDomainParameters = new ECDomainParameters(ecParameterSpec.getCurve(),
                ecParameterSpec.getG(), ecParameterSpec.getN());

        //通过私钥值和私钥钥基本参数创建私钥参数对象
        ECPrivateKeyParameters ecPrivateKeyParameters = new ECPrivateKeyParameters(privateKey.getD(),
                ecDomainParameters);

        //通过解密模式创建解密引擎并初始化
        SM2Engine sm2Engine = new SM2Engine(mode);

        sm2Engine.init(false, ecPrivateKeyParameters);
        String result = null;

        try {
            //通过解密引擎对密文字节串进行解密
            byte[] arrayOfBytes = sm2Engine.processBlock(cipherDataByte, 0, cipherDataByte.length);

            //将解密后的字节串转换为utf8字符编码的字符串（需要与明文加密时字符串转换成字节串所指定的字符编码保持一致）
            result = new String(arrayOfBytes, StandardCharsets.UTF_8);
        } catch (Exception e) {
            log.warn("SM2解密时出现异常" + e.getMessage());
        }
        return result;
    }

    public static String decrypt(String privateKey, String cipherDate){
        BCECPrivateKey bcecPrivateKey = getBCECPrivateKeyByPrivateKeyHex(privateKey);
        return decrypt(bcecPrivateKey,cipherDate,1);
    }

}
