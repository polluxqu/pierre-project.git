package cn.qup.infra.pierre.security.util;

import java.security.Security;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import cn.qup.infra.pierre.core.util.HexUtil;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.apache.commons.codec.binary.Base64;

public class SM4Util {

    private static final String ALGORITHM_NAME = "SM4";
    private static final String ALGORITHM_NAME_ECB_PADDING = "SM4/ECB/PKCS5Padding";

    /**
     * SM4算法目前只支持128位（即密钥 16 字节）
     */

    private static final int DEFAULT_KEY_SIZE = 128;
    private static final Object lock = new Object();

    static {
        Security.addProvider(new BouncyCastleProvider());
    }

    /**
     * 加密
     *
     * @param data 数据
     * @param key  秘钥
     * @return 密文
     */
    public static String encrypt(String data, String key) throws Exception {

        SecretKey secretKey = new SecretKeySpec(key.getBytes(), ALGORITHM_NAME);
        Cipher cipher = Cipher.getInstance(ALGORITHM_NAME_ECB_PADDING,
                BouncyCastleProvider.PROVIDER_NAME);
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);
        byte[] encryptedBytes = cipher.doFinal(data.getBytes());
        return HexUtil.byteArrayToHexStr(encryptedBytes);
    }

    /**
     * 解密
     *
     * @param data 数据
     * @param key  秘钥
     * @return 明文
     */
    public static String decrypt(String data, String key) throws Exception {
        SecretKey secretKey = new SecretKeySpec(key.getBytes(), ALGORITHM_NAME);
        Cipher cipher = Cipher.getInstance(ALGORITHM_NAME_ECB_PADDING,
                BouncyCastleProvider.PROVIDER_NAME);
        cipher.init(Cipher.DECRYPT_MODE, secretKey);
        byte[] decryptedBytes = cipher.doFinal(HexUtil.hexStrToByteArray(data));
        return new String(decryptedBytes);
    }
}
