package cn.qup.infra.pierre.security.encrypt;

import cn.qup.infra.pierre.core.util.HexUtil;
import cn.qup.infra.pierre.security.enums.encryptEnum.DecEnum;
import org.apache.commons.lang3.RandomStringUtils;

public class DESSlatUtil {

    public static String Encrypt(String Text, String skey, boolean isSalt){
        try{
            if(isSalt){
                String slatText = HexUtil.byteArrayToHexStr(RandomStringUtils.randomAlphanumeric(4).getBytes());
                String key = EncryptUtil.MD_ENCRYPT.encryptHex(skey).substring(0,8);
                String plaintext = EncryptUtil.DES_ENCRYPT.encryptHex(Text,key,slatText, DecEnum.CBC_PKCS5PADDING);
                return plaintext + slatText;
            }else {
                String key = EncryptUtil.MD_ENCRYPT.encryptHex(skey).substring(0,8);
                return EncryptUtil.DES_ENCRYPT.encryptHex(Text,key,key, DecEnum.CBC_PKCS5PADDING);
            }
        }catch (Exception e){
            return "";
        }

    }

    public static String Decrypt(String Text, String skey, boolean isSalt){
        try{
            if(isSalt){
                String slatText = Text.substring(Text.length() -8, Text.length());
                String plaintext = Text.substring(0,Text.length()-8);
                String key = EncryptUtil.MD_ENCRYPT.encryptHex(skey).substring(0,8);
                return  EncryptUtil.DES_ENCRYPT.decryptHex(plaintext,key,slatText, DecEnum.CBC_PKCS5PADDING);
            }else {
                String key = EncryptUtil.MD_ENCRYPT.encryptHex(skey).substring(0,8);
                return EncryptUtil.DES_ENCRYPT.decryptHex(Text,key,key, DecEnum.CBC_PKCS5PADDING);
            }
        }catch (Exception e){
            return "";
        }
    }
}
