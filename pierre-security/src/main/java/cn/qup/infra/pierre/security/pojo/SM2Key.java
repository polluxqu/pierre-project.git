package cn.qup.infra.pierre.security.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SM2Key {
    private String publicKey;
    private String privateKey;
}
