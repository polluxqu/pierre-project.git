package cn.qup.infra.pierre.security.util;

import cn.qup.infra.pierre.core.pojo.ApiResult;
import cn.qup.infra.pierre.core.util.HexUtil;
import cn.qup.infra.pierre.security.pojo.RSAKey;
import lombok.extern.slf4j.Slf4j;
import lombok.extern.slf4j.XSlf4j;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Date;
import java.util.Map;

@Slf4j
public class RSAKeyPair {

    private static final int MAX_ENCRYPT_BLOCK = 128;
    private static final int MAX_ENCRYPT_SIZE = 500;
    /**
     * 生成一对RSA 公私钥
     * @return ApiResult<RSAKey>
     */
    public static ApiResult<RSAKey> Generator(){
        ApiResult<RSAKey> result = new ApiResult<>();
        KeyPairGenerator keyPairGenerator;
        try{

            keyPairGenerator = KeyPairGenerator.getInstance("RSA");
            SecureRandom secureRandom = new SecureRandom(new Date().toString().getBytes());
            keyPairGenerator.initialize(512, secureRandom);
            KeyPair keyPair = keyPairGenerator.genKeyPair();

            String publicKey = Base64.getEncoder().encodeToString(keyPair.getPublic().getEncoded());
            String privateKey = Base64.getEncoder().encodeToString(keyPair.getPrivate().getEncoded());

            RSAKey key = new RSAKey();
            key.setPublicKey(publicKey);
            key.setPrivateKey(privateKey);
            result.setData(key);

        }catch (Exception e){
            result.setCode(500);
            result.setMessage("GENERATOR_KEY_PAIR_FAILED");
            log.warn("",e);
        }
        return result;
    }

    /**
     * 通过一个字串返回公钥对象
     * @param publicKey 公钥
     * @return 公钥对象
     */
    private static PublicKey getPublicKey(String publicKey){
        try{
            X509EncodedKeySpec spec = new X509EncodedKeySpec(Base64.getDecoder().decode(publicKey));
            KeyFactory kf = KeyFactory.getInstance("RSA");
            return kf.generatePublic(spec);
        }catch (Exception e){
            return null;
        }
    }

    /**
     * 通过一个字串返回私钥对象
     * @param privateKey
     * @return
     */
    private static PrivateKey getPrivateKey(String privateKey){
        try{
            PKCS8EncodedKeySpec spec =new PKCS8EncodedKeySpec(Base64.getDecoder().decode(privateKey));
            KeyFactory kf = KeyFactory.getInstance("RSA");
            return kf.generatePrivate(spec);
        }catch (Exception e){
            return null;
        }
    }

    /**
     * 对指定字串使用指定公钥加密
     * @param text
     * @param publicKey
     * @return
     */
    public static ApiResult<String> Encrypt(String text, String publicKey){
        ApiResult<String> result = new ApiResult<>();
        Cipher cipher;
        RSAPublicKey pubKey;
        byte[] cipherText;
        int  offSet  = 0;
        int i = 0;
        StringBuilder sb = new StringBuilder();
        try{
            cipher = Cipher.getInstance("RSA");
            pubKey = (RSAPublicKey) getPublicKey(publicKey);
            cipher.init(Cipher.ENCRYPT_MODE, pubKey);
            while (text.length() - offSet>0){
                if(text.length() - offSet > MAX_ENCRYPT_SIZE){
                    cipherText = cipher.doFinal(text.getBytes(),offSet,MAX_ENCRYPT_SIZE);
                }else {
                    cipherText = cipher.doFinal(text.getBytes(),offSet,text.length() - offSet);
                }
                sb.append(HexUtil.byteArrayToHexStr(cipherText));
                i++;
                offSet = i * MAX_ENCRYPT_SIZE;
            }
            String var= Base64.getEncoder().encodeToString(HexUtil.hexStrToByteArray(sb.toString()));
            result.setData(var);

        }catch (NoSuchAlgorithmException e){
            result.setCode(500);
            result.setMessage("RSA_NOT_SUPPORT: " + e.getMessage());
        }catch (NoSuchPaddingException | BadPaddingException e){
            result.setCode(500);
            result.setMessage("PADDING_NOT_SUPPORT: " + e.getMessage());
        }catch (InvalidKeyException e){
            result.setCode(500);
            result.setMessage("INVALID_KEY: " + e.getMessage());
        }catch (IllegalBlockSizeException e){
            result.setCode(500);
            result.setMessage("Illegal_BLOCK_SIZE: " + e.getMessage());
        }catch (Exception e){
            result.setCode(500);
            result.setMessage("UNKNOWN_REASON_FAILED: " + e.getMessage());
        }
        return result;
    }

    /**
     * 对指定密文使用私钥解密
     * @param text
     * @param privateKey
     * @return
     */
    public static ApiResult<String> Decrypt(String text, String privateKey){
        ApiResult<String> result = new ApiResult<>();
        Cipher cipher;
        RSAPrivateKey pk;
        byte[] cipherText;
        int offSet = 0;
        int i=0;
        StringBuilder sb = new StringBuilder();
        text = HexUtil.byteArrayToHexStr(Base64.getDecoder().decode(text));
        try{

            cipher = Cipher.getInstance("RSA");
            pk = (RSAPrivateKey) getPrivateKey(privateKey);
            cipher.init(Cipher.DECRYPT_MODE, pk);
            while (text.length() - offSet >0){
                cipherText = cipher.doFinal(HexUtil.hexStrToByteArray(text.substring(offSet,offSet + MAX_ENCRYPT_BLOCK)));
                sb.append(new String(cipherText));
                i++;
                offSet = i * MAX_ENCRYPT_BLOCK;
            }
            result.setData(sb.toString());
        }catch (NoSuchAlgorithmException e){
            result.setCode(500);
            result.setMessage("RSA_NOT_SUPPORT: " + e.getMessage());
        }catch (NoSuchPaddingException | BadPaddingException e){
            result.setCode(500);
            result.setMessage("PADDING_NOT_SUPPORT: " + e.getMessage());
        }catch (InvalidKeyException e){
            result.setCode(500);
            result.setMessage("INVALID_KEY: " + e.getMessage());
        }catch (IllegalBlockSizeException e){
            result.setCode(500);
            result.setMessage("Illegal_BLOCK_SIZE: " + e.getMessage());
        }
        return result;

    }
}
