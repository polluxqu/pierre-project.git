package cn.qup.infra.pierre.security.util;

import org.apache.commons.lang3.RandomStringUtils;

import java.util.*;

public class PasswordGenerator {
    private static final String LETTERS_UPPER = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final String LETTERS_LOWER = "abcdefghijklmnopqrstuvwxyz";
    private static final String NUMBER = "0123456789";
    private static final String SYMBOL = "!@#$%^&*()_+-=";

    public static String Generator(int length, boolean includeNumber, boolean includeSymbol, boolean includeLower, String symbol){
        Random r = new Random(new Date().getTime());

        int upperLen = 0;
        int numberLen =0;
        int symbolLen =0;
        int lowerLen =0;
        // 计算各类字符数量，之所以前面的需要 -3、-2 是避免前面的在随机时随机到最大，导致后面在随机是 length - 前面长度之后为0，一直随机不出来的问题
        if(includeNumber){
            while (numberLen ==0){
                numberLen = r.nextInt(length -3) ;
            }
        }
        if(includeSymbol){
            while (symbolLen ==0){
                symbolLen = r.nextInt(length-2 - numberLen);
            }
        }
        if(includeLower){
            while (lowerLen ==0){
                lowerLen =r.nextInt(length - numberLen -1 - symbolLen ) ;
            }
        }
        upperLen = length - numberLen - symbolLen - lowerLen ;

        // System.out.println(String.format("Upper:%s,Number:%s,Symbol:%s,Lower:%s",upperLen,numberLen,symbolLen,lowerLen));
        String result = RandomStringUtils.random(upperLen,LETTERS_UPPER);
        result = result + RandomStringUtils.random(numberLen,NUMBER);
        result = result + RandomStringUtils.random(symbolLen,symbol);
        result = result + RandomStringUtils.random(lowerLen,LETTERS_LOWER);

        //打乱位置
        List<String> stringList = Arrays.asList(result.split(""));
        Collections.shuffle(stringList);
        return String.join("",stringList);

    }

    public static String Generator(int length, boolean includeNumber, boolean includeSymbol, boolean includeLower){
        return Generator(length,includeNumber,includeSymbol,includeLower,SYMBOL);
    }

    public static double calculatePasswordEntropy(String password) {
        int length = password.length();
        Map<Character, Integer> charCountMap = new HashMap<>();

        for (int i = 0; i < length; i++) {
            char c = password.charAt(i);

            if (!charCountMap.containsKey(c)) {
                charCountMap.put(c, 1);
            } else {
                charCountMap.replace(c, charCountMap.get(c) + 1);
            }
        }

        List<Integer> countsList = new ArrayList<>(charCountMap.values());
        Collections.sort(countsList);

        double log2 = Math.log(2);
        double sumProbabilities = 0;

        for (int count : countsList) {
            double probability = ((double) count / length);
            sumProbabilities += -probability * Math.log(probability) / log2;
        }

        return sumProbabilities;
    }

}
