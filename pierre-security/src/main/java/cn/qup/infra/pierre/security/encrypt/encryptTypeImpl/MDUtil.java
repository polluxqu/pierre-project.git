package cn.qup.infra.pierre.security.encrypt.encryptTypeImpl;

import cn.qup.infra.pierre.security.encrypt.EncryptUtil;
import cn.qup.infra.pierre.security.encrypt.impl.MessageDigestImp;
import cn.qup.infra.pierre.security.enums.encryptEnum.MDEnum;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * MD加密工具类
 */
public class MDUtil extends MessageDigestImp<MDEnum> {

//	private final static Logger logger = LoggerFactory.getLogger(ShaUtil.class);

    public MDUtil(MDEnum defaultEncrypt) {
        this.defaultAlgorithm = defaultEncrypt == null ? MDEnum.MD5 : defaultEncrypt;
        this.configSlat = EncryptUtil.MD_SLAT;
    }

    @Override
    protected byte[] encrypt(String content, String slat, MDEnum encryptType) {
        try {
            String encryptContent = null;
            if (slat != null)
            {
                encryptContent = content + slat;
            }
            else
            {
                encryptContent = content;
            }
            MessageDigest messageDigest = MessageDigest.getInstance(encryptType.getEncryptType());
            return messageDigest.digest(encryptContent.getBytes());
        } catch (NoSuchAlgorithmException e) {
//			logger.error("MD MessageDigest init error, encrypt type no support.");
        }
        return null;
    }
}
