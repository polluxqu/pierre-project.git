package cn.qup.infra.pierre.redis.config;

import cn.qup.infra.pierre.redis.util.FastJson2JsonRedisSerializer;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;

import javax.annotation.Resource;
import java.io.Serializable;


@Configuration
@AutoConfigureAfter(RedisAutoConfiguration.class)
public class RedisConfig {

    @Resource
    LettuceConnectionFactory redisConnectionFactory;

    @Resource
    RedisConnectionFactory factory;

    @Bean
    public RedisTemplate<String, Serializable> redisCacheTemplate() {
        RedisTemplate<String, Serializable> template = new RedisTemplate<>();

        RedisSerializer stringSerializer = new FastJson2JsonRedisSerializer<>(Object.class);
        template.setKeySerializer(stringSerializer);
        template.setValueSerializer(stringSerializer);
        template.setHashKeySerializer(stringSerializer);
        template.setHashValueSerializer(stringSerializer);
        template.setConnectionFactory(redisConnectionFactory);
        template.afterPropertiesSet();
        return template;
    }

    /**
     * spring-data-redis提供了两种redis template：RedisTemplate和StringRedisTemplate
     * 区别就在于key，value的serializer上，为了保证key，value的序列化/反序列化一致，统一都使用StringRedisTemplate
     * 这里实际使用StringRedisTemplate覆盖了原有的RedisTemplate
     *
     * @param
     * @return
     */
    @Bean
    public RedisTemplate<String, String> redisTemplate() {
        return new StringRedisTemplate(factory);
    }
}


