package cn.qup.infra.pierre.springbootstarter.util.interceptor;

import cn.qup.infra.pierre.core.pojo.ApiResult;
import cn.qup.infra.pierre.core.properties.AppRuntime;
import cn.qup.infra.pierre.springbootstarter.pojo.Feature;
import cn.qup.infra.pierre.springbootstarter.pojo.ValidateResponse;
import cn.qup.infra.pierre.springbootstarter.service.IPMSValidateService;
import cn.qup.infra.pierre.springbootstarter.util.HttpRequestUtil;
import cn.qup.infra.pierre.springbootstarter.util.net.RequestReaderHttpServletRequestWrapper;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Slf4j
@Component
public class AutoKeyAuthInterceptor implements HandlerInterceptor {

    @Autowired
    AppRuntime appRuntime;

    @Autowired
    @Qualifier("keyAuthNeedsList")
    List<Feature> keyAuthNeedsList;

    @Autowired(required = false)
    IPMSValidateService ipmsValidateService;
//    private IPMSValidateService ipmsValidateService;

    @Override
    public boolean preHandle(@NotNull HttpServletRequest request, @NotNull HttpServletResponse response, @NotNull Object handler) throws Exception {
        Optional<Feature> optional =keyAuthNeedsList.stream().filter(x->x.getEndpoint().contains(request.getRequestURI())).findAny();
        if(!optional.equals(Optional.empty())) {
            Feature feature = optional.get();
            // 只对加了 @KeyAuthNeeds 注解的接口进行校验
            ValidateResponse validateResponse = ipmsValidateService.pmsKeyAuthValidate(request);
            if(validateResponse==null){
                response.setStatus(502);
                HttpRequestUtil.responseOutWithJson(response, new ApiResult<>(502, "后端鉴权服务未响应，请稍候再试",null));
                return false;
            }
            if(!(validateResponse.getCode().equals("0") || validateResponse.getCode().equals("200"))){
                response.setStatus(Integer.parseInt(validateResponse.getCode()));
                HttpRequestUtil.responseOutWithJson(response, new ApiResult<>(Integer.valueOf(validateResponse.getCode()), validateResponse.getMessage(),null));
                return false;
            }
            // 后端鉴权服务通过，开始插入自定义 header
            for (Map.Entry<String, String> keySet : validateResponse.getInsertHeaders().entrySet()) {
                if (request instanceof RequestReaderHttpServletRequestWrapper) {
                    // wrapper 模式，对 wrapper 执行自定义 header 添加动作
                    ((RequestReaderHttpServletRequestWrapper) request).putHeader(keySet.getKey(), keySet.getValue());
                } else {
                    // 原生 HttpServletRequest 使用反射查找 header 对象执行 addHeader
                    HttpRequestUtil.reflectSetHeader(request, keySet.getKey(), keySet.getValue());
                }
            }
        }
        return true;
    }

    @Override
    public void postHandle(@NotNull HttpServletRequest request, @NotNull HttpServletResponse response, @NotNull Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(@NotNull HttpServletRequest request, @NotNull HttpServletResponse response, @NotNull Object handler, Exception ex) throws Exception {

    }
}
