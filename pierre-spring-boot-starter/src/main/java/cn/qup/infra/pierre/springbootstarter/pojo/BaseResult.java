package cn.qup.infra.pierre.springbootstarter.pojo;

import cn.qup.infra.pierre.core.enums.CommonErrorCode;
import lombok.Data;
import java.io.Serializable;

@Data
public class BaseResult<T> implements Serializable {

    private static final long serialVersionUID = 1L;
    private String code;
    private String msg;
    private T data;

//    public boolean isSuccess() {
//
//        return CommonErrorCode.SUCCESS.getCode().equals(this.code);
//    }

    public static <T> BaseResult<T> ok() {

        return restBaseResult((T)null, CommonErrorCode.SUCCESS);
    }

    public static <T> BaseResult<T> ok(T data) {

        return restBaseResult(data, CommonErrorCode.SUCCESS);
    }

    public static <T> BaseResult<T> fail() {

        return restBaseResult((T)null, CommonErrorCode.UNKNOWN_EXCEPTION);
    }

    public static <T> BaseResult<T> fail(CommonErrorCode baseCodeMsg) {

        return restBaseResult((T)null, baseCodeMsg);
    }

    public static <T> BaseResult<T> fail(CommonErrorCode baseCodeMsg, String placeHolder) {

        return restBaseResult((T)null,baseCodeMsg.getCode(), String.format(baseCodeMsg.getMsg(), placeHolder));
    }

    public static <T> BaseResult<T> fail(String code, String msg) {

        return restBaseResult((T)null, code, msg);
    }

    private static <T> BaseResult<T> restBaseResult(T data, CommonErrorCode baseCodeMsg) {

        return restBaseResult(data, baseCodeMsg.getCode(), baseCodeMsg.getMsg());
    }

    private static <T> BaseResult<T> restBaseResult(T data, String code, String msg) {

        BaseResult<T> apiBaseResult = new BaseResult<>();
        apiBaseResult.setCode(code);
        apiBaseResult.setData(data);
        apiBaseResult.setMsg(msg);
        return apiBaseResult;
    }

    public BaseResult() {

    }

}
