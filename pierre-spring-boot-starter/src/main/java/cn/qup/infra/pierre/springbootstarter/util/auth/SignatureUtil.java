package cn.qup.infra.pierre.springbootstarter.util.auth;

import cn.qup.infra.pierre.springbootstarter.pojo.SignatureBody;
import cn.qup.infra.pierre.springbootstarter.util.net.RequestReaderHttpServletRequestWrapper;
import com.alibaba.fastjson2.JSON;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

/**
 * 签名工具类，用于验证请求的签名
 */
public class SignatureUtil {

    /**
     * 验证请求的签名
     *
     * @param request HttpServletRequest对象
     * @param secret 密钥
     * @return 签名验证结果，true表示签名有效，false表示签名无效
     */
    public static boolean verifySignature(HttpServletRequest request, String secret) {
        // 检查请求头中是否包含signature，如果没有则返回false
        if (request.getHeader("signature") == null) {
            return false;
        }
        // 计算请求的签名，并与请求头中的signature进行比较
        return Objects.equals(calculateSignature(request, secret), request.getHeader("signature"));
    }

    /**
     * 计算请求的签名值
     *
     * @param request HttpServletRequest对象
     * @param secret 密钥
     * @return 计算得到的签名值
     */
    private static String calculateSignature(HttpServletRequest request, String secret) {
        try {
            // 构建签名需要的请求体
            SignatureBody body = new SignatureBody();
            body.setUrl(request.getRequestURL().toString());
            body.setMethod(request.getMethod().toLowerCase());
            // 如果请求头中包含nonce，则设置到请求体中
            if (request.getHeader("nonce") != null) {
                body.setNonce(request.getHeader("nonce"));
            }
            // 根据请求方法不同，处理GET和POST请求
            switch (request.getMethod().toLowerCase()) {
                case "get":
                    body.setQueryString(request.getQueryString());
                    break;
                case "post":
                    RequestReaderHttpServletRequestWrapper wrapper = new RequestReaderHttpServletRequestWrapper(request);
                    // 读取POST请求的请求体内容
                    body.setBody(wrapper.inputStream2String(wrapper.getInputStream()));
                    body.setQueryString(request.getQueryString());
                    break;
            }
            // 将请求体转换为JSON字符串
            String data = JSON.toJSONString(body);
            // 使用HMAC-SHA256算法计算签名
            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(secret.getBytes(StandardCharsets.UTF_8), "HmacSHA256");
            sha256_HMAC.init(secret_key);
            byte[] array = sha256_HMAC.doFinal(data.getBytes(StandardCharsets.UTF_8));
            // 将签名结果转换为十六进制字符串
            StringBuilder sb = new StringBuilder();
            for (byte item : array) {
                sb.append(Integer.toHexString((item & 0xFF) | 0x100), 1, 3);
            }
            return sb.toString().toLowerCase();
        } catch (Exception e) {
            return null;
        }
    }
}
