package cn.qup.infra.pierre.springbootstarter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PierreStarterApplication {

    public static void main(String[] args) {
        SpringApplication.run(PierreStarterApplication.class, args);
    }
}
