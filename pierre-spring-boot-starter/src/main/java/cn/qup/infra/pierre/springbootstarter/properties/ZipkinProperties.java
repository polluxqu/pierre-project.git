package cn.qup.infra.pierre.springbootstarter.properties;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "pierre.trace")
@EnableConfigurationProperties({ZipkinProperties.class})
public class ZipkinProperties {

    private String url;
//    private String serviceName;
    // 连接超时时间
    private int connectTimeout = 6000;
    // 读取超时时间
    private int readTimeout=6000;
    // 是否启动压缩
    private boolean compressionEnabled = true;
    // 上传 span 的间隔时间
    private int flushInterval=1;
    private float samplerRate=1;

}
