package cn.qup.infra.pierre.springbootstarter.advice;

import cn.qup.infra.pierre.core.enums.CommonErrorCode;
import cn.qup.infra.pierre.core.pojo.ApiResult;
import cn.qup.infra.pierre.springbootstarter.constant.SwaggerUrl;
import cn.qup.infra.pierre.springbootstarter.pojo.BaseResult;
import com.alibaba.fastjson.JSON;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import java.util.Objects;

@Slf4j
@Configuration
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@RestControllerAdvice(annotations = {RestController.class})
public class BaseResultResponseAdvice implements ResponseBodyAdvice<Object> {

    @Override
    public boolean supports(@NotNull MethodParameter returnType, @NotNull Class<? extends HttpMessageConverter<?>> converterType) {
        return true;
    }

    /**
     * 在响应体写入前对响应体进行处理。
     *
     * @param body 原始响应体对象。
     * @param methodParameter 方法参数信息，用于判断期望的响应类型。
     * @param selectedContentType 选中的内容类型。
     * @param selectedConverterType 选中的消息转换器类型。
     * @param serverHttpRequest 服务器HttpRequest对象。
     * @param serverHttpResponse 服务器HttpResponse对象。
     * @return 修改后的响应体对象。
     */
    @Override
    @SuppressWarnings("rawtypes")
    public Object beforeBodyWrite(Object body, @NotNull MethodParameter methodParameter,
                                  @NotNull MediaType selectedContentType,
                                  @NotNull Class<? extends HttpMessageConverter<?>> selectedConverterType,
                                  @NotNull ServerHttpRequest serverHttpRequest,
                                  @NotNull ServerHttpResponse serverHttpResponse) {
        try {
            // 针对 swagger, actuator 等接口不进行统一返回结构转换
            if(SwaggerUrl.List.stream().anyMatch(x -> serverHttpRequest.getURI().getPath().toLowerCase().contains(x))){
                return body;
            }
            Class<?> parameterType = methodParameter.getParameterType();
            switch (parameterType.getSimpleName()){
                case "ResponseEntity":{
                    if(body instanceof ResponseEntity){
                        ResponseEntity responseEntity = (ResponseEntity) body;
                        if(responseEntity.getStatusCode().equals(HttpStatus.OK)){
                            return BaseResult.ok(responseEntity.getBody());
                        }else{
                            return BaseResult.fail(responseEntity.getStatusCode().toString(), Objects.requireNonNull(responseEntity.getBody()).toString());
                        }
                    }else{
                        return BaseResult.ok(body);
                    }
                }
                case "BaseResult":{
                    BaseResult baseResult = (BaseResult) body;
                    if(!baseResult.getCode().equalsIgnoreCase("200")){
                        // 对于非 200 的请求，将 BaseResult 中的 code 透传至请求的 Http Status Code
                        try{
                            serverHttpResponse.setStatusCode(HttpStatus.valueOf(Integer.parseInt(baseResult.getCode())));
                        }catch (Exception e){
                            // 大部分异常的可能是 BaseResult 中的 code 为应用自定义 code，比如 100020 这种，而 serverHttpResponse
                            // 的 setStatusCode 接受标准的 HttpStatus，故非标的 code 在执行 HttpStatus.valueOf 时会抛出异常
                            // 这里就统一将非标的 code 改为 500
                            serverHttpResponse.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR);
                        }

                    }
                    return body;
                }
                case "String":{
                    serverHttpResponse.getHeaders().setContentType(MediaType.APPLICATION_JSON);
//                    return BaseResult.ok(JSON.toJSONString(body));
                    return JSON.toJSONString(BaseResult.ok(body));
                }
                case "ApiResult":{
                    ApiResult apiResult = (ApiResult) body;
                    if(apiResult.getCode().equals(200) | apiResult.getCode().equals(0)){
                        return BaseResult.ok(apiResult.getData());
                    }else{
                        return BaseResult.fail(apiResult.getCode().toString(), apiResult.getMessage());
                    }
                }
                default:{
                    return BaseResult.ok(body);
                }
            }
        } catch (Throwable t) {
            log.warn("BaseResultResponseAdvice error {}", t.getMessage());
//            t.printStackTrace();
            return BaseResult.fail(CommonErrorCode.TRANSFORM_RESPONSE_EXCEPTION,t.getMessage());
        }
    }
}
