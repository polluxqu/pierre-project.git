package cn.qup.infra.pierre.springbootstarter.util.interceptor;

import cn.qup.infra.pierre.core.pojo.ApiResult;
import cn.qup.infra.pierre.springbootstarter.constant.SwaggerUrl;
import cn.qup.infra.pierre.springbootstarter.util.HttpRequestUtil;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
public class DisableSwaggerPublicAccessInterceptor implements HandlerInterceptor {

    private static final List<String> internalIP = new ArrayList<>();
    static {
        internalIP.add("10.");
        internalIP.add("127.0.0.1");
        internalIP.add("localhost");
        internalIP.add("172.");
        internalIP.add("192.168.");
    }

    @Override
    public boolean preHandle(@NotNull HttpServletRequest request, @NotNull HttpServletResponse response, @NotNull Object handler) throws Exception {
        if(SwaggerUrl.List.stream().anyMatch(x -> request.getRequestURI().toLowerCase().contains(x))){
            // 命中 Swagger 相关 Url，判断是否是内网访问
            if(internalIP.stream().noneMatch(x->HttpRequestUtil.getIPAddress(request).contains(x))){
                // 非内网访问，直接阻断
                log.info(String.format("【Swagger】request ip: %s, request url: %s", HttpRequestUtil.getIPAddress(request),request.getRequestURI().toLowerCase()));
                response.setStatus(403);
                HttpRequestUtil.responseOutWithJson(response, new ApiResult<>(403, "Access Forbidden",
                        String.format("disallowed to access: %s", request.getRequestURI())));
                return false;
            }
        }
        return true;
    }

    @Override
    public void postHandle(@NotNull HttpServletRequest request, @NotNull HttpServletResponse response, @NotNull Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(@NotNull HttpServletRequest request, @NotNull HttpServletResponse response, @NotNull Object handler, Exception ex) throws Exception {

    }


}
