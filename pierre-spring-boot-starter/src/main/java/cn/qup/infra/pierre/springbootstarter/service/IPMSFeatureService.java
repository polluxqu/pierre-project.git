package cn.qup.infra.pierre.springbootstarter.service;

import cn.qup.infra.pierre.core.pojo.ApiResult;
import cn.qup.infra.pierre.core.properties.AppRuntime;
import cn.qup.infra.pierre.springbootstarter.pojo.Feature;

import java.util.List;

public interface IPMSFeatureService {

    ApiResult<Boolean> registerFeature(AppRuntime appRuntime, List<Feature> features);

}
