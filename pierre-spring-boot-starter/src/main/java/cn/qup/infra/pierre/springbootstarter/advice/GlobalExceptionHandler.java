package cn.qup.infra.pierre.springbootstarter.advice;

import cn.qup.infra.pierre.core.enums.CommonErrorCode;
import cn.qup.infra.pierre.core.exception.BizException;
import cn.qup.infra.pierre.springbootstarter.pojo.BaseResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.lang.reflect.UndeclaredThrowableException;
import java.util.StringJoiner;

/**
 * 全局异常处理器，用于捕获和处理应用中抛出的各类异常
 * 并返回统一格式的响应结果
 */
@Slf4j
@Configuration
@RestControllerAdvice
public class GlobalExceptionHandler {

    /**
     * 捕获参数校验异常
     * 当控制器方法中的参数绑定出现校验错误时，此方法将被调用
     *
     * @param e MethodArgumentNotValidException 异常对象
     * @return 包含错误信息的 ResponseEntity 对象
     */
    @ExceptionHandler({MethodArgumentNotValidException.class})
    public <T> ResponseEntity<BaseResult<T>> handle(MethodArgumentNotValidException e){
       // 创建并返回一个包含参数校验错误信息的响应结果
       BaseResult<T> baseResult= BaseResult.fail(CommonErrorCode.APP_PARAM_ERROR.getCode(),CommonErrorCode.APP_PARAM_ERROR.getMsg());
       ResponseEntity<BaseResult<T>> responseEntity = ResponseEntity.status(HttpStatus.valueOf(Integer.parseInt(baseResult.getCode())))
                .body(baseResult);
        return responseEntity;
    }

    /**
     * 捕获业务异常
     * 当应用中抛出 BizException 异常时，此方法将被调用
     *
     * @param e BizException 异常对象
     * @return 包含错误码和错误信息的 ResponseEntity 对象
     */
    @ExceptionHandler({BizException.class})
    public <T> ResponseEntity<BaseResult<T>> handle(BizException e){
        // 创建并返回一个包含业务异常错误信息的响应结果
        BaseResult<T> baseResult= BaseResult.fail(e.getCode(),e.getMsg());
        ResponseEntity<BaseResult<T>> responseEntity = ResponseEntity.status(HttpStatus.valueOf(Integer.parseInt(baseResult.getCode())))
                .body(baseResult);
        return responseEntity;
    }

    /**
     * 捕获 HttpMessageNotReadableException 异常
     * 当前端传入的请求体无法被正确解析时，此方法将被调用
     *
     * @param e HttpMessageNotReadableException 异常对象
     * @return 包含参数错误信息的 ResponseEntity 对象
     */
    @ExceptionHandler({HttpMessageNotReadableException.class})
    public <T> ResponseEntity<BaseResult<T>> handle(HttpMessageNotReadableException e){
        // 创建并返回一个包含请求体解析错误信息的响应结果
        BaseResult<T> baseResult= BaseResult.fail(CommonErrorCode.APP_PARAM_ERROR);
        ResponseEntity<BaseResult<T>> responseEntity = ResponseEntity.status(HttpStatus.valueOf(Integer.parseInt(baseResult.getCode())))
                .body(baseResult);
        return responseEntity;
    }

    /**
     * 捕获未声明的Throwable异常
     * 当方法中抛出未声明的Throwable类型异常时，此方法将被调用
     *
     * @param e UndeclaredThrowableException 异常对象
     * @return 包含调用被拒绝信息的 ResponseEntity 对象
     */
    @ExceptionHandler({UndeclaredThrowableException.class})
    public <T> ResponseEntity<BaseResult<T>> handle(UndeclaredThrowableException e){
        // 记录警告日志，并创建一个包含调用被拒绝信息的响应结果返回
        log.warn("",e);
        BaseResult<T> baseResult= BaseResult.fail(CommonErrorCode.INVOKE_IS_REJECT);
        ResponseEntity<BaseResult<T>> responseEntity = ResponseEntity.status(HttpStatus.valueOf(Integer.parseInt(baseResult.getCode())))
                .body(baseResult);
        return responseEntity;
    }

    /**
     * 捕获通用异常
     * 当应用中抛出除上述特殊情况外的其他异常时，此方法将被调用
     *
     * @param e Exception 异常对象
     * @return 包含未知异常信息的 ResponseEntity 对象
     */
    @ExceptionHandler({Exception.class})
    public <T> ResponseEntity<BaseResult<T>> handle(Exception e){
        // 可选择记录日志，并创建一个包含未知异常错误信息的响应结果返回
//        log.warn("",e);
        StringJoiner joiner = new StringJoiner(":");
        joiner.add(CommonErrorCode.INVOKE_IS_REJECT.getMsg()).add(e.getMessage());
        BaseResult<T> baseResult= BaseResult.fail(CommonErrorCode.UNKNOWN_EXCEPTION.getCode(), joiner.toString());
        ResponseEntity<BaseResult<T>> responseEntity = ResponseEntity.status(HttpStatus.valueOf(Integer.parseInt(baseResult.getCode())))
                .body(baseResult);
        return responseEntity;
    }

    /**
     * 构造函数，用于初始化 GlobalExceptionHandler 实例
     *
     * @param messageSource MessageSource 实例，用于国际化错误消息
     */
    public GlobalExceptionHandler(MessageSource messageSource){
    }
}

