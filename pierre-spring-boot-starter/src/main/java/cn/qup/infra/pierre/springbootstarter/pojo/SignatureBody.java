package cn.qup.infra.pierre.springbootstarter.pojo;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SignatureBody {

    @JSONField(ordinal = 0)
    private String url;
    @JSONField(ordinal = 1)
    private String method;
    @JSONField(ordinal = 2)
    private String body;
    @JSONField(ordinal = 3)
    private String queryString;
    @JSONField(ordinal = 4)
    private String nonce;

}
