package cn.qup.infra.pierre.springbootstarter.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ValidateResponse {

    private String code;
    private String message;
    private Map<String,String> insertHeaders;

}
