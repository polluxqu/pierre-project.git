package cn.qup.infra.pierre.springbootstarter.annotation;

import java.lang.annotation.*;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface KeyAuthNeeds {
    boolean headerOnly() default false;
    String[] roles() default {};
}
