package cn.qup.infra.pierre.springbootstarter.constant;

public enum AccessLimitRegion {
    IP,
    GLOBAL
}
