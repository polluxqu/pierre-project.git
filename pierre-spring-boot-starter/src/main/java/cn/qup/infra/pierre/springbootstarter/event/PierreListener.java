package cn.qup.infra.pierre.springbootstarter.event;

/**
 * 定义通用事件监听器
 */
public interface PierreListener<E extends PierreEvent> {

    public <E extends PierreEvent> void onApplicationEvent(E event);


}
