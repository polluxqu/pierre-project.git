package cn.qup.infra.pierre.springbootstarter.config;

import cn.qup.infra.pierre.core.properties.AppRuntime;
import cn.qup.infra.pierre.springbootstarter.properties.ZipkinProperties;
import com.github.kristofa.brave.*;
import com.github.kristofa.brave.http.DefaultSpanNameProvider;
import com.github.kristofa.brave.http.HttpSpanCollector;
import com.github.kristofa.brave.okhttp.BraveOkHttpRequestResponseInterceptor;
import com.github.kristofa.brave.servlet.BraveServletFilter;
import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.util.ObjectUtils;

@Slf4j
public class ZipkinAutoConfiguration {

    @Autowired
    ZipkinProperties zipkinProperties;
    @Autowired
    AppRuntime appRuntime;

    /**
     * 配置 span 收集器
     * @return
     */
    @Bean
    public SpanCollector spanCollector() {

        if(!ObjectUtils.isEmpty(zipkinProperties.getUrl())){
            HttpSpanCollector.Config config = HttpSpanCollector.Config.builder()
                    .connectTimeout(zipkinProperties.getConnectTimeout())
                    .compressionEnabled(zipkinProperties.isCompressionEnabled())
                    .flushInterval(zipkinProperties.getFlushInterval())
                    .readTimeout(zipkinProperties.getReadTimeout())
                    .build();
            return HttpSpanCollector.create(zipkinProperties.getUrl(), config, new EmptySpanCollectorMetricsHandler());
        }else{
            log.warn("【Pierre-Trace】: None zipkin server url found, skip zipkin configuration.");
            return new EmptySpanCollector();
        }

    }

    /**
     * 配置采集率
     * @param spanCollector
     * @return
     */

    @Bean
    public Brave brave(SpanCollector spanCollector) {

        Brave.Builder builder = new Brave.Builder(appRuntime.getAppName() + "-" + appRuntime.getInstanceId());
        if(!ObjectUtils.isEmpty(zipkinProperties.getUrl())){
            builder.spanCollector(spanCollector)
                    .traceSampler(Sampler.create(zipkinProperties.getSamplerRate()))
                    .build();
        }
        return builder.build();
    }

    /**
     * @Description: 设置server的（服务端收到请求和服务端完成处理，并将结果发送给客户端）过滤器
     * @Param:
     * @return: 过滤器
     */

    @Bean
    public BraveServletFilter braveServletFilter(Brave brave) {

            BraveServletFilter filter = new BraveServletFilter(brave.serverRequestInterceptor(),
                    brave.serverResponseInterceptor(), new DefaultSpanNameProvider());
            return filter;
    }

    /**
     * @Description: 设置client的 rs和cs的拦截器
     * @Param:
     * @return: OkHttpClient 返回请求实例
     */

    @Bean
    public OkHttpClient okHttpClient(Brave brave) {
        if(!ObjectUtils.isEmpty(zipkinProperties.getUrl())){
            OkHttpClient httpClient = new OkHttpClient.Builder()
                    .addInterceptor(new BraveOkHttpRequestResponseInterceptor(
                            brave.clientRequestInterceptor(),
                            brave.clientResponseInterceptor(),
                            new DefaultSpanNameProvider())).build();
            return httpClient;
        }else{
            return new OkHttpClient.Builder().build();
        }
    }
}
