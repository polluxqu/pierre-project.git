package cn.qup.infra.pierre.springbootstarter.util;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.concurrent.ConcurrentHashMap;

public class CacheUtil {

    /**
     * 用于存储键值对的并发映射表，其中值为Timestamp类型，确保线程安全。
     */
    private static final ConcurrentHashMap<String, Timestamp> cache = new ConcurrentHashMap<>();

    /**
     * 将给定的键值对放入缓存中。
     *
     * @param key   缓存中的键
     * @param value 缓存中的值，一个时间戳
     */
    public static void put(String key, Timestamp value) {
        cache.put(key, value);
    }

    /**
     * 根据键和指定的时间范围检查缓存中是否存在有效的条目。
     *
     * @param key       缓存中的键
     * @param during    指定的时间范围（以毫秒为单位）
     * @return 如果存在可以匹配的key，则返回当前时间范围内的key条目数，否则返回0
     */
    public static int get(String key, long during) {
        Instant now = Instant.now();
        return (int) cache.entrySet().stream()
                .filter(entry -> now.minusMillis(during).isBefore(entry.getValue().toInstant()) &&
                        entry.getKey().equals(key))
                .count();
    }

    /**
     * 从缓存中移除指定键的过期条目。
     *
     * @param key       缓存中的键
     * @param during    指定的过期时间范围（以毫秒为单位）
     */
    public static void removeExpired(String key, long during) {
        cache.entrySet().removeIf(entry -> entry.getValue().before(new Timestamp(System.currentTimeMillis() - during))
        && entry.getKey().equals(key));
    }

}
