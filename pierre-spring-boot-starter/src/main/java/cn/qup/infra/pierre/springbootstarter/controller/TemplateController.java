package cn.qup.infra.pierre.springbootstarter.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/pierre/web")
public class TemplateController {

    @GetMapping("/template")
    public String serveTemplate(Model model) {
        // 添加模型数据，如果需要的话
        model.addAttribute("title", "My Starter Project");
        model.addAttribute("message", "Hello from Starter!");

        // 返回模板文件的名称
        return "starter-template";
    }

    @GetMapping("/")
    public String index(Model model) {
        // 添加模型数据，如果需要的话
//        model.addAttribute("title", "My Starter Project");
//        model.addAttribute("message", "Hello from Starter!");

        // 返回模板文件的名称
        return "index";
    }
}
