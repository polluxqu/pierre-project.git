package cn.qup.infra.pierre.springbootstarter.config;

import cn.qup.infra.pierre.springbootstarter.annotation.EnableAutoRegisterPMS;
import cn.qup.infra.pierre.springbootstarter.annotation.KeyAuthNeeds;
import cn.qup.infra.pierre.springbootstarter.annotation.SignatureNeeds;
import cn.qup.infra.pierre.springbootstarter.pojo.Feature;
import cn.qup.infra.pierre.springbootstarter.util.ReflectUtil;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.reflections.Reflections;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMapping;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;


@Slf4j
@Configuration
public class PMSAutoConfiguration {

    @Bean(name = "keyAuthNeedsList")
    public List<Feature> keyAuthNeedsList(){
        return getAnnotationList(KeyAuthNeeds.class);
    }

    @Bean(name = "signatureNeedsList")
    public List<Feature> signatureNeedsList(){
        return getAnnotationList(SignatureNeeds.class);
    }

    @Bean(name = "allControllerList")
    public List<Feature> allControllerList(){
        return getAnnotationList(RequestMapping.class);
    }

    @Bean("EnableAutoRegisterPMS")
    boolean EnableAutoRegisterPMS(){
        Reflections reflections = ReflectUtil.getMainPackageReflections();
        // // 获取主应包下所有类中带有 EnableRegisterPMS 注解的类（其实只需要启动类上有添加就可以了）
        Set<Class<?>> clz = reflections.getTypesAnnotatedWith(EnableAutoRegisterPMS.class);
        return clz.size()>0 ? true : false;
    }

    /**
     * 反射获取应用下所有 Controller 中的 RequestMapping 下带指定注解的接口
     * @param clz
     * @return
     */
    private List<Feature> getAnnotationList(Class<? extends Annotation> clz) {

        Reflections reflections = ReflectUtil.getMainPackageReflections();
        // 获取主应用包下所有类中带 KeyAuthNeeds 注解的方法
        List<Feature> features = new ArrayList<>();
        try{
            Set<Method> methods = reflections.getMethodsAnnotatedWith(clz);
            for (Method method : methods
            ) {
                Class<?> controller = method.getDeclaringClass();
                RequestMapping annotation = controller.getAnnotation(RequestMapping.class);
                String requestMappingByController ="";
                if(annotation!=null){
                    // 有的 controller 包在类一级有可能未设定 @RequestMapping 注解
                    requestMappingByController = annotation.value()[0];
                }
                Feature feature = getFeature(method, requestMappingByController, controller);
                features.add(feature);
            }
        }catch (Exception e){
            log.warn("",e);
//            e.printStackTrace();
        }
//        log.info("Reflect Controller found {} {} in All RequestMapping.",features.size(),clz.getSimpleName());
        return features;
    }

    @NotNull
    private static Feature getFeature(Method method, String requestMappingByController, Class<?> controller) {
        String keyAuthUrl = requestMappingByController + method.getAnnotation(RequestMapping.class).value()[0];
        Api api = controller.getAnnotation(Api.class);
        String categoryName ="";
        if(api!=null){
            categoryName = api.tags()[0];
        }else{
            categoryName = controller.getSimpleName();
        }
        Feature feature = new Feature();
        feature.setCategory(categoryName);
        feature.setName(method.getName());
        feature.setDescription(method.getAnnotation(ApiOperation.class).value());
        if(method.getAnnotation(RequestMapping.class).method().length>0){
            feature.setMethod(method.getAnnotation(RequestMapping.class).method()[0].name());
        }else{
            // 如果 controller 中的接口未指定方法，则写入 ALL
            feature.setMethod("ALL");
        }
        feature.setEndpoint(keyAuthUrl);
        feature.setComment(method.getAnnotation(ApiOperation.class).notes());
        return feature;
    }
}
