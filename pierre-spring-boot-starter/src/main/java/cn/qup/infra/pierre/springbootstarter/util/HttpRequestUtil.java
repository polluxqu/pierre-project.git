package cn.qup.infra.pierre.springbootstarter.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.http.MimeHeaders;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@Slf4j
public class HttpRequestUtil {
    /**
     * 从 HttpServletRequest 中获取调用方 IP 信息
     *
     * @param request 透传的 request 对象
     * @return request的发起ip
     */
    public static String getIPAddress(HttpServletRequest request) {
        String ip = null;

        //X-Forwarded-For：Squid 服务代理
        String ipAddresses = request.getHeader("X-Forwarded-For");

        if (ipAddresses == null || ipAddresses.isEmpty() || "unknown".equalsIgnoreCase(ipAddresses)) {
            //Proxy-Client-IP：apache 服务代理
            ipAddresses = request.getHeader("Proxy-Client-IP");
        }

        if (ipAddresses == null || ipAddresses.isEmpty() || "unknown".equalsIgnoreCase(ipAddresses)) {
            //WL-Proxy-Client-IP：weblogic 服务代理
            ipAddresses = request.getHeader("WL-Proxy-Client-IP");
        }

        if (ipAddresses == null || ipAddresses.isEmpty() || "unknown".equalsIgnoreCase(ipAddresses)) {
            //HTTP_CLIENT_IP：有些代理服务器
            ipAddresses = request.getHeader("HTTP_CLIENT_IP");
        }

        if (ipAddresses == null || ipAddresses.isEmpty() || "unknown".equalsIgnoreCase(ipAddresses)) {
            //X-Real-IP：nginx服务代理
            ipAddresses = request.getHeader("X-Real-IP");
        }

        //有些网络通过多层代理，那么获取到的ip就会有多个，一般都是通过逗号（,）分割开来，并且第一个ip为客户端的真实IP
        if (ipAddresses != null && !ipAddresses.isEmpty()) {
            ip = ipAddresses.split(",")[0];
        }

        //还是不能获取到，最后再通过request.getRemoteAddr();获取
        if (ip == null || ip.isEmpty() || "unknown".equalsIgnoreCase(ipAddresses)) {
            ip = request.getRemoteAddr();
        }
        return ip;
    }

    /**
     * 自定义 HttpServletResponse
     * @param response 透传的 response
     * @param o 要写入的对象
     */
    public static void responseOutWithJson(HttpServletResponse response, Object o){
        JSONObject jsonObject = JSONObject.parseObject(JSON.toJSONString(o));
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json;charset=utf-8");
        try{
            response.getWriter().append(jsonObject.toJSONString());
        }catch (IOException e){
            log.warn("response out json error with {}",e.getMessage());
//            e.printStackTrace();
        }
    }

    /**
     * 从 HttpServletRequest 中获取 Request Headers
     *
     * @param request 透传 request
     * @return 返回透传的 request 中的 headers
     */
    public static Map<String, String> getHeaders(HttpServletRequest request) {
        Iterator<String> iterator = request.getHeaderNames().asIterator();
        Map<String, String> map = new HashMap<>();
        while (iterator.hasNext()) {
            String key = iterator.next();
            map.put(key, request.getHeader(key));
        }
        return map;
    }

    /**
     * 原生 HttpServletRequest 使用反身查找 header 对象执行 addHeader
     * @param request 透传 request
     * @param key 要注入的header key
     * @param value 要注入的header value
     */
    public static void reflectSetHeader(HttpServletRequest request, String key, String value){
        Class<? extends HttpServletRequest> requestClass = request.getClass();
        try{
            Field request1 = requestClass.getDeclaredField("request");
            request1.setAccessible(true);
            Object o = request1.get(request);
            Field coyoteRequest = o.getClass().getDeclaredField("coyoteRequest");
            coyoteRequest.setAccessible(true);
            Object o1 = coyoteRequest.get(o);
            Field headers = o1.getClass().getDeclaredField("headers");
            headers.setAccessible(true);
            MimeHeaders o2 = (MimeHeaders)headers.get(o1);
            o2.addValue(key).setString(value);
        } catch (Exception e) {
            log.error("reflectSetHeader error: {}", e.getMessage());
//            e.printStackTrace();
        }
    }

}
