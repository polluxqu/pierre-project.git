package cn.qup.infra.pierre.springbootstarter.config;

import cn.qup.infra.pierre.core.util.DateTimeUtil;
import cn.qup.infra.pierre.springbootstarter.properties.ExtendGitProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@Slf4j
public class GitPropertiesAutoConfiguration {

    @Bean
    ExtendGitProperties extendGitProperties(){
        ExtendGitProperties extendGitProperties = new ExtendGitProperties();
        Properties properties = getGitProperties();
        if(properties!=null){
            extendGitProperties.setCommitId(properties.getProperty("git.branch"));
            extendGitProperties.setCommitId(properties.getProperty("git.commit.id"));
            if(properties.getProperty("git.commit.id.abbrev")==null){
                extendGitProperties.setShortCommitId(properties.getProperty("git.commit.id").substring(0,7));
            }else{
                extendGitProperties.setShortCommitId(properties.getProperty("git.commit.id.abbrev"));
            }
            extendGitProperties.setGitOriginUrl(properties.getProperty("git.remote.origin.url"));
            extendGitProperties.setAuthorName(properties.getProperty("git.commit.user.name"));
            extendGitProperties.setAuthorEmail(properties.getProperty("git.commit.user.email"));
            if(properties.getProperty("git.commit.time")!=null){
                String dt = DateTimeUtil.getLocalTime(properties.getProperty("git.commit.time"),
                        DateTimeUtil.DATE_TIME_FORMAT_UTC_YYYY_MM_DD_HH_MI_SS_Z);
                extendGitProperties.setCommitTime(dt);
            }
            extendGitProperties.setComment(properties.getProperty("git.commit.message.full"));
            extendGitProperties.setCount(properties.getProperty("git.total.commit.count"));
        }
        return extendGitProperties;
    }

    private Properties getGitProperties(){
        Properties properties=null;
        try {
            //获取文件
            InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("git.properties");
            if(inputStream!=null){
                properties = new Properties();
                //读取git.properties
                properties.load(inputStream);
            }
        }catch (IOException e){
            log.warn("load git properties failed by: {}",e.getMessage());
//            e.printStackTrace();
        }
        return properties;
    }

}
