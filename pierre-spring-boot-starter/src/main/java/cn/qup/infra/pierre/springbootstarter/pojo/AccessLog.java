package cn.qup.infra.pierre.springbootstarter.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

public class AccessLog {
    @JsonProperty(index = 8)
    public String env;

    @JsonProperty(index = 4 )
    public String verb;

    @JsonProperty(index = 1)
    public String clientIp;

    @JsonProperty(index = 2)
    public String requestUrl;

    @JsonProperty(index = 7)
    public String queryString;

    @JsonProperty(index = 3)
    public String timestamp;

    @JsonProperty(index = 8)
    public String postContent;

    @JsonProperty(index = 6)
    public Map<String, String> headers;

    @JsonProperty(index = 5)
    public String requestId;
}
