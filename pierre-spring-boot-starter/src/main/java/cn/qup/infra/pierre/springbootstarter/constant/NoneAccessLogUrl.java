package cn.qup.infra.pierre.springbootstarter.constant;

import java.util.ArrayList;
import java.util.List;

public class NoneAccessLogUrl {
    public static List<String> List = new ArrayList<>();
    static{
        List.add("/swagger-resources");
        List.add("/v3/api-docs");
        List.add("/v2/api-docs");
        List.add("/error");
        List.add("/favicon.ico");
    }

}
