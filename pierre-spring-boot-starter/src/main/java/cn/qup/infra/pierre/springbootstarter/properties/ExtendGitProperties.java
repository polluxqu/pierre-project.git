package cn.qup.infra.pierre.springbootstarter.properties;

import lombok.Data;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@EnableConfigurationProperties({ExtendGitProperties.class})
public class ExtendGitProperties {

    private String branch;
    private String commitId;
    private String gitOriginUrl;
    private String shortCommitId;
    private String commitTime;
    private String authorName;
    private String authorEmail;
    private String comment;
    private String count;

}
