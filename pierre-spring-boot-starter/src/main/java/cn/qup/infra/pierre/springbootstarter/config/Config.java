package cn.qup.infra.pierre.springbootstarter.config;

import cn.qup.infra.pierre.springbootstarter.controller.DemoController;
import cn.qup.infra.pierre.springbootstarter.controller.TemplateController;
import cn.qup.infra.pierre.springbootstarter.properties.ZipkinProperties;
import cn.qup.infra.pierre.springbootstarter.util.aspect.AccessLimitAspect;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;

@Slf4j
@Import({GitPropertiesAutoConfiguration.class,
InterceptorAutoConfiguration.class,
JacksonAutoConfiguration.class,
PMSAutoConfiguration.class,
CustomCacheAutoConfiguration.class,
SwaggerAutoConfiguration.class,
ZipkinAutoConfiguration.class,
PierreWebAutoConfiguration.class,
CoreWebAutoConfiguration.class,
DemoController.class,
TemplateController.class})
public class Config {

    @Bean
    ZipkinProperties zipkinProperties(){
        ZipkinProperties zipkinProperties = new ZipkinProperties();
        log.info("【Pierre-Starter】: zipkin configuration is '{}'", JSON.toJSONString(zipkinProperties));
        return zipkinProperties;
    }

    @Bean
    // 自动化装配 Access Limit Aspect
    AccessLimitAspect accessLimitAspect(){
        return new AccessLimitAspect();
    }

}
