package cn.qup.infra.pierre.springbootstarter.config;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import net.bytebuddy.asm.Advice;
import org.apache.tomcat.util.digester.DocumentProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.freemarker.FreeMarkerProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

@Slf4j
@Configuration
//@Order(1)
public class CoreWebAutoConfiguration implements WebMvcConfigurer {
    @Autowired
    private FreeMarkerProperties freeMarkerProperties;


    @Override
    public void configureViewResolvers(ViewResolverRegistry registry) {
        registry.freeMarker();
//        freeMarkerProperties.getSettings().put("template_loader_path", "/templates/");
        freeMarkerProperties.setTemplateLoaderPath("classpath:/templates");
        freeMarkerProperties.setContentType(new MediaType("text", "html", 0.9f));
        freeMarkerProperties.setCache(false);
        freeMarkerProperties.setSuffix(".ftl");
        freeMarkerProperties.setCharset(StandardCharsets.UTF_8);
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/resources/**")
                .addResourceLocations("classpath:/resources/"); // 如果你有静态资源，可以添加资源处理器
        registry.addResourceHandler("/assets/**")
                .addResourceLocations("classpath:/static/assets/");
    }
}
