package cn.qup.infra.pierre.springbootstarter.controller;

import cn.qup.infra.pierre.springbootstarter.annotation.AccessLimit;
import cn.qup.infra.pierre.springbootstarter.constant.AccessLimitType;
import cn.qup.infra.pierre.springbootstarter.pojo.Demo;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/api/v1/starter/web")
public class DemoController {

//    @ApiOperation(value = "测试Get Page", notes = "")
//    @RequestMapping(name = "demoList", value = "/demoList",method = RequestMethod.GET)
    @GetMapping(value = "/demoList")
    @AccessLimit(type = AccessLimitType.QUOTA, duration = 60, limit = 100)
    public ModelAndView list(Map<String, Object> map) {
        List<Demo> demoList = new ArrayList<>(4);
        demoList.add(new Demo(1, "标题1", "编程小石头1", "2501902696"));
        demoList.add(new Demo(2, "标题2", "编程小石头2", "2501902696"));
        demoList.add(new Demo(3, "标题3", "编程小石头3", "2501902696"));
        demoList.add(new Demo(4, "标题4", "编程小石头4", "2501902696"));
        map.put("demoList", demoList);
        return new ModelAndView("list", map);
    }

    @GetMapping(value = "/demoList2")
    public String list2(Model model) {
        List<Demo> demoList = new ArrayList<>(4);
        demoList.add(new Demo(1, "标题1", "编程小石头1", "2501902696"));
        demoList.add(new Demo(2, "标题2", "编程小石头2", "2501902696"));
        demoList.add(new Demo(3, "标题3", "编程小石头3", "2501902696"));
        demoList.add(new Demo(4, "标题4", "编程小石头4", "2501902696"));
        model.addAttribute("demoList",demoList);
        return "list";
    }
}
