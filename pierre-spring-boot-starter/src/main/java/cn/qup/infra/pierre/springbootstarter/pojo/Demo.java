package cn.qup.infra.pierre.springbootstarter.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Demo {

    private Integer id;
    private String title;
    private String name;
    private String wechat;
}
