package cn.qup.infra.pierre.springbootstarter.config;

import cn.qup.infra.pierre.springbootstarter.advice.BaseResultResponseAdvice;
import cn.qup.infra.pierre.springbootstarter.advice.GlobalExceptionHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
public class PierreWebAutoConfiguration {

    /**
     * 配置全局异常捕捉，统一输出格式
     * @param messageSource context message source
     * @return GlobalExceptionHandler
     */
    @Bean
    @ConditionalOnMissingBean(GlobalExceptionHandler.class)
    public GlobalExceptionHandler globalAdvice(MessageSource messageSource) {
        return new GlobalExceptionHandler(messageSource);
    }

    /**
     * 配置response body处理器
     */
    @Bean
    @ConditionalOnMissingBean(BaseResultResponseAdvice.class)
    public BaseResultResponseAdvice resultResponseBodyAdvice() {
        return new BaseResultResponseAdvice();
    }

}
