package cn.qup.infra.pierre.springbootstarter.service;

import cn.qup.infra.pierre.springbootstarter.pojo.ValidateResponse;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;


public interface IPMSValidateService {

    ValidateResponse pmsKeyAuthValidate(HttpServletRequest request);

    ValidateResponse pmsSignatureValidate(HttpServletRequest request);
}
