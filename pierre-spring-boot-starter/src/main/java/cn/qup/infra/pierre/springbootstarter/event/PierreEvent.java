package cn.qup.infra.pierre.springbootstarter.event;

import lombok.*;

import javax.annotation.Nullable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PierreEvent {

    // 所有事件标准结构需包含事件名称
    private String eventName;

    // 所有事件标准结构需包含事件ID，用于事件溯源
    @Nullable
    private String eventId;

    // 所有事件标准结构中需包含事件时间，默认为当前毫秒数
    @Nullable
    private Long eventTime ;

    public PierreEvent(String eventName) {
        this.eventName = eventName;
    }
}
