package cn.qup.infra.pierre.springbootstarter.event;

import cn.qup.infra.pierre.core.exception.BizException;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ObjectUtils;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 定义统一事件管理
 */
@Slf4j
public class EventManage {

    // 记录发布的事件和对该事件进行了监听的监听器
    @Getter
    private static final Map<String, List<PierreListener<? extends PierreEvent>>> listeners = new HashMap<>();
    // 核心线程数
    private static final int CORE_THREADS = 10;
    // 最大线程数
    private static int MAX_THREADS = 50;
    // 线程池
    private static final ExecutorService executorService = new ThreadPoolExecutor(
            CORE_THREADS, // 核心线程数
            MAX_THREADS, // 最大线程数
            0L, // 空闲线程存活时间
            TimeUnit.MILLISECONDS, // 时间单位
            new LinkedBlockingQueue<>(100), // 工作队列
            new ThreadPoolExecutor.DiscardOldestPolicy() // 拒绝策略, 当线程队列满了，再有事件来，就抛弃最旧的事件回调
    );

    /**
     * 设置最大线程数
     * @param maxThreads 最大线程数
     */
    public void setMaxThreads(int maxThreads){
        MAX_THREADS = maxThreads;
    }
    /**
     * 发布事件
     * @param event 事件对象
     */
    public static void publish(PierreEvent event) {
        try{
            if(ObjectUtils.isEmpty(event.getEventName())){
                throw new BizException("500","Event Name cannot be null");
            }
            List<PierreListener<? extends PierreEvent>> listenerList = listeners.get(event.getEventName());
            if (listenerList != null) {
                for (PierreListener<? extends PierreEvent> listener : listenerList) {
                    // 确保事件id和事件时间被正确设置
                    if(ObjectUtils.isEmpty(event.getEventId())){
                        event.setEventId(UUID.randomUUID().toString());
                    }
                    if(ObjectUtils.isEmpty(event.getEventTime())){
                        event.setEventTime(System.currentTimeMillis());
                    }
//                    //  修改事件回调至异步方法，确保事件处理不影响主流程
//                    listener.onApplicationEvent(event);
                    // 异步执行事件监听器的回调方法
                    submitListenerTask(listener, event);

                }
            }else{
                throw new BizException("500","Event Name {} not registered",event.getEventName());
            }
        }catch (BizException e){
            log.warn("publish event failed by: {}",e.getMessage());
        }

    }

    /**
     * 注册监听器
     * @param eventName 事件名称
     * @param listener 监听器
     */
    public static <E extends PierreEvent> void addListener(String eventName, PierreListener<E> listener) {
        log.info("added listener for event: {}",eventName);
        try{
            if (listeners.containsKey(eventName)){
                throw new BizException("500","Event Name {} already exists",eventName);
            }
            List<PierreListener<? extends PierreEvent>> listenerList = listeners.computeIfAbsent(eventName, k -> new ArrayList<>());
            listenerList.add(listener);
        }catch (BizException e){
            log.warn("add listener failed by: {}",e.getMessage());
        }

    }

    /**
     * 移除监听器
     * @param eventName 事件名称
     * @param listener 监听器
     */
    public static void removeListener(String eventName, PierreListener<? extends PierreEvent> listener) {
        log.info("removed listener for event: {}",eventName);
        try{
            List<PierreListener<? extends PierreEvent>> listeners = map.get(eventName);
            if (listeners != null) {
                listeners.remove(listener);
            }
        }catch (BizException e){
            log.warn("remove listener failed by: {}",e.getMessage());
        }
    }

    /**
     * 异步执行事件监听器的回调方法
     * @param listener 监听器
     * @param event 事件对象
     */
    private static void submitListenerTask(PierreListener<? extends PierreEvent> listener, PierreEvent event) {
        executorService.submit(() -> {
            try {
                listener.onApplicationEvent(event);
            } catch (Exception e) {
                // 处理任务执行时可能出现的异常
                log.warn("Async execution of event failed", e);
            }
        });
    }

}
