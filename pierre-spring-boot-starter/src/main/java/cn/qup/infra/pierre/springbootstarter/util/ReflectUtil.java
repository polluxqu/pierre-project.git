package cn.qup.infra.pierre.springbootstarter.util;

import cn.qup.infra.pierre.core.util.ApplicationContextUtil;
import lombok.extern.slf4j.Slf4j;
import org.reflections.Reflections;
import org.reflections.scanners.MethodAnnotationsScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;

@Slf4j
public class ReflectUtil {

    public static Reflections getMainPackageReflections(){
        //设置扫描路径
        String packageName = "";
        // 获取当前主应用包名
//        StackTraceElement[] stackTraceElements = new RuntimeException().getStackTrace();
//        for (StackTraceElement stackTraceElement : stackTraceElements) {
//            if ("main".equals(stackTraceElement.getMethodName())) {
//                packageName = stackTraceElement.getClassName();
//                break;
//            }
//        }
//        packageName = packageName.substring(0, packageName.lastIndexOf("."));
        packageName = ApplicationContextUtil.getApplicationPackageName();
        // 反射主应用包名下所有类

        return new Reflections(new ConfigurationBuilder()
                .setUrls(ClasspathHelper.forPackage(packageName))
                .setScanners(new MethodAnnotationsScanner(),new TypeAnnotationsScanner(), new SubTypesScanner()));
    }
}
