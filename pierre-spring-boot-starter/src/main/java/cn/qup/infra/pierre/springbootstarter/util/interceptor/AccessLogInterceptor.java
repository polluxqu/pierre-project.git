package cn.qup.infra.pierre.springbootstarter.util.interceptor;

import cn.qup.infra.pierre.core.properties.AppRuntime;
import cn.qup.infra.pierre.core.util.DateTimeUtil;
import cn.qup.infra.pierre.security.encrypt.DESSlatUtil;
import cn.qup.infra.pierre.springbootstarter.constant.NoneAccessLogUrl;
import cn.qup.infra.pierre.springbootstarter.pojo.AccessLog;
import cn.qup.infra.pierre.springbootstarter.util.HttpRequestUtil;
import cn.qup.infra.pierre.springbootstarter.util.net.RequestReaderHttpServletRequestWrapper;
import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Slf4j
@Component
public class AccessLogInterceptor implements HandlerInterceptor {

    @Autowired
    AppRuntime appRuntime;

    private final Logger logger = LoggerFactory.getLogger("ACCESS_LOG");

//    private static final List<String> NoneAccessLogContentTypeList = new ArrayList<>();
//    static {
//        NoneAccessLogContentTypeList.add("image");
//    }
    @Override
    public boolean preHandle(@NotNull HttpServletRequest request, @NotNull HttpServletResponse response, @NotNull Object handler) throws Exception {

        if (NoneAccessLogUrl.List.stream().noneMatch(x -> request.getRequestURI().toLowerCase().contains(x))) {
            AccessLog accessLog = new AccessLog();
            accessLog.requestId = UUID.randomUUID().toString();
            accessLog.env = appRuntime.getEnv().name();
            accessLog.clientIp = HttpRequestUtil.getIPAddress(request);
            accessLog.verb = request.getMethod();
            if(accessLog.verb.equalsIgnoreCase("POST") && request.getContentType().contains("application/json")){
                // 创建重写后的 HttpServletRequest
                RequestReaderHttpServletRequestWrapper wrapper = new RequestReaderHttpServletRequestWrapper(request);
                // 获取 body 参数
                String bodyParams =wrapper.inputStream2String(request.getInputStream());
                if(appRuntime.isEncryptContent()){
                    bodyParams = DESSlatUtil.Encrypt(bodyParams,"8156",true);
                }
                accessLog.postContent = bodyParams.length()>=1024 ? bodyParams.substring(0,1024) + "..." : bodyParams;
            }
            accessLog.requestUrl = request.getRequestURI();
            accessLog.queryString = request.getQueryString();
            accessLog.headers = HttpRequestUtil.getHeaders(request);
            accessLog.timestamp = DateTimeUtil.dateFormat(new Date(), DateTimeUtil.DATE_TIME_FORMAT_YYYY_MM_DD_HH_MI_SS);
            ObjectMapper mapper = new ObjectMapper();
            logger.info("【Access Log】: " + mapper.writeValueAsString(accessLog));
            response.setHeader("pierre-x-request-id",accessLog.requestId);
        }
        return true;
    }

    @Override
    public void postHandle(@NotNull HttpServletRequest request, @NotNull HttpServletResponse response, @NotNull Object handler, ModelAndView modelAndView) throws Exception {
    }

    @Override
    public void afterCompletion(@NotNull HttpServletRequest request, @NotNull HttpServletResponse response, @NotNull Object handler, Exception ex) throws Exception {

    }



}
