package cn.qup.infra.pierre.springbootstarter.constant;

import java.util.ArrayList;

public class SwaggerUrl {
    public static java.util.List<String> List = new ArrayList<>();
    static{
        List.add("/doc.html");
        List.add("/webjars");
        List.add("/pierre-manage");
        List.add("/swagger-resources");
        List.add("/v3/api-docs");
        List.add("/v2/api-docs");
    }
}
