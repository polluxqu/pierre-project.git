package cn.qup.infra.pierre.springbootstarter.annotation;

import cn.qup.infra.pierre.springbootstarter.constant.AccessLimitRegion;
import cn.qup.infra.pierre.springbootstarter.constant.AccessLimitType;

import java.lang.annotation.*;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface AccessLimit {
    AccessLimitRegion region() default AccessLimitRegion.GLOBAL;
    // 接口限流类型 QPS 或 QUOTA
    AccessLimitType type() default AccessLimitType.QUOTA;
    // 接口限流窗口周期，秒
    int duration() default 60;
    // 接口限流窗口内允许的最大调用次数，或 QPS 最大值
    int limit() default 100;
}
