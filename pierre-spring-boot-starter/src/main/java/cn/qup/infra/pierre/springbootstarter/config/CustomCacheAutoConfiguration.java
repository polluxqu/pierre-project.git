package cn.qup.infra.pierre.springbootstarter.config;

import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ConcurrentHashMap;

@Configuration
public class CustomCacheAutoConfiguration {

    public final ConcurrentHashMap<String, Integer> cache = new ConcurrentHashMap<>();
}
