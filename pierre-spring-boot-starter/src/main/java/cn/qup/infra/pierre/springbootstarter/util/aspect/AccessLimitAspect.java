package cn.qup.infra.pierre.springbootstarter.util.aspect;

import cn.qup.infra.pierre.core.exception.BizException;
import cn.qup.infra.pierre.core.pojo.ApiResult;
import cn.qup.infra.pierre.springbootstarter.annotation.AccessLimit;
import cn.qup.infra.pierre.springbootstarter.config.CustomCacheAutoConfiguration;
import cn.qup.infra.pierre.springbootstarter.constant.AccessLimitRegion;
import cn.qup.infra.pierre.springbootstarter.constant.AccessLimitType;
import cn.qup.infra.pierre.springbootstarter.util.CacheUtil;
import cn.qup.infra.pierre.springbootstarter.util.HttpRequestUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;

@Slf4j
@Aspect
@Component
public class AccessLimitAspect {

    @Pointcut("@annotation(cn.qup.infra.pierre.springbootstarter.annotation.AccessLimit)")
    public void pointCut() {
    }

    @Around("pointCut()")
    public Object limitAccess(ProceedingJoinPoint joinPoint) throws Throwable {
        // 获取方法上的注解，并获取注解中的限流参数
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        AccessLimit accessLimit = signature.getMethod().getAnnotation(AccessLimit.class);
        AccessLimitType type = accessLimit.type();
        AccessLimitRegion region = accessLimit.region();
        int allowedRequests = accessLimit.limit();
        long timeWindow = accessLimit.duration() * 1000L; // 转换成毫秒

        String key = generateKey(region); // 生成基于方法和参数的唯一标识
        int count = CacheUtil.get(key,timeWindow);
            switch (type){
                case QPS:
                    // 如果是QPS限流
                    if (count >= allowedRequests) {
                        // 如果调用QPS超过限制，返回一个自定义的响应
                        // 由于QPS求的是并发连接数，统计中当前是按最近1秒的时间窗口判断是否并发，而并未去做更
                        // 精确的AOP before +， after - 然后count 这样的判断。
                        CacheUtil.removeExpired(key,1000);
                        return new ApiResult<>(HttpStatus.TOO_MANY_REQUESTS.value(), "接口调用QPS超出限制",null);
                    }
                    CacheUtil.put(key,new Timestamp(new Date().getTime()));
                    break;
                case QUOTA:
                    // 如果是请求量限流
                    if (count >= allowedRequests) {
                        // 如果调用次数超过限制，返回一个自定义的响应
                        return new ApiResult<>(HttpStatus.TOO_MANY_REQUESTS.value(), "接口时间窗口内调用次数超出限制",null);
                    }
                    CacheUtil.put(key,new Timestamp(new Date().getTime()));
                    // 清理过期的key，确保只保留指定时间窗口内的计数
                    CacheUtil.removeExpired(key,timeWindow);
            }

        // 如果没有超出限制，正常执行方法
        return joinPoint.proceed();
    }

    private String generateKey(AccessLimitRegion region) {
        // 生成key，可以包含方法名、参数值等信息，确保唯一性
        // 当限流范围为 Global 时，不考虑请求来源IP，单一接口调用阈值达到限流标准即执行限流
        // 当限流范围为 IP 时，仅在同一 IP 来源的请求达到阈值才对该 IP 执行限流
        HttpServletRequest request = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
        String ipAddress = HttpRequestUtil.getIPAddress(request);
        String url = request.getRequestURL().toString();
//        String methodName = joinPoint.getSignature().getName();
        String key = StringUtils.EMPTY;
        switch (region){
            case GLOBAL:
                key =  "url" + ":" + url;
                break;
            case IP:
                key= "access_limit:" + ipAddress + ":" + "url" + ":" + url;
        }
        return key;

    }
}
