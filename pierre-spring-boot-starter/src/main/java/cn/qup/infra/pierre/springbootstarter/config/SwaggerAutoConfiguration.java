package cn.qup.infra.pierre.springbootstarter.config;

import cn.qup.infra.pierre.core.enums.AppEnv;
import cn.qup.infra.pierre.core.properties.AppRuntime;
import cn.qup.infra.pierre.springbootstarter.properties.ExtendGitProperties;
import cn.qup.infra.pierre.springbootstarter.properties.SwaggerProperties;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.boot.SpringBootVersion;
import org.springframework.context.annotation.Bean;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.mvc.method.RequestMappingInfoHandlerMapping;
import springfox.documentation.RequestHandler;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.RequestHandlerProvider;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.spring.web.plugins.WebFluxRequestHandlerProvider;
import springfox.documentation.spring.web.plugins.WebMvcRequestHandlerProvider;

import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
public class SwaggerAutoConfiguration implements WebMvcConfigurer {
    @Autowired
    AppRuntime appRuntime;

    @Autowired
    private ExtendGitProperties git;

    private final SwaggerProperties swaggerProperties = new SwaggerProperties();

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("doc.html")
                .addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");

    }

    @Bean
    public Docket createRestApi(){
        // 只在 local 及 dev 环境中开放 swagger
        boolean enable = (appRuntime.getEnv().equals(AppEnv.dev) || appRuntime.getEnv().equals(AppEnv.local));
        return new Docket(DocumentationType.SWAGGER_2).pathMapping("/")
                .enable(enable)
                .apiInfo(apiInfo())
                .host(swaggerProperties.getTryHost())
                // 选择哪些接口作为swagger的doc发布
                .select()
                .apis(RequestHandlerSelectors.basePackage(appRuntime.getAppRootPackageName()))
//                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build().protocols(newHashSet("https","http"))
                .securityContexts(securityContexts());
    }

    /**
     * API 页面上半部分展示信息
     * @return
     */
    private ApiInfo apiInfo(){
        if(git != null){
            return new ApiInfoBuilder().title(appRuntime.getAppDescription() + " 接口文档")
                    .description("Pierre Project")
                    .termsOfServiceUrl(git.getGitOriginUrl())
                    .contact(new Contact(git.getAuthorName(),"",git.getAuthorEmail()))
                    .version(String.format("Commit Id: %s, Commit Time: %s, Spring Boot: %s <br /> " +
                                    "Comment: %s <br />Commit Count: %s",
                            git.getShortCommitId(),git.getCommitTime(), SpringBootVersion.getVersion(),git.getComment(),git.getCount()))
                    .build();
        }else{
            return new ApiInfoBuilder().title(appRuntime.getAppDescription() + " 接口文档")
                    .description("Pierre Project")
                    .build();
        }
    }

    /**
     * 授权信息全局应用
     */
    private List<SecurityContext> securityContexts() {
        return Collections.singletonList(
                SecurityContext.builder()
                        .securityReferences(Collections.singletonList(new SecurityReference("BASE_TOKEN", new AuthorizationScope[]{new AuthorizationScope("global", "")})))
                        .build()
        );
    }

    @SafeVarargs
    private final <T> Set<T> newHashSet(T... ts) {
        if (ts.length > 0) {
            return new LinkedHashSet<>(Arrays.asList(ts));
        }
        return null;
    }

    /**
     * 修复 spring fox swagger 与 spring boot starter actuator 2.6 之后的版本冲突问题
     * @return
     */
    @Bean
    public static BeanPostProcessor springFoxHandlerProviderBeanPostProcessor() {
        return new BeanPostProcessor() {

            @Override
            public Object postProcessAfterInitialization(@NotNull Object bean, @NotNull String beanName) throws BeansException {
                if (bean instanceof WebMvcRequestHandlerProvider || bean instanceof WebFluxRequestHandlerProvider) {
//                    for ( RequestHandler handler: ((RequestHandlerProvider) bean).requestHandlers()
//                         ) {
//                        log.info("handler.getName() = " + JSON.toJSONString(handler.key().getPathMappings()));
//                    }
                    customizeSpringFoxHandlerMappings(getHandlerMappings(bean));
                }
                return bean;
            }

            private <T extends RequestMappingInfoHandlerMapping> void customizeSpringFoxHandlerMappings(List<T> mappings) {
                List<T> copy = mappings.stream()
                        .filter(mapping -> mapping.getPatternParser() == null)
                        .collect(Collectors.toList());
                mappings.clear();
                mappings.addAll(copy);
            }

            @SuppressWarnings("unchecked")
            private List<RequestMappingInfoHandlerMapping> getHandlerMappings(Object bean) {
                try {
                    Field field = ReflectionUtils.findField(bean.getClass(), "handlerMappings");
                    assert field != null;
                    field.setAccessible(true);
                    return (List<RequestMappingInfoHandlerMapping>) field.get(bean);
                } catch (IllegalArgumentException | IllegalAccessException e) {
                    log.warn("",e);
                    return new ArrayList<RequestMappingInfoHandlerMapping>();
                }
            }
        };
    }

}
