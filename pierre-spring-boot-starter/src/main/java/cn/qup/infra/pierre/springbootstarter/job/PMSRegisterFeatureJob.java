package cn.qup.infra.pierre.springbootstarter.job;

import cn.qup.infra.pierre.core.properties.AppRuntime;
import cn.qup.infra.pierre.springbootstarter.pojo.Feature;
import cn.qup.infra.pierre.springbootstarter.service.IPMSFeatureService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PMSRegisterFeatureJob implements InitializingBean {

    @Autowired
    AppRuntime appRuntime;

    @Autowired
    @Qualifier("allControllerList")
    List<Feature> features;

    @Autowired
    @Qualifier("EnableAutoRegisterPMS")
    boolean enableAutoRegisterPMS;

    private IPMSFeatureService ipmsFeatureService;

    @Override
    public void afterPropertiesSet() {
        if(enableAutoRegisterPMS){
            ipmsFeatureService.registerFeature(appRuntime,features);
        }
    }
}
