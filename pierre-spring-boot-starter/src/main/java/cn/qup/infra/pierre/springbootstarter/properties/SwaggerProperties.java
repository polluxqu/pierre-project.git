package cn.qup.infra.pierre.springbootstarter.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "swagger")
@EnableConfigurationProperties({SwaggerProperties.class})
public class SwaggerProperties {
    private String tryHost = "http://localhost:8080";
}
