package cn.qup.infra.pierre.springbootstarter.constant;

public enum AccessLimitType {
    QPS,
    QUOTA
}
