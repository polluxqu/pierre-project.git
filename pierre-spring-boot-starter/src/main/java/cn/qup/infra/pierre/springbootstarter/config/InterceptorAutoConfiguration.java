package cn.qup.infra.pierre.springbootstarter.config;

import cn.qup.infra.pierre.core.properties.AppRuntime;
import cn.qup.infra.pierre.springbootstarter.constant.NoneAccessLogUrl;
import cn.qup.infra.pierre.springbootstarter.util.interceptor.AccessLogInterceptor;
import cn.qup.infra.pierre.springbootstarter.util.interceptor.AutoKeyAuthInterceptor;
import cn.qup.infra.pierre.springbootstarter.util.interceptor.DisableSwaggerPublicAccessInterceptor;
import cn.qup.infra.pierre.springbootstarter.util.interceptor.SignatureInterceptor;
import cn.qup.infra.pierre.springbootstarter.util.net.RequestBodyFilter;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Slf4j
@Configuration
@Order(Ordered.HIGHEST_PRECEDENCE)
public class InterceptorAutoConfiguration implements WebMvcConfigurer {

    @Autowired
    AppRuntime appRuntime;

    @Bean
    public AccessLogInterceptor accessLogInterceptor(){
        return new AccessLogInterceptor();
    }
    @Bean
    public DisableSwaggerPublicAccessInterceptor disableSwaggerPublicAccessInterceptor(){
        return new DisableSwaggerPublicAccessInterceptor();
    }
    @Bean
    public AutoKeyAuthInterceptor autoKeyAuthInterceptor(){return new AutoKeyAuthInterceptor();}
    @Bean
    public SignatureInterceptor signatureInterceptor(){
        return new SignatureInterceptor();
    }

    @Bean
    public CorsFilter corsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", buildConfig());
        return new CorsFilter(source);
    }

    @Bean
    public FilterRegistrationBean setLogFilter(){
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        RequestBodyFilter requestBodyFilter = new RequestBodyFilter();
        registrationBean.setFilter(requestBodyFilter);
        registrationBean.setName("interceptor filter body params");
        registrationBean.addUrlPatterns("/*");
//        registrationBean.setOrder(1);
        return registrationBean;
    }

    /**
     * 管理所有的拦截器，并对swagger 相关的路径进行排除
     */
    @Override
    public void addInterceptors(@NotNull InterceptorRegistry registry) {
        try{
            // 添加 starter 中的各类拦截器
            // 禁止 swagger 公网访问
            // 禁止 actuator 公网访问
            if(appRuntime.isSwaggerInternalAccessOnly()){
                registry.addInterceptor(disableSwaggerPublicAccessInterceptor())
                        .addPathPatterns("/doc.html**/**")
                        .addPathPatterns("/swagger**/**")
                        .addPathPatterns("/webjars/**")
                        .addPathPatterns("/actuator/**")
                        .addPathPatterns("/v3/**")
                        .addPathPatterns("/v2/**");
            }

            // 访问日志拦截器，排除 swagger 相关
            registry.addInterceptor(accessLogInterceptor())
                    .excludePathPatterns(NoneAccessLogUrl.List)
                    .excludePathPatterns("/swagger**/**")
                    .excludePathPatterns("/webjars/**")
                    .excludePathPatterns("/v3/**")
                    .excludePathPatterns("/doc.html")
                    .addPathPatterns("/**");

            // PMS Token模式自动验证
            registry.addInterceptor(autoKeyAuthInterceptor())
                    .excludePathPatterns("/swagger**/**")
                    .excludePathPatterns("/webjars/**")
                    .excludePathPatterns("/v3/**")
                    .excludePathPatterns("/actuator/**")
                    .excludePathPatterns("/doc.html")
                    .addPathPatterns("/**");

            // PMS 签名模式自动验证
            registry.addInterceptor(signatureInterceptor())
                    .excludePathPatterns("/swagger**/**")
                    .excludePathPatterns("/webjars/**")
                    .excludePathPatterns("/v3/**")
                    .excludePathPatterns("/actuator/**")
                    .excludePathPatterns("/doc.html")
                    .addPathPatterns("/**");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 设置允许跨域
     * @return
     */
    private CorsConfiguration buildConfig() {
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        corsConfiguration.addAllowedOriginPattern("*");
        corsConfiguration.setAllowCredentials(true);
        corsConfiguration.addAllowedHeader("*");
        corsConfiguration.addAllowedMethod("*");

        return corsConfiguration;
    }
}
