package cn.qup.infra.pierre.springbootstarter.pojo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Feature {

    @ApiModelProperty("feature 所属类别")
    private String category;

    @ApiModelProperty("feature 名称，同一服务id下唯一")
    private String name;

    @ApiModelProperty("feature 方法，GET/POST/PUT 等")
    private String method;

    @ApiModelProperty("feature 终结点地址，为相对地址例如  /api/v1/sso/login")
    private String endpoint;

    @ApiModelProperty("接口设置不验签只反查 token 对应的用户信息")
    private String headerOnly;

    @ApiModelProperty("接口说明")
    private String description;

    @ApiModelProperty("接口备注")
    private String comment;
}
