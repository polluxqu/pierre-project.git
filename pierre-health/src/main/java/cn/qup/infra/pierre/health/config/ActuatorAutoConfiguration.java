package cn.qup.infra.pierre.health.config;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.endpoint.EndpointId;
import org.springframework.boot.actuate.autoconfigure.endpoint.web.WebEndpointProperties;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.web.reactive.HttpHandlerAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.reactive.context.ReactiveWebServerApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.util.ObjectUtils;
import org.springframework.web.reactive.config.WebFluxConfigurer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Slf4j
@Configuration
@EnableConfigurationProperties
@ConditionalOnClass(WebEndpointProperties.class) // 确保只有在存在WebEndpointProperties类时才应用此配置
public class ActuatorAutoConfiguration {
    @Autowired
    private WebEndpointProperties webEndpointProperties;

    @Bean
    // 避免与 actuator 原生配置冲突
    @Primary
    // 仅在本地及开发环境开放 Actuator 的访问
    @Profile({"local","dev"})
    public WebEndpointProperties actuatorWebEndpointProperties() {
        // 配置exposure.include 和 exposure.exclude
        // 仅在主应用未配置 actuator 端点时提供默认配置，如主应用已做配置，则不进行覆盖
        WebEndpointProperties.Exposure exposure = webEndpointProperties.getExposure();

        if(exposure.getExclude().isEmpty() && exposure.getInclude().isEmpty()){
            // 配置 允许的端点
            Set<String> include = new HashSet<>(Arrays.asList("health","info","mappings","http-trace","metrics","prometheus"));
            webEndpointProperties.getExposure().setInclude(include); // 根据需要替换这些端点名称
            // 配置 禁止的端点
            Set<String> exclude = new HashSet<>(Arrays.asList("threaddump","beans","configprops","env","heapdump"));
            webEndpointProperties.getExposure().setExclude(exclude);
        }

        // 在主应用未配置Actuator base-path 的情况下，变更配置 base-path 默认为 pierre-manage
        // 如主应用已配置自定义的Actuator base-path，则不进行覆盖
        if(webEndpointProperties.getBasePath().equalsIgnoreCase("/actuator")){
            webEndpointProperties.setBasePath("/pierre-manage"); // 更改Actuator默认的基础路径
        }
        log.info("【Pierre-Health】: Actuator base-path is '{}'", webEndpointProperties.getBasePath());
        return webEndpointProperties;
    }
}
