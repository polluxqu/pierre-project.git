package cn.qup.infra.pierre.health.config;

import org.springframework.context.annotation.Import;

@Import({ConflictHandlingConfig.class,
TraceAutoConfiguration.class,
ActuatorAutoConfiguration.class})
public class Config {
}
